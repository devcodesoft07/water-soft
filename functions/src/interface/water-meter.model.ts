/* eslint-disable linebreak-style */
import { PartnerAction } from './partners/partner-action.model';
import { Partner } from './partners/partner.model';

/* eslint-disable @typescript-eslint/naming-convention */
export interface WaterMeter {
  id: string;
  water_meter_number: string;
  bought: Date;
  unit_cost: number;
  active: boolean;
  used: boolean;
  expenses: boolean;
  register_date: Date;
  partner?: Partner;
  partner_action?: PartnerAction;
}
