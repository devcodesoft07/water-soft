/* eslint-disable linebreak-style */
export interface ReaderRequest {
  dateStart: string,
  dateEnd: string,
  location: string,
}
