/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/naming-convention */
import { Partner } from '../partners/partner.model';
export interface IncomeRequest {
  dateStart: string;
  dateEnd: string;
}

export interface Income {
  id: string;
  amount: number;
  coin: string;
  detail: string;
  date: Date;
  category: IncomeCatergory;
  partner: Partner[];
}

export interface IncomeCatergory {
  id: string;
  name: string;
  description: string;
  image: string;
  type: string;
}
