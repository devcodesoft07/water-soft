/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/naming-convention */
export interface IncomesReportRequest {
  startDate: string;
  endDate: string;
  monthName: string;
}
