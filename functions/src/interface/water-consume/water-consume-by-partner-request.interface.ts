/* eslint-disable linebreak-style */

import { PayByPartner } from '../partners/pay-by-partner.interface';

/* eslint-disable @typescript-eslint/naming-convention */
export interface WaterConsumeByPartnerRequest {
  dateStart: string;
  dateEnd: string;
  partnerId: string;
}

export interface AddWaterConsumeReadingByPartnerRequest {
  dateStart: string;
  dateEnd: string;
  partnerId: string;
  data: PayByPartner;
}
