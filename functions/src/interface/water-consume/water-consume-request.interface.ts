/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/naming-convention */
export interface WaterConsumeRequest {
  dateStart: string;
  dateEnd: string;
  partnerId: string;
  partnerActionId: string;
}
