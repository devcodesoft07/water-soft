/* eslint-disable linebreak-style */
/* eslint-disable no-trailing-spaces */
/* eslint-disable @typescript-eslint/no-explicit-any */
export interface ResponseApi {
  message: string;
  status: number;
  data: any;
}
