/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/naming-convention */
export interface ExpensesReportRequest {
  startDate: string;
  endDate: string;
  monthName: string;
}
