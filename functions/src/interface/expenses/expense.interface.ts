/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/naming-convention */
export interface Expense {
  id: string;
  amount: number;
  coin: string;
  expense_category: ExpenseCategory;
  passage: boolean;
  detail: string;
  date: Date;
}

interface ExpenseCategory {
  id: number;
  name: string;
  description: string;
}
