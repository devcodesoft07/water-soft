/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/naming-convention */
import { CreateUpdateModel } from '../create-update.model';

export interface PartnerCommunity extends CreateUpdateModel {
  id: number;
  name: string;
  district_id: number;
  district: string;
  location: string;
}
