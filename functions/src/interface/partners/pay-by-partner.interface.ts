/* eslint-disable linebreak-style */
import { Timestamp } from 'firebase-admin/firestore';

export interface PayByPartner {
  id?: string;
  dateToPay: Date | Timestamp;
  amount: number;
  debts: number;
  readering_last: number;
  readering_actual: number;
  water_consume: number;
  isPay: boolean;
}
