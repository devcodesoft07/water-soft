/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/naming-convention */
import { WaterMeter } from '../water-meter.model';
import { PartnerAction } from './partner-action.model';
import { PartnerCommunity } from './partner-community.model';

export interface Partner {
  id?: string;
  name: string;
  last_name: string;
  identity_card: IdentityCardPartner;
  spouse: string;
  community: PartnerCommunity;
  telephones: string[];
  membership: Date;
  sewerage: boolean;
  active: boolean;
  waterCut: any;
  waterMeter: WaterMeter;
  partnerAction: PartnerAction[];
}

export interface IdentityCardPartner {
  ci_number: number;
  complement: string;
  issued: string;
}
