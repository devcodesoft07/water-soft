/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/naming-convention */
import { CreateUpdateModel } from '../create-update.model';

export interface IdentityCardPartner extends CreateUpdateModel {
  ci_number: number;
  complement: string;
  issued: string;
  partner_id: string;
}
