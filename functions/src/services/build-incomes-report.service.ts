/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-var-requires */
import { HttpsFunction, https } from 'firebase-functions';
import { firestore } from 'firebase-admin';
import { DocumentData, QueryDocumentSnapshot } from 'firebase-admin/firestore';
import { Income } from '../interface/incomes/income.model';
import { IncomesReportRequest } from '../interface/incomes/incomes-report-request.interface';
import { PayByPartner } from '../interface/partners/pay-by-partner.interface';
const PDFDocument = require('pdfkit-table');

export class BuildIncomesReportService {
  private _collectionName: string;
  private _waterConcumeSubCollectionName: string;

  constructor() {
    this._collectionName = 'income';
    this._waterConcumeSubCollectionName = 'water-consume';
  }

  buildIncomesReportByMonth(): HttpsFunction {
    return https.onCall(async (incomesReportRequest: IncomesReportRequest) => {
      const incomesByDate: Income[] = await this._getIncomesByDate(incomesReportRequest);
      const dataToWaterConsume: PayByPartner[] = await this._getAllPaymentsByAllPartners(incomesReportRequest);
      const incomesDataToTable = await this._generateDataToTable(incomesByDate, dataToWaterConsume);
      const result = await this.generatePDF(incomesDataToTable, incomesReportRequest);
      return result;
    });
  }

  private async _getIncomesByDate(request: IncomesReportRequest): Promise<Income[]> {
    return await (
      await firestore().collection(this._collectionName)
        .where('date', '>=', request.startDate)
        .where('date', '<', request.endDate)
        .get()
    ).docs.map((document: QueryDocumentSnapshot<DocumentData>) => {
      return {
        id: document.id,
        ...document.data(),
      } as any;
    });
  }

  private async generatePDF(incomes: any, incomesReportRequest: IncomesReportRequest): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        const pdfDoc = new PDFDocument({ margin: 30 });
        const chunks: any[] = [];

        pdfDoc.text(`${'Reporte de Ingresos del mes de'.concat(` ${incomesReportRequest.monthName} `).concat(new Date(incomesReportRequest.startDate).getFullYear().toString())}`, {
          align: 'center',
        });

        const table: any = {
          title: ' ',
          headers: [
            { label: 'Fecha', property: 'date', width: 100, renderer: null },
            { label: 'Categoría', property: 'category', width: 100, renderer: null },
            { label: 'Detalle', property: 'detail', width: 200, renderer: null },
            { label: 'Monto $', property: 'amount1', width: 70, renderer: null },
            { label: 'Monto Bs', property: 'amount2', width: 70, renderer: null },
          ],
          datas: incomes,
          rows: [
            [
              'TOTAL',
              '',
              '',
              `${incomes.reduce((total: number, currentValue: any) => total + (Number(currentValue.amount1) || 0), 0)} $us`,
              `${incomes.reduce((total: number, currentValue: any) => total + (Number(currentValue.amount2) || 0), 0)} Bs`,
            ],
          ],
        };
        pdfDoc.table(table, {
          columnSpacing: 8,
          padding: 10,
          align: 'center',
          prepareHeader: () => pdfDoc.font('Helvetica-Bold').fontSize(10),
          prepareRow: (row: any, indexColumn: number, indexRow: any, rectRow: any, rectCell: any) => {
            if (Array.isArray(row)) {
              pdfDoc.font('Helvetica-Bold').fontSize(9);
            } else {
              pdfDoc.font('Helvetica').fontSize(8);
            }
          },
        });

        pdfDoc.on('data', (chunk: any) => chunks.push(chunk));

        pdfDoc.on('end', () => {
          const pdfBuffer = Buffer.concat(chunks);

          const filename = 'example.pdf';
          const response: any = {
            headers: {
              'Content-Type': 'application/pdf',
              'Content-Disposition': `inline; filename="${filename}"`,
              'Content-Length': pdfBuffer.length.toString(),
            },
            data: pdfBuffer.toString('base64'),
          };
          resolve(response);
        });

        pdfDoc.end();
      } catch (error) {
        console.error('Error generando el PDF:', error);
        reject(error);
      }
    });
  }

  private async _getAllPaymentsByAllPartners(request: IncomesReportRequest): Promise<PayByPartner[]> {
    const partnersSnapshot = await firestore().collection('partner').get();
    const allPayments: PayByPartner[] = [];

    for (const partnerDoc of partnersSnapshot.docs) {
      const paymentsSnapshot = await partnerDoc.ref
        .collection(this._waterConcumeSubCollectionName)
        .where('dateToPay', '>=', request.startDate)
        .where('dateToPay', '<', request.endDate)
        .get();

      const payments = paymentsSnapshot.docs.map((document: QueryDocumentSnapshot<DocumentData>) => {
        return {
          partnerId: partnerDoc.id,
          paymentId: document.id,
          ...document.data(),
        } as any;
      });

      allPayments.push(...payments);
    }

    return allPayments;
  }

  private _generateDataToTable(incomes: Income[], waterConsumeByPartners: PayByPartner[]): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        const incomesDataTable = [];
        let actionPayAmountIncomes = {
          category: 'Venta de Acción',
          detail: 'Ingresos por Venta de Acción',
          amount1: 0,
          amount2: 0,
        } as any;
        for (const income of incomes) {
          const dataToTable = {
            date: `${new Date(income.date).toLocaleDateString()}`,
            category: `${income.category.name}`,
            detail: `${income.detail}`,
            amount1: `${income.coin.includes('$us') ? income.amount : 0}`,
            amount2: `${income.coin.includes('Bs') ? income.amount : 0}`,
          };
          if (income.category.type === 'ACTION_SALE') {
            actionPayAmountIncomes = {
              ...actionPayAmountIncomes,
              amount1: actionPayAmountIncomes.amount1 += Number(dataToTable.amount1),
              amount2: actionPayAmountIncomes.amount2 += Number(dataToTable.amount2),
            };
          } else {
            incomesDataTable.push(dataToTable);
          }
        }

        const amountWaterConsume: number = waterConsumeByPartners.reduce((total, currentValue: PayByPartner) => total + (currentValue.amount || 0), 0);

        const waterConsumePayAmountIncomes = {
          category: 'Consumo de Agua',
          detail: 'Total cobrado por consumo de agua',
          amount1: 0,
          amount2: amountWaterConsume,
        } as any;

        const amountWaterConsumeDebts: number = waterConsumeByPartners.reduce((total, currentValue: PayByPartner) => total + (currentValue.debts || 0), 0);

        const amountWaterConsumeDebtsIncomes = {
          category: 'Multas Consumo de Agua',
          detail: 'Total cobrado por multas de pagos atrasados del consumo de agua',
          amount1: 0,
          amount2: amountWaterConsumeDebts,
        } as any;

        incomesDataTable.push(actionPayAmountIncomes);
        incomesDataTable.push(waterConsumePayAmountIncomes);
        incomesDataTable.push(amountWaterConsumeDebtsIncomes);

        resolve(incomesDataTable);
      } catch (error) {
        reject(error);
      }
    });
  }
}
