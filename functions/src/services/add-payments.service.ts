/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/no-empty-function */

import { firestore } from 'firebase-admin';
import { https, HttpsFunction, Request, Response } from 'firebase-functions';
import { DocumentData, QueryDocumentSnapshot } from 'firebase-admin/firestore';
import { ResponseUtil } from '../utils/response.util';
import { Partner } from '../interface/partners/partner.model';
import { PayByPartner } from '../interface/partners/pay-by-partner.interface';

export class AddPaymentsService {
  private _resUtil: ResponseUtil;
  private _collectionName: string;
  private _subCollectionName: string;
  constructor() {
    this._resUtil = new ResponseUtil();
    this._collectionName = 'partner';
    this._subCollectionName = 'water-consumed';
  }

  addPaymentsToPartner(): HttpsFunction {
    return https.onRequest(async (request: Request, response: Response) => {
      const partnerList: Partner[] = await this._getAllPartners();
      this._addPayByPartner(partnerList);
      response.send(
        this._resUtil.getResponseData(
          response.statusMessage,
          response.statusCode,
          partnerList
        )
      );
    });
  }

  private _addPayByPartner(partners: DocumentData[]): void {
    partners.forEach((partner: DocumentData) => {
      this._addNewPay(partner);
    });
  }

  private async _addNewPay(partner: DocumentData): Promise<void> {
    const payData: PayByPartner = {
      dateToPay: new Date(),
      amount: 0,
      debts: 0,
      water_consume: 0,
      isPay: false,
      readering_actual: 0,
      readering_last: 0,
    };
    await firestore().collection(this._collectionName).doc(partner.id).collection(this._subCollectionName).add(payData);
  }

  private async _getAllPartners(): Promise<Partner[]> {
    return await (
      await firestore().collection(this._collectionName).get()
    ).docs.map((doc: QueryDocumentSnapshot<DocumentData>) => {
      return {
        id: doc.id,
        ...doc.data(),
      } as Partner;
    });
  }
}
