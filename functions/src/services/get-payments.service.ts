/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-explicit-any */

import { firestore } from 'firebase-admin';
import { DocumentData, QueryDocumentSnapshot } from 'firebase-admin/firestore';
import { https, HttpsFunction } from 'firebase-functions';
import { Partner } from '../interface/partners/partner.model';
import { PayByPartner } from '../interface/partners/pay-by-partner.interface';
import { ReaderRequest } from '../interface/reader-request.interface';
import { ResponseApi } from '../interface/response-api.iterface';
import { ResponseUtil } from '../utils/response.util';
import { WaterMeter } from '../interface/water-meter.model';
export interface WaterConsumedQuerySnapshot {
  partner: any;
  payments: firestore.QuerySnapshot<firestore.DocumentData>;
  paymentCurrenMonth: firestore.QuerySnapshot<firestore.DocumentData>;
}
export interface WaterConsumed {
  partner: any;
  water_consumed: PayByPartner[];
}

export class GetPaymentsService {
  private _resUtil: ResponseUtil;
  private _collectionName: string;
  private _subCollectionName: string;
  constructor() {
    this._resUtil = new ResponseUtil();
    this._collectionName = 'partner';
    this._subCollectionName = 'water-consume';
  }

  getAllPaymentsByMonth(): HttpsFunction {
    return https.onCall(async (request: ReaderRequest) => {
      const partnerList: Partner[] = await this._getAllPartners(request);
      return await this._getPayments(partnerList, request);
    });
  }

  private async _getPayments(partners: DocumentData[], request: ReaderRequest): Promise<ResponseApi> {
    const allPaymentsByUser: Promise<WaterConsumedQuerySnapshot>[] = await this._getPaymentsByUser(partners, request);
    const allPaymentsByUserValues: Promise<WaterConsumed>[] = await this._getPaymentsByUserValues(allPaymentsByUser);

    const waterConsumed: WaterConsumed[] = await Promise.all(allPaymentsByUserValues);
    return this._resUtil.getResponseData(
      'Partner list readered success',
      200,
      waterConsumed
    );
  }

  private async _getAllPartners(request: ReaderRequest): Promise<Partner[]> {
    const location = request.location as string;
    const partners = (
      await firestore().collection(this._collectionName).orderBy('last_name', 'asc').get()
    ).docs.map(async (doc: QueryDocumentSnapshot<DocumentData>) => {
      const partnerDocRef = firestore().collection(this._collectionName).doc(doc.id);
      const actionPartnerCollectionRef = partnerDocRef.collection('action');
      const actionPartnerQuerySnapshot = await actionPartnerCollectionRef.limit(1).get();

      const waterMeterOfPartner: WaterMeter = actionPartnerQuerySnapshot.docs[0] ? actionPartnerQuerySnapshot.docs[0].data().water_meter?.measurer : {};

      return {
        id: doc.id,
        ...doc.data(),
        waterMeter: waterMeterOfPartner,
      } as Partner;
    });

    const resolvedPartners = await Promise.all(partners);

    return resolvedPartners.filter((partner) => request.location !== 'Todos los Socios' ? partner.community.location === location : partner);
  }

  private async _getPaymentsByUser(partners: DocumentData[], request: ReaderRequest): Promise<Promise<WaterConsumedQuerySnapshot>[]> {
    const today: Date = new Date();
    const firstDayOfCurrentMonth = new Date(today.getFullYear(), today.getMonth(), 1);
    const lastDayOfCurrentMonth = new Date(today.getFullYear(), today.getMonth() + 1, 0);

    const firstDayOfCurrentMonthISO = firstDayOfCurrentMonth.toISOString();
    const lastDayOfCurrentMonthISO = lastDayOfCurrentMonth.toISOString();

    return await (partners.map(async (partner: DocumentData) => {
      const payments: firestore.QuerySnapshot<firestore.DocumentData> =
        await await firestore()
          .collection(this._collectionName)
          .doc(partner.id)
          .collection(this._subCollectionName)
          .where('dateToRead', '>=', request.dateStart)
          .where('dateToRead', '<=', request.dateEnd)
          .where('isPay', '==', false)
          .get();

      const paymentCurrenMonth = await firestore()
        .collection(this._collectionName)
        .doc(partner.id)
        .collection(this._subCollectionName)
        .where('dateToRead', '>=', firstDayOfCurrentMonthISO)
        .where('dateToRead', '<=', lastDayOfCurrentMonthISO)
        .limit(1)
        .get();

      return {
        partner,
        payments,
        paymentCurrenMonth,
      };
    }));
  }

  private async _getPaymentsByUserValues(paymentsByUser: Promise<WaterConsumedQuerySnapshot>[]): Promise<Promise<WaterConsumed>[]> {
    return await paymentsByUser.map(async (data) => {
      let waterConsumed: PayByPartner[] = (await data).payments.docs.map(
        (doc: firestore.DocumentData) => {
          return {
            id: doc.id,
            ...doc.data(),
          } as PayByPartner;
        }
      );

      const paymentCurrenMonth: PayByPartner[] = (await data).paymentCurrenMonth.docs.map(
        (doc: firestore.DocumentData) => {
          return {
            id: doc.id,
            ...doc.data(),
          } as PayByPartner;
        }
      );

      if (paymentCurrenMonth && !!paymentCurrenMonth.length && paymentCurrenMonth[0].id) {
        const lastWat: PayByPartner | undefined = waterConsumed.find((payment: PayByPartner) => payment.id === paymentCurrenMonth[0].id);
        if (!lastWat) {
          waterConsumed = [...waterConsumed, paymentCurrenMonth[0]];
        }
      }

      return {
        partner: (await data).partner,
        water_consumed: waterConsumed,
      };
    });
  }
}
