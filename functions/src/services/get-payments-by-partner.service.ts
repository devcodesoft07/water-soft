/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/no-empty-function */

import { HttpsFunction, https } from 'firebase-functions';
import { ResponseUtil } from '../utils/response.util';
import { WaterConsumeByPartnerRequest } from '../interface/water-consume/water-consume-by-partner-request.interface';
import { PayByPartner } from '../interface/partners/pay-by-partner.interface';
import { firestore } from 'firebase-admin';
import { DocumentData, QueryDocumentSnapshot } from 'firebase-admin/firestore';
import { ResponseApi } from '../interface/response-api.iterface';
import { Partner } from '../interface/partners/partner.model';

/* eslint-disable @typescript-eslint/no-explicit-any */
export class GetPaymentsByPartnerService {
  private _resUtil: ResponseUtil;
  private _collectionName: string;
  private _subCollectionName: string;

  constructor() {
    this._resUtil = new ResponseUtil();
    this._collectionName = 'partner';
    this._subCollectionName = 'water-consume';
  }

  getAllPaymentsByPartnerByMonth(): HttpsFunction {
    return https.onCall(async (request: WaterConsumeByPartnerRequest) => {
      const payByPartner: PayByPartner[] = await this._getAllPaymentsByPartner(request);
      return await this._getWaterConsumeByPartnerByMonth(payByPartner, request);
    });
  }

  private async _getWaterConsumeByPartnerByMonth(payByPartner: PayByPartner[], request: WaterConsumeByPartnerRequest): Promise<ResponseApi> {
    const partnerData: Partner = await this._getPartnerData(request);
    const waterConsumeByMonths: PayByPartner[] = await Promise.all(payByPartner);
    return this._resUtil.getResponseData(
      'Water consume by partner success',
      200,
      {
        waterConsumeByMonths: waterConsumeByMonths,
        partnerData: partnerData,
      });
  }

  private async _getAllPaymentsByPartner(request: WaterConsumeByPartnerRequest): Promise<PayByPartner[]> {
    return await (
      await firestore().collection(this._collectionName).doc(request.partnerId).collection(this._subCollectionName)
        .where('dateToRead', '>=', request.dateStart)
        .where('dateToRead', '<=', request.dateEnd)
        .get()
    ).docs.map((document: QueryDocumentSnapshot<DocumentData>) => {
      return {
        id: document.id,
        ...document.data(),
      } as PayByPartner;
    });
  }

  private async _getPartnerData(request: WaterConsumeByPartnerRequest): Promise<Partner> {
    const documentSnapshot = await firestore().collection(this._collectionName).doc(request.partnerId).get();

    return {
      id: documentSnapshot.id,
      ...documentSnapshot.data(),
    } as Partner;
  }
}
