/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/no-empty-function */
import { https, HttpsFunction, Request, Response } from 'firebase-functions';
import { firestore } from 'firebase-admin';
import { DocumentReference, DocumentData } from 'firebase-admin/firestore';
import { ResponseUtil } from '../utils/response.util';

export class AddMessageService {
  private resUtil: ResponseUtil;
  constructor() {
    this.resUtil = new ResponseUtil();
  }

  hello(): HttpsFunction {
    return https.onRequest((request: Request, response: Response) => {
      const textToMessage: string = request.query.text as string;
      response.send(
        this.resUtil.getResponseData(
          response.statusMessage,
          response.statusCode,
          textToMessage
        )
      );
    });
  }

  addMessages(): HttpsFunction {
    return https.onRequest(async (request: Request, response: Response) => {
      const textToMessage: string = request.query.text as string;
      const addNewMessage: DocumentReference<DocumentData> = await firestore()
        .collection('message')
        .add({ original: textToMessage });
      response.send(
        this.resUtil.getResponseData(
          response.statusMessage,
          response.statusCode,
          addNewMessage.id
        )
      );
    });
  }
}
