/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { HttpsFunction, https } from 'firebase-functions';
import { ResponseUtil } from '../utils/response.util';
import { WaterConsumeRequest } from '../interface/water-consume/water-consume-request.interface';
import { PayByPartner } from '../interface/partners/pay-by-partner.interface';
import { firestore } from 'firebase-admin';
import { DocumentData, QueryDocumentSnapshot } from 'firebase-admin/firestore';
import { ResponseApi } from '../interface/response-api.iterface';
import { WaterCut } from '../interface/water-cut/WaterCut';

export class GetWaterConsumeByPartnerService {
  private _resUtil: ResponseUtil;
  private _collectionName: string;
  private _subCollectionName: string;

  constructor() {
    this._resUtil = new ResponseUtil();
    this._collectionName = 'partner';
    this._subCollectionName = 'water-consume';
  }

  getWaterConsumeByPartner(): HttpsFunction {
    return https.onCall(async (request: WaterConsumeRequest) => {
      const payByPartner: PayByPartner[] = await this._getAllPaymentsByPartner(request);
      const historicalWaterCut: WaterCut[] = await this._getAllHistoricalWaterCutByPartner(request);
      return await this._getWaterConsumeByPartnerByMonth(payByPartner, historicalWaterCut);
    });
  }

  private async _getWaterConsumeByPartnerByMonth(payByPartner: PayByPartner[], historicalWaterCut: WaterCut[]): Promise<ResponseApi> {
    const waterConsumeByMonths: PayByPartner[] = await Promise.all(payByPartner);
    const historicalWaterCutByPartnerAction: WaterCut[] = await Promise.all(historicalWaterCut);
    return this._resUtil.getResponseData(
      'Water consume by partner success',
      200,
      {
        waterConsumeByMonths: waterConsumeByMonths,
        historicalWaterCut: historicalWaterCutByPartnerAction,
      });
  }

  private async _getAllPaymentsByPartner(request: WaterConsumeRequest): Promise<PayByPartner[]> {
    return await (
      await firestore().collection(this._collectionName).doc(request.partnerId).collection(this._subCollectionName)
        .where('dateToRead', '>=', request.dateStart)
        .where('dateToRead', '<', request.dateEnd)
        .get()
    ).docs.map((document: QueryDocumentSnapshot<DocumentData>) => {
      return {
        id: document.id,
        ...document.data(),
      } as PayByPartner;
    });
  }

  private async _getAllHistoricalWaterCutByPartner(request: WaterConsumeRequest): Promise<WaterCut[]> {
    return await (
      await firestore().collection(this._collectionName).doc(request.partnerId).collection('action').doc(request.partnerActionId).collection('history-water-cut').get()
    ).docs.map((document: QueryDocumentSnapshot<DocumentData>) => {
      return {
        id: document.id,
        ...document.data(),
      } as WaterCut;
    });
  }
}
