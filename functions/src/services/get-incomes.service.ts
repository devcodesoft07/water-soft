/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { HttpsFunction, https } from 'firebase-functions';
import { ResponseUtil } from '../utils/response.util';
import { PayByPartner } from '../interface/partners/pay-by-partner.interface';
import { firestore } from 'firebase-admin';
import { DocumentData, QueryDocumentSnapshot } from 'firebase-admin/firestore';
import { ResponseApi } from '../interface/response-api.iterface';
import { Income, IncomeRequest } from '../interface/incomes/income.model';

export class GetIncomesService {
  private _resUtil: ResponseUtil;
  private _collectionName: string;
  private _subCollectionName: string;

  constructor() {
    this._resUtil = new ResponseUtil();
    this._collectionName = 'income';
    this._subCollectionName = 'water-consume';
  }

  getIncomesByDate(): HttpsFunction {
    return https.onCall(async (request: IncomeRequest) => {
      return await this._getAllIncomesByMonthAndYear(request);
    });
  }

  private async _getAllIncomesByMonthAndYear(request: IncomeRequest): Promise<ResponseApi> {
    const incomesData: Income[] = await this._getAllIncomes(request);
    const waterConsumeByAllPartners: PayByPartner[] = await this._getAllPaymentsByAllPartners(request);

    const incomesAllData: Income[] = await Promise.all(incomesData);
    return this._resUtil.getResponseData(
      'Incomes get success',
      200,
      {
        incomesData: incomesAllData,
        totalIncomesByWaterConsume: waterConsumeByAllPartners,
      });
  }

  private async _getAllIncomes(request: IncomeRequest): Promise<Income[]> {
    return await (
      await firestore().collection(this._collectionName)
        .where('date', '>=', request.dateStart)
        .where('date', '<', request.dateEnd)
        .get()
    ).docs.map((document: QueryDocumentSnapshot<DocumentData>) => {
      return {
        id: document.id,
        ...document.data(),
      } as any;
    });
  }

  private async _getAllPaymentsByAllPartners(request: IncomeRequest): Promise<PayByPartner[]> {
    const partnersSnapshot = await firestore().collection('partner').get();
    const allPayments: PayByPartner[] = [];

    for (const partnerDoc of partnersSnapshot.docs) {
      const paymentsSnapshot = await partnerDoc.ref
        .collection(this._subCollectionName)
        .where('dateToPay', '>=', request.dateStart)
        .where('dateToPay', '<', request.dateEnd)
        .get();

      const payments = paymentsSnapshot.docs.map((document: QueryDocumentSnapshot<DocumentData>) => {
        return {
          partnerId: partnerDoc.id,
          paymentId: document.id,
          ...document.data(),
        } as any;
      });

      allPayments.push(...payments);
    }

    return allPayments;
  }
}
