/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/no-empty-function */
import { HttpsFunction, https } from 'firebase-functions';
import { ResponseUtil } from '../utils/response.util';
import { ReaderRequest } from '../interface/reader-request.interface';
import { Partner } from '../interface/partners/partner.model';
import { firestore } from 'firebase-admin';
import { DocumentData, QueryDocumentSnapshot } from 'firebase-admin/firestore';
import { ResponseApi } from '../interface/response-api.iterface';

export class GetPartnersDataChartService {
  private _resUtil: ResponseUtil;
  private _collectionName: string;

  constructor() {
    this._resUtil = new ResponseUtil();
    this._collectionName = 'partner';
  }

  getPartnersDataToChart(): HttpsFunction {
    return https.onCall(async (request: ReaderRequest) => {
      const partners: Partner[] = await this._getAllPartners();
      const partnersDataToChart = await this._buildPartnersDataToChart(partners);
      return await this._getPartnersDataToChartResponse(partnersDataToChart);
    });
  }

  private _getPartnersDataToChartResponse(dataToChart: any): ResponseApi {
    return this._resUtil.getResponseData(
      'Incomes get success',
      200,
      dataToChart
    );
  }

  private async _getAllPartners(): Promise<Partner[]> {
    return await (
      await firestore().collection(this._collectionName).get()
    ).docs.map((document: QueryDocumentSnapshot<DocumentData>) => {
      return {
        id: document.id,
        ...document.data(),
      } as any;
    });
  }

  private _buildPartnersDataToChart(partners: Partner[]): Promise<any> {
    return new Promise((resolve, reject) => {
      const dataToChart: any = [];

      const partnersActive: any = {
        name: 'Activos',
        value: partners.reduce((acumulador, elemento) => {
          if (elemento.active) {
            return acumulador + 1;
          }
          return acumulador;
        }, 0),
      };

      const partnersInactive: any = {
        name: 'Inactivos',
        value: partners.filter((partner: Partner) => !partner.active).length,
      };

      const partnersInCut: any = {
        name: 'En Corte',
        value: partners.filter((partner: Partner) => !!partner.waterCut?.active).length,
      };

      dataToChart.push(partnersActive);
      dataToChart.push(partnersInactive);
      dataToChart.push(partnersInCut);

      resolve(dataToChart);
    });
  }
}
