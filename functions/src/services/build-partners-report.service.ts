/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-var-requires */
import { HttpsFunction, https } from 'firebase-functions';
import { ReaderRequest } from '../interface/reader-request.interface';
import { Partner } from '../interface/partners/partner.model';
import { DocumentData, QueryDocumentSnapshot } from 'firebase-admin/firestore';
import { firestore } from 'firebase-admin';
import { PartnerAction } from '../interface/partners/partner-action.model';
const PDFDocument = require('pdfkit-table');

export class BuildPartnersReportService {
  private _collectionName: string;
  private _subCollectionName: string;

  constructor() {
    this._collectionName = 'partner';
    this._subCollectionName = 'action';
  }

  buildPartnersReport(): HttpsFunction {
    return https.onCall(async (partnersReportIncomes: ReaderRequest) => {
      const partnerList: Partner[] = await this._getAllPartners(partnersReportIncomes);
      const partnerListWithActions: Promise<Partner>[] = await this._getPartnersActions(partnerList);
      const partnerLisActions: Partner[] = await Promise.all(partnerListWithActions);
      const result = await this.generatePDF(partnerLisActions, partnersReportIncomes);
      return result;
    });
  }

  private async _getAllPartners(request: ReaderRequest): Promise<Partner[]> {
    const location = request.location as string;
    return await (
      await firestore().collection(this._collectionName).orderBy('last_name', 'asc').get()
    ).docs.map((doc: QueryDocumentSnapshot<DocumentData>) => {
      return {
        id: doc.id,
        ...doc.data(),
      } as Partner;
    }).filter((partner) => request.location !== 'Todos los Socios' ? partner.community.location === location : partner);
  }

  private async _getPartnersActions(partners: DocumentData[]): Promise<Promise<Partner>[]> {
    return await (partners.map(async (partner: DocumentData) => {
      const actionsByPartner: firestore.QuerySnapshot<firestore.DocumentData> =
        await await firestore()
          .collection(this._collectionName)
          .doc(partner.id)
          .collection(this._subCollectionName)
          .get();
      return {
        ...partner,
        partnerAction: (await actionsByPartner).docs.map(
          (doc: firestore.DocumentData) => {
            return {
              id: doc.id,
              ...doc.data(),
            } as PartnerAction;
          }
        ),
      } as Partner;
    }));
  }

  private async generatePDF(partners: Partner[], partnersReportIncomes: ReaderRequest): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        const pdfDoc = new PDFDocument({ margin: 30 });
        const chunks: any[] = [];

        pdfDoc.text(('AGUA POTABLE SAN PABLO'), {
          align: 'center',
        });
        pdfDoc.text((' '), {
          align: 'center',
        });
        pdfDoc.text(('Lista de Socios'.concat(' - ').concat(partnersReportIncomes.location)), {
          align: 'center',
        });

        const table: any = {
          title: ' ',
          headers: [
            {
              label: 'Nro', property: '', width: 40, renderer: function(value: any, indexColumn: number, indexRow: number, row: Partner) {
                return indexRow + 1;
              },
            },
            { label: 'Apellidos', property: 'last_name', width: 120, renderer: null },
            { label: 'Nombres', property: 'name', width: 100, renderer: null },
            {
              label: 'C.I.', property: '', width: 80, renderer: function(value: any, indexColumn: number, indexRow: number, row: Partner) {
                if (row && row.identity_card) {
                  return row.identity_card.ci_number.toString().concat(' ' + row.identity_card.complement).concat('- ').concat(row.identity_card.issued);
                }
                return '';
              },
            },
            {
              label: 'Medidor', property: '', width: 80, renderer: function(value: any, indexColumn: number, indexRow: number, row: Partner) {
                if (row && row.partnerAction[0].water_meter?.measurer) {
                  return row.partnerAction[0].water_meter?.measurer?.water_meter_number ? row.partnerAction[0].water_meter?.measurer?.water_meter_number : '';
                }
                return '';
              },
            },
            {
              label: 'Comunidad', property: '', width: 80, renderer: function(value: any, indexColumn: number, indexRow: number, row: Partner) {
                if (row && row.community) {
                  return row.community.location;
                }
                return '';
              },
            },
            {
              label: 'Alc.', property: '', width: 50, renderer: function(value: any, indexColumn: number, indexRow: number, row: Partner) {
                return row.sewerage ? 'Si' : 'No';
              },
            },
          ],
          datas: partners,
        };
        pdfDoc.table(table, {
          columnSpacing: 0,
          padding: 0,
          align: 'center',
          prepareHeader: () => pdfDoc.font('Helvetica-Bold').fontSize(10),
          prepareRow: (row: any, indexColumn: number, indexRow: any, rectRow: any, rectCell: any) => {
            // if (Array.isArray(row)) {
            //   pdfDoc.font('Helvetica-Bold').fontSize(9);
            // } else {
            pdfDoc.font('Helvetica').fontSize(8);
            // }
          },
        });

        pdfDoc.on('data', (chunk: any) => chunks.push(chunk));

        pdfDoc.on('end', () => {
          const pdfBuffer = Buffer.concat(chunks);

          const filename = 'example.pdf';
          const response: any = {
            headers: {
              'Content-Type': 'application/pdf',
              'Content-Disposition': `inline; filename="${filename}"`,
              'Content-Length': pdfBuffer.length.toString(),
            },
            data: pdfBuffer.toString('base64'),
          };
          resolve(response);
        });

        pdfDoc.end();
      } catch (error) {
        console.error('Error generando el PDF:', error);
        reject(error);
      }
    });
  }
}
