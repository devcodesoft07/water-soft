/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-var-requires */
import { HttpsFunction, https } from 'firebase-functions';
import { ExpensesReportRequest } from '../interface/expenses/expenses-report-request.interface';
import { firestore } from 'firebase-admin';
import { DocumentData, QueryDocumentSnapshot } from 'firebase-admin/firestore';
import { Expense } from '../interface/expenses/expense.interface';
import { ResponseApi } from '../interface/response-api.iterface';
import { ResponseUtil } from '../utils/response.util';
const PDFDocument = require('pdfkit-table');

export class BuildExpensesReportService {
  private _resUtil: ResponseUtil;
  private _collectionName: string;

  constructor() {
    this._resUtil = new ResponseUtil();
    this._collectionName = 'expenses';
  }

  buildExpensesReportByMonth(): HttpsFunction {
    return https.onCall(async (expensesReportRequest: ExpensesReportRequest) => {
      const expensesByDate: Expense[] = await this._getExpensesByDate(expensesReportRequest);
      const expensesDataToTable = await this._generateDataToTable(expensesByDate);
      const result = await this.generatePDF(expensesDataToTable, expensesReportRequest);
      return await this._getExpensesReport(result);
    });
  }

  private async _getExpensesReport(documentBase64: string): Promise<ResponseApi> {
    return this._resUtil.getResponseData(
      'Expenses Report Success',
      200,
      documentBase64,
    );
  }

  private async _getExpensesByDate(request: ExpensesReportRequest): Promise<Expense[]> {
    return await (
      await firestore().collection(this._collectionName)
        .where('date', '>=', request.startDate)
        .where('date', '<', request.endDate)
        .get()
    ).docs.map((document: QueryDocumentSnapshot<DocumentData>) => {
      return {
        id: document.id,
        ...document.data(),
      } as Expense;
    });
  }

  private async generatePDF(expenses: any, request: ExpensesReportRequest): Promise<string> {
    return new Promise((resolve, reject) => {
      try {
        const pdfDoc = new PDFDocument({ margin: 30 });
        const chunks: any[] = [];

        pdfDoc.text(`${'Reporte de Gastos del mes de'.concat(` ${request.monthName} `).concat(new Date(request.startDate).getFullYear().toString())}`, {
          align: 'center',
        });

        const table: any = {
          title: ' ',
          headers: [
            { label: 'Fecha', property: 'date', width: 100, renderer: null },
            { label: 'Categoría', property: 'category', width: 100, renderer: null },
            { label: 'Detalle', property: 'detail', width: 200, renderer: null },
            { label: 'Monto $', property: 'amount1', width: 70, renderer: null },
            { label: 'Monto Bs', property: 'amount2', width: 70, renderer: null },
          ],
          datas: expenses,
          rows: [
            [
              'TOTAL',
              '',
              '',
              `${expenses.reduce((total: number, currentValue: any) => total + (Number(currentValue.amount1) || 0), 0)} $us`,
              `${expenses.reduce((total: number, currentValue: any) => total + (Number(currentValue.amount2) || 0), 0)} Bs`,
            ],
          ],
        };
        pdfDoc.table(table, {
          columnSpacing: 8,
          padding: 10,
          align: 'center',
          prepareHeader: () => pdfDoc.font('Helvetica-Bold').fontSize(10),
          prepareRow: (row: any, indexColumn: number, indexRow: any, rectRow: any, rectCell: any) => {
            if (Array.isArray(row)) {
              pdfDoc.font('Helvetica-Bold').fontSize(9);
            } else {
              pdfDoc.font('Helvetica').fontSize(8);
            }
          },
        });

        pdfDoc.on('data', (chunk: any) => chunks.push(chunk));

        pdfDoc.on('end', () => {
          const pdfBuffer = Buffer.concat(chunks);
          resolve(pdfBuffer.toString('base64'));
        });

        pdfDoc.end();
      } catch (error) {
        console.error('Error generando el PDF:', error);
        reject(error);
      }
    });
  }

  private _generateDataToTable(expenses: Expense[]): Promise<any[]> {
    return new Promise((resolve, reject) => {
      try {
        const expensesDataTable = [];
        for (const expense of expenses) {
          const dataToTable = {
            date: `${new Date(expense.date).toLocaleDateString()}`,
            category: `${expense.expense_category.description}`,
            detail: `${expense.detail}`,
            amount1: `${expense.coin.includes('$us') ? expense.amount : 0}`,
            amount2: `${expense.coin.includes('Bs') ? expense.amount : 0}`,
          };
          expensesDataTable.push(dataToTable);
        }
        resolve(expensesDataTable);
      } catch (error) {
        reject(error);
      }
    });
  }
}
