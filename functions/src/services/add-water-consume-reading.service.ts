/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/no-empty-function */
import { HttpsFunction, https } from 'firebase-functions';
import { ResponseUtil } from '../utils/response.util';
import { AddWaterConsumeReadingByPartnerRequest } from '../interface/water-consume/water-consume-by-partner-request.interface';
import { PayByPartner } from '../interface/partners/pay-by-partner.interface';
import { firestore } from 'firebase-admin';
import { ResponseApi } from '../interface/response-api.iterface';
import { DocumentData, QueryDocumentSnapshot } from 'firebase-admin/firestore';

export class AddWaterConsumeReadingService {
  private _resUtil: ResponseUtil;
  private _collectionName: string;
  private _subCollectionName: string;
  constructor() {
    this._resUtil = new ResponseUtil();
    this._collectionName = 'partner';
    this._subCollectionName = 'water-consume';
  }

  addWaterConsumeReadingByPartnerByMonth(): HttpsFunction {
    return https.onCall(async (request: AddWaterConsumeReadingByPartnerRequest) => {
      const payByPartner: PayByPartner[] = await this._getAllPaymentsByPartner(request);
      return await this._addWaterConsumeByPartnerByMonth(payByPartner, request);
    });
  }

  private async _addWaterConsumeByPartnerByMonth(payByPartner: PayByPartner[], request: AddWaterConsumeReadingByPartnerRequest): Promise<ResponseApi> {
    // const partnerData: Partner = await this._getPartnerData(request);
    const waterConsumeByMonths: PayByPartner[] = await Promise.all(payByPartner);
    let message: string;
    let code: number;
    if (waterConsumeByMonths.length === 0) {
      this._addWaterConsumeReadingByPartner(request);
      message = 'La lecturación se agrego correctamente';
      code = 200;
    } else {
      message = 'El socio ya cuenta con la lecturación del mes ingresado';
      code = 400;
    }
    return this._resUtil.getResponseData(
      message,
      code,
      {
        waterConsumeByMonths: waterConsumeByMonths,
        // partnerData: partnerData,
      });
  }

  private async _getAllPaymentsByPartner(request: AddWaterConsumeReadingByPartnerRequest): Promise<PayByPartner[]> {
    return await (
      await firestore().collection(this._collectionName).doc(request.partnerId).collection(this._subCollectionName)
        .where('dateToRead', '>=', request.dateStart)
        .where('dateToRead', '<=', request.dateEnd)
        .get()
    ).docs.map((document: QueryDocumentSnapshot<DocumentData>) => {
      return {
        id: document.id,
        ...document.data(),
      } as PayByPartner;
    });
  }

  private _addWaterConsumeReadingByPartner(request: AddWaterConsumeReadingByPartnerRequest): void {
    firestore()
      .collection(this._collectionName).doc(request.partnerId)
      .collection(this._subCollectionName).add(request.data);
  }
}
