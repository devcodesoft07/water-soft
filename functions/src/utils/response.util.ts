/* eslint-disable linebreak-style */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { ResponseApi } from '../interface/response-api.iterface';

export class ResponseUtil {
  public getResponseData(message: string, status: number, data: any): ResponseApi {
    return {
      message,
      status,
      data,
    };
  }
}
