/* eslint-disable linebreak-style */

import * as admin from 'firebase-admin';
import { AddPaymentsService } from './services/add-payments.service';
import { HttpsFunction } from 'firebase-functions';
import { GetPaymentsService } from './services/get-payments.service';
import { GetWaterConsumeByPartnerService } from './services/get-water-consume-by-partner.service';
import { GetIncomesService } from './services/get-incomes.service';
import { BuildExpensesReportService } from './services/build-expenses-report.service';
import { BuildIncomesReportService } from './services/build-incomes-report.service';
import { GetPaymentsByPartnerService } from './services/get-payments-by-partner.service';
import { GetPartnersDataChartService } from './services/get-partners-data-chard.service';
import { BuildPartnersReportService } from './services/build-partners-report.service';
import { AddWaterConsumeReadingService } from './services/add-water-consume-reading.service';


admin.initializeApp();

// const addMessageService: AddMessageService = new AddMessageService();
const addPaymentsService: AddPaymentsService = new AddPaymentsService();
const getPaymentsService: GetPaymentsService = new GetPaymentsService();
const getWaterConsumeByPartnerService: GetWaterConsumeByPartnerService = new GetWaterConsumeByPartnerService();
const buildExpensesReportService: BuildExpensesReportService = new BuildExpensesReportService();
const buildIncomesReportService: BuildIncomesReportService = new BuildIncomesReportService();
const getIncomesService: GetIncomesService = new GetIncomesService();
const getPaymentsByPartner: GetPaymentsByPartnerService = new GetPaymentsByPartnerService();
const getPartnersDataChartService: GetPartnersDataChartService = new GetPartnersDataChartService();
const buildPartnersReportService: BuildPartnersReportService = new BuildPartnersReportService();
const addWaterConsumeReadingService: AddWaterConsumeReadingService = new AddWaterConsumeReadingService();

// export const hello: HttpsFunction = ce.hello();
// export const addMessage: HttpsFunction = addMessageService.addMessages();
export const getPayments: HttpsFunction = getPaymentsService.getAllPaymentsByMonth();
export const getPaymentsByPartnerId: HttpsFunction = getPaymentsByPartner.getAllPaymentsByPartnerByMonth();
export const getIncomesReportByMonth: HttpsFunction = buildIncomesReportService.buildIncomesReportByMonth();
export const getWaterConsumeByPartnerForDate: HttpsFunction = getWaterConsumeByPartnerService.getWaterConsumeByPartner();
export const getExpensesReportByMonth: HttpsFunction = buildExpensesReportService.buildExpensesReportByMonth();
export const getIncomesServiceByDate: HttpsFunction = getIncomesService.getIncomesByDate();
export const addPayments: HttpsFunction = addPaymentsService.addPaymentsToPartner();
export const getPartnersDataToChart: HttpsFunction = getPartnersDataChartService.getPartnersDataToChart();
export const getPartnersReportService: HttpsFunction = buildPartnersReportService.buildPartnersReport();
export const addWaterConsumeReadingByPartner: HttpsFunction = addWaterConsumeReadingService.addWaterConsumeReadingByPartnerByMonth();
