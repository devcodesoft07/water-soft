export const environment = {
  production: false,
  // apiUrl: 'http://127.0.0.1:8000/api/',
  apiUrl: 'https://fierce-gorge-01018.herokuapp.com/api/',
  //apiUrlFireFunctions: 'http://127.0.0.1:5001/water-soft-606c9/us-central1',
  apiUrlFireFunctions: 'https://us-central1-water-soft-606c9.cloudfunctions.net',
  firebaseConfig: {
    apiKey: 'AIzaSyAzouxDwWC6fzJmMsOdp47VItsG7QACugI',
    authDomain: 'water-soft-606c9.firebaseapp.com',
    projectId: 'water-soft-606c9',
    storageBucket: 'water-soft-606c9.appspot.com',
    messagingSenderId: '218304930907',
    appId: '1:218304930907:web:d658e8f98909f7ef7acb7e',
    measurementId: 'G-5ZJX0CJH0Q'
  }
};
