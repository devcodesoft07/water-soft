export const environment = {
  production: true,
  apiUrlFireFunctions: 'https://us-central1-water-soft-606c9.cloudfunctions.net',
  apiUrl: 'https://fierce-gorge-01018.herokuapp.com/api/',
  firebaseConfig: {
    apiKey: "AIzaSyCE-EsUf9l1ildX9LLZz4qsONg8HbbNpKs",
    authDomain: "water-soft-prod.firebaseapp.com",
    projectId: "water-soft-prod",
    storageBucket: "water-soft-prod.appspot.com",
    messagingSenderId: "996169911697",
    appId: "1:996169911697:web:779cf579c3348507a5de30",
    // measurementId: 'G-5ZJX0CJH0Q'
  }
};