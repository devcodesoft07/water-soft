import { Component, OnInit } from '@angular/core';
import { AuthConstants } from '@core/config/auth-constanst';
import { ERole, IUser } from '@core/models/admin/user.model';
import { MenuController } from '@ionic/angular';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
})
export class AdminPage implements OnInit {
  date: Date = new Date();
  public user: IUser;
  public isReader: boolean;

  constructor(
    private menuController: MenuController,
    private _cookieService: CookieService
  ) { }

  ngOnInit() {
    this.user = JSON.parse(this._cookieService.get(AuthConstants.user));
    this.isReader = this.user.role === ERole.READER;
  }

  closeMenu(): void {
    this.menuController.close('main-menu');
  }
}
