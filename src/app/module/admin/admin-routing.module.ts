import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminPage } from './admin.page';
import { AuthGuard } from '@shared/guard/auth/auth.guard';
import { ERole } from '@core/models/admin/user.model';

const routes: Routes = [
  {
    path: '',
    component: AdminPage,
    children: [
      {
        path: 'inicio',
        canActivate: [AuthGuard],
        data: { roles: [ERole.ADMIN] },
        loadChildren: () => import('./page/admin-home/admin-home.module').then(m => m.AdminHomePageModule)
      },
      {
        path: 'ingresos',
        canActivate: [AuthGuard],
        data: { roles: [ERole.ADMIN] },
        loadChildren: () => import('./page/incomes/incomes.module').then(m => m.IncomesPageModule)
      },
      {
        path: 'gastos',
        canActivate: [AuthGuard],
        data: { roles: [ERole.ADMIN] },
        loadChildren: () => import('./page/expenses/expenses.module').then(m => m.ExpensesPageModule)
      },
      {
        path: 'medidores',
        canActivate: [AuthGuard],
        data: { roles: [ERole.ADMIN] },
        loadChildren: () => import('./page/water-mater/water-mater.module').then(m => m.WaterMaterPageModule)
      },
      {
        path: 'socios',
        canActivate: [AuthGuard],
        data: { roles: [ERole.ADMIN] },
        loadChildren: () => import('./page/partners/partners.module').then(m => m.PartnersPageModule)
      },
      {
        path: 'lecturador',
        canActivate: [AuthGuard],
        data: { roles: [ERole.READER, ERole.ADMIN] },
        loadChildren: () => import('./page/reader/reader.module').then(m => m.ReaderPageModule)
      },
      {
        path: 'actas',
        canActivate: [AuthGuard],
        data: { roles: [ERole.ADMIN] },
        loadChildren: () => import('./page/document/document.module').then(m => m.DocumentPageModule)
      },
      // {
      //   path: 'multas',
      //   canActivate: [AuthGuard],
      //   data: { roles: [ERole.ADMIN] },
      //   loadChildren: () => import('./page/report/report.module').then(m => m.ReportPageModule)
      // },
      {
        path: 'precios',
        canActivate: [AuthGuard],
        data: { roles: [ERole.ADMIN] },
        loadChildren: () => import('./page/perfil/perfil.module').then(m => m.PerfilPageModule)
      },
      { path: '', pathMatch: 'full', redirectTo: 'medidores' }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminPageRoutingModule { }
