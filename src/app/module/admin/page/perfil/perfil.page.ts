import { Component, OnInit, OnDestroy } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ConstanstEnum } from '@core/enum/constants.enum';
import { PriceConfig } from '@core/models/price-config.model';
import { PriceService } from '@core/services/admin/price.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit, OnDestroy {
  price: PriceConfig;
  public dataForm: UntypedFormGroup;
  private subscription: Subscription = new Subscription();
  constructor(
    private priceService: PriceService,
    private formBuilder: UntypedFormBuilder
  ) {
    this.buildForm();
  }

  ngOnInit() {
    this.getDataPrice();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  updatePriceProperty(value: number, keyFormProperty: string): void {
    if (this.dataForm.controls[keyFormProperty].valid) {
      this.dataForm.controls[keyFormProperty].setValue(Number(value));
      const priceUpdated: PriceConfig = {
        ...this.price,
        [keyFormProperty]: this.dataForm.controls[keyFormProperty].value
      };

      this.priceService.update(priceUpdated).then().catch();
    }
  }

  private getDataPrice(): void {
    const priceSubscription: Subscription = this.priceService.getData().subscribe(
      (priceData: PriceConfig) => {
        this.price = priceData;
        this.dataForm.patchValue(this.price);
      }
    );
    this.subscription.add(priceSubscription);
  }

  get waterMeterPrice(): any {
    return this.dataForm.get('water_meter_price');
  }

  get actionPrice(): any {
    return this.dataForm.get('action_price');
  }

  get debtsPrice(): any {
    return this.dataForm.get('debts');
  }

  get seweragePrice(): any {
    return this.dataForm.get('sewerage_price');
  }

  get firstWaterPrice(): any {
    return this.dataForm.get('first_water_price');
  }

  get secondWaterPrice(): any {
    return this.dataForm.get('second_water_price');
  }

  get thirdWaterPrice(): any {
    return this.dataForm.get('third_water_price');
  }

  get minimalPrice(): any {
    return this.dataForm.get('minimal_price');
  }

  private buildForm(): void {
    this.dataForm = this.formBuilder.group({
      action_price: [, Validators.pattern(ConstanstEnum.PATTERN_ONLY_NUMBER)],
      debts: [, Validators.pattern(ConstanstEnum.PATTERN_ONLY_NUMBER)],
      first_range: [, Validators.pattern(ConstanstEnum.PATTERN_ONLY_NUMBER)],
      first_water_price: [, Validators.pattern(ConstanstEnum.PATTERN_ONLY_NUMBER)],
      second_range: [, Validators.pattern(ConstanstEnum.PATTERN_ONLY_NUMBER)],
      second_water_price: [, Validators.pattern(ConstanstEnum.PATTERN_ONLY_NUMBER)],
      third_water_price: [, Validators.pattern(ConstanstEnum.PATTERN_ONLY_NUMBER)],
      water_meter_price: [, Validators.pattern(ConstanstEnum.PATTERN_ONLY_NUMBER)],
      minimal_price: [, Validators.pattern(ConstanstEnum.PATTERN_ONLY_NUMBER)],
      sewerage_price: [, Validators.pattern(ConstanstEnum.PATTERN_ONLY_NUMBER)]
    });
  }

  public updateDataPrices(event: any): void {
    this.dataForm.patchValue(this.price);
  }
}
