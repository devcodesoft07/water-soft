import { Component, HostListener, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-config-input',
  templateUrl: './config-input.component.html',
  styleUrls: ['./config-input.component.scss']
})
export class ConfigInputComponent implements OnInit {
  @Input() image: string;
  @Input() title: string;
  @Input() subTitle: string;
  @Input() typeInput: string;
  @Input() value: number;
  @Input() firstValue: number;
  @Input() secondValue: number;
  @Input() thirdValue: number;
  @Input() minimal: number;
  @Input() firstRange: number;
  @Input() secondRange: number;
  @Input() placeholder: string;
  @Input() currency: string;
  @Input() multiple = false;
  @Input() hasMargin = true;
  @Input() formControlPrice: FormControl;
  @Input() firstRangeControlPrice: FormControl;
  @Input() secondRangeControlPrice: FormControl;
  @Input() thirdRangeControlPrice: FormControl;
  @Input() minimalControlPrice: FormControl;

  @Output() valueEventEmitter: EventEmitter<number> = new EventEmitter<number>();
  @Output() firstPriceEventEmitter: EventEmitter<number> = new EventEmitter<number>();
  @Output() secondPriceEventEmitter: EventEmitter<number> = new EventEmitter<number>();
  @Output() thirdPriceEventEmitter: EventEmitter<number> = new EventEmitter<number>();
  @Output() minimalPriceEventEmitter: EventEmitter<number> = new EventEmitter<number>();
  @Output() updateDataPricesEventEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();


  showOnlyInput = false;
  showIconEdit = false;
  constructor() {
    this.image = 'assets/images/config/water.svg';
    this.title = 'Acción de un socio';
    this.subTitle = 'El precio del agua varia segun la cantidad de metros cúbicos consumidos:';
    this.value = 150;
    this.firstValue = 0.5;
    this.secondValue = 0.5;
    this.thirdValue = 0.5;
    this.typeInput = 'text';
    this.currency = 'Bs. ';
    this.placeholder = 'Ingresa el precio del item';
  }

  @HostListener('mouseenter', ['$event'])
  showIconToEdit(): void {
    this.showIconEdit = true;
  }

  @HostListener('mouseleave', ['$event'])
  hideIconToEdit(): void {
    this.showIconEdit = false;
  }

  // @HostListener('focusout', ['$event.target.value'])
  // onBlur() {
  //   this.showOnlyInput = false;
  // }

  ngOnInit(): void {
  }

  public updateDataPrices(): void {
    this.showOnlyInput = this.showOnlyInput ? false : true;
    this.updateDataPricesEventEmitter.emit(true);
  }
}
