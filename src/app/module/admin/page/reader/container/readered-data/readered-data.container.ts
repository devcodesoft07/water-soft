/* eslint-disable @angular-eslint/component-class-suffix */
import { Component, Input, OnInit } from '@angular/core';
import { Gender } from '@core/enum/gender.enum';
import { PartnerReader } from '@core/models/admin/partners/partner.model';

@Component({
  selector: 'app-readered-data',
  templateUrl: './readered-data.container.html',
  styleUrls: ['./readered-data.container.scss']
})
export class ReaderedDataContainer implements OnInit {
  @Input() partner: PartnerReader;
  @Input() index: number;
  @Input() actualIndexSelected: number;
  @Input() dataPrice: number;
  gender = Gender;
  public titleInfo: string;
  constructor() {
    this.titleInfo = '';
  }

  public ngOnInit(): void {
    if (this.partner?.partner?.waterCut) {
      this.titleInfo = this.partner?.partner?.waterCut.cut_non_payment ? 'En corte por falta de pago' : 'Medidor Averiado';
    }
  }



}
