/* eslint-disable @angular-eslint/component-class-suffix */
import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Partner, PayByPartner } from '@core/models/admin/partners/partner.model';
import { WaterConsumed } from '@core/models/admin/water-consumed/water-consumed.interface';
import { ReaderFormComponent } from '../../components/reader-form/reader-form.component';
import { ModalController } from '@ionic/angular';
import { PriceConfig } from '@core/models/price-config.model';
import { OptionSelectedByUserEnum } from '@core/enum/option-selected-by-user.enum';
import { WaterCutFormComponent } from '../../components/water-cut-form/water-cut-form.component';
import { WaterMeterConsumedFireService } from '@core/services/admin/reader/water-meter-consumed-fire.service';
import { delay, take } from 'rxjs/operators';
import { PartnerWaterConsumeFireService } from '@core/services/admin/partner/partner-water-consume-fire.service';
import moment from 'moment';
import { NgxScannerQrcodeComponent, ScannerQRCodeConfig, ScannerQRCodeDevice, ScannerQRCodeResult } from 'ngx-scanner-qrcode';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';

@Component({
  selector: 'app-water-consumed-data',
  templateUrl: './water-consumed-data.container.html',
  styleUrls: ['./water-consumed-data.container.scss'],
})
export class WaterConsumedDataContainer implements OnInit {
  @Input() partnerSelected: Partner;
  @Input() dataPrice: (PriceConfig & { id: string }) | undefined;
  @Input() waterConsumedList: PayByPartner[];
  @Input() isReaderView: boolean;
  @Input() lastWaterConsume: WaterConsumed;
  @Input() isViewSmall: boolean;
  allWaterConsumeByPartner: WaterConsumed[];
  public isModalOpen = false;
  public newCurrentMonthWaterConsume: WaterConsumed;
  public showLoading: boolean;

  @Output() reloadWaterConsumedByPartner: EventEmitter<boolean>;
  displayedColumns: string[];
  optionSelectedByUser = OptionSelectedByUserEnum;
  public scannerEnabled: boolean = true;
  public today = new Date();

  public config: ScannerQRCodeConfig = {
    constraints: {
      video: {
        width: window.innerWidth,
        height: window.innerHeight // https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
      }
    },
    canvasStyles: {
      font: '0px serif',
      lineWidth: 3,
      fillStyle: 'transparent',
      strokeStyle: 'transparent',
    } as any // https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D
  };

  @ViewChild('action') action: NgxScannerQrcodeComponent;

  constructor(private modalController: ModalController,
    private waterMeterConsumedFireService: WaterMeterConsumedFireService,
    private _partnerWaterConsumeFireService: PartnerWaterConsumeFireService
  ) {
    this.displayedColumns = ['date', 'consume', 'amount', 'sewerage', 'debts', 'total', 'action'];
    this.reloadWaterConsumedByPartner = new EventEmitter<boolean>();
    this.isReaderView = false;
  }

  ngOnInit(): void {
    this._getWaterConsumeByPartner();
  }

  ngAfterViewInit(): void {
    this.action?.isReady.pipe(delay(1000)).subscribe(() => {
      this.handle(this.action, 'start');
    });
  }

  private _getWaterConsumeByPartner(): void {
    if (this.isReaderView && this.partnerSelected && this.partnerSelected.id) {
      this.waterMeterConsumedFireService.getAll(this.partnerSelected)
        .pipe(
          take(1)
        )
        .subscribe(
          (waterConsumedByPartner: WaterConsumed[]) => {
            this.allWaterConsumeByPartner = waterConsumedByPartner.filter((waterConsume: WaterConsumed) => waterConsume.isPay === false);
            this.lastWaterConsume = waterConsumedByPartner[0];
          }
        );
    }
  }

  async showReaderFormComponent(optionSelected: OptionSelectedByUserEnum, waterConsumed: WaterConsumed): Promise<void> {
    const cssCustomClass: string = optionSelected == OptionSelectedByUserEnum.PAY ? 'custom-modal-pay-water-consume' : '';

    const modal = await this.modalController.create({
      component: ReaderFormComponent,
      componentProps: {
        partner: this.partnerSelected,
        prices: this.dataPrice,
        optionSelectedByUser: optionSelected,
        waterConsume: waterConsumed
      },
      mode: 'ios',
      backdropDismiss: false,
      cssClass: cssCustomClass
    });

    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data.save) {
        this.reloadWaterConsumedByPartner.emit(dataReturned.data.save);
      }
    });

    await modal.present();
  }

  async registerWaterCut(): Promise<void> {
    const modal = await this.modalController.create({
      component: WaterCutFormComponent,
      componentProps: {
        partner: this.partnerSelected,
        liableToCut: this.waterConsumedList.length > 3 ? true : false
      },
      mode: 'ios',
      backdropDismiss: false
    });
    await modal.present();
  }

  public closeModal(): void {
    this.modalController.dismiss({
      save: false,
    });

    if (this.action?.isStart) {
      this.handle(this.action, 'stop');
    }
  }

  async addNewPayment(): Promise<void> {
    const modal = await this.modalController.create({
      component: ReaderFormComponent,
      componentProps: {
        partner: this.partnerSelected,
        prices: this.dataPrice,
        waterConsume: this.lastWaterConsume
      },
      mode: 'ios',
      backdropDismiss: false
    });

    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data.data) {
        this.newCurrentMonthWaterConsume = dataReturned.data.data
        this.isModalOpen = true;

        this._generatePDF();
        this.showLoading = true;
      }
    });

    await modal.present();
  }

  public scanSuccessHandler($event: any) {
    this.scannerEnabled = false;
    /*const appointment = new Appointment($event);
    this.logService.logAppointment(appointment).subscribe(
      (result: OperationResponse) => {
        this.information = $event;
        this.transports = result.object;
        this.cd.markForCheck();
      },
      (error: any) => {
        this.information = "Ha ocurrido un error por favor intentalo nuevamente ... ";
        this.cd.markForCheck();
      }); */
    const dataToPartner: Partner = JSON.parse($event);

    this.partnerSelected = {
      ...dataToPartner
    };
    this.readByPartner();
  }

  public readByPartner(): void {
    const startDate = moment(new Date()).startOf('month').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
    const endDate = moment(new Date()).endOf('month').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');

    this._partnerWaterConsumeFireService.getWaterConsumeByPartnerIdForDate(startDate, endDate, this.partnerSelected?.id)
      .pipe(
        take(1)
      )
      .subscribe((responseApi: any) => {
        this.partnerSelected = responseApi.data.partnerData;
        this.waterConsumedList = responseApi.data.waterConsumeByMonths;
        this._getWaterConsumeByPartner();
      })

    if (this.action.isStart) {
      this.handle(this.action, 'stop');
    }
  }

  public onEvent(event: ScannerQRCodeResult[], action?: any): void {
    event?.length && action && action.pause(); // Detect once and pause scan!

    const dataToPartner: Partner = JSON.parse(event[0].value);

    this.partnerSelected = {
      ...dataToPartner
    };
    this.readByPartner();

    //   let binArrayToString = function(binArray) {
    //     let str = "";
    //     for (let i = 0; i < binArray.length; i++) {        
    //         str += String.fromCharCode(parseInt(binArray[i]));
    //     }
    //     return str;
    // }
    // console.log('utf8ArrayToString',binArrayToString(e[0].data));
  }

  public handle(action: any, fn: string): void {
    const playDeviceFacingBack = (devices: ScannerQRCodeDevice[]) => {
      // front camera or back camera check here!
      const device = devices.find(f => (/back|rear|environment/gi.test(f.label))); // Default Back Facing Camera
      action.playDevice(device ? device.deviceId : devices[1].deviceId);
    }

    if (fn === 'start') {
      action[fn](playDeviceFacingBack).subscribe((r: any) => console.log(fn, r), alert);
    } else {
      action[fn]().subscribe((r: any) => console.log(fn, r), alert);
    }
  }


  private _generatePDF(): void {
    setTimeout(() => {
      let data = document.getElementById('contentToConvert');
      html2canvas(data, { scale: 4, width: 700, height: 300, logging: false }).then(canvas => {
        let imgWidth = 185;
        let imgHeight = 0;

        const contentDataURL = canvas.toDataURL('image/png', 1.0);

        let pdf = new jsPDF('l', 'mm', [140, 70]);
        pdf.addImage(contentDataURL, 'PNG', 0, 0, imgWidth, imgHeight)

        window.open(pdf.output('bloburl') as any, '_blank');
        this.showLoading = false;

        this.modalController.dismiss({
          save: true,
        });
      });
    }, 100);
  }
}
