import { FormGroup, UntypedFormBuilder, Validators } from '@angular/forms';
import { ConstanstEnum } from '@core/enum/constants.enum';

export class ReaderFormValidator {
  readerForm!: FormGroup;
  today: string = new Date().toISOString();
  constructor(public formBuilder: UntypedFormBuilder) {
    this.buildForm();
  }
  private buildForm(): void {
    this.readerForm = this.formBuilder.group({
      amount: [
        { value: '', disabled: true },
        [
          Validators.required,
        ],
      ],
      readering_last: ['', [Validators.required, Validators.pattern(ConstanstEnum.PATTERN_ONLY_NUMBER)]],
      readering_actual: ['', [Validators.required, Validators.pattern(ConstanstEnum.PATTERN_ONLY_NUMBER)]],
      water_consume: ['', [Validators.required]],
      dateToPay: [''],
      dateToRead: [this.today, [Validators.required]],
      debts: [''],
      sewerage: [0],
      isPay: [false]
    });
  }
}
