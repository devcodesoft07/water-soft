import { Component, OnInit, Input } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { PartnerAction } from '@core/models/admin/partners/partner-action.model';
import { WaterCut } from '@core/models/admin/water-cut/WaterCut';
import { PartnerActionFireService } from '@core/services/admin/action-partner/partner-action-fire.service';
import { WaterCutService } from '@core/services/admin/water-cut/water-cut.service';
import { MessageService } from '@core/services/message/message.service';
import { ModalController } from '@ionic/angular';
import { Partner } from 'functions/src/interface/partners/partner.model';
import moment from 'moment';
import { Subscription } from 'rxjs';
import { SweetAlertOptions } from 'sweetalert2';

@Component({
  selector: 'app-water-cut-form',
  templateUrl: './water-cut-form.component.html',
  styleUrls: ['./water-cut-form.component.scss']
})
export class WaterCutFormComponent implements OnInit {
  @Input() partner!: Partner;
  @Input() liableToCut!: boolean;
  public state!: boolean;
  public waterCutForm: UntypedFormGroup;

  private _propertiesToShow: SweetAlertOptions;
  private _subscription: Subscription;
  private partnerAction: PartnerAction;

  constructor(
    private modalController: ModalController,
    private _formBuilder: UntypedFormBuilder,
    private messageService: MessageService,
    private _partnerActionFireService: PartnerActionFireService,
    private _waterCutService: WaterCutService
  ) {
    this._propertiesToShow = {
      title: '',
      text: 'Verifica que los datos sean correctos, el socio y el medidor se registraran como inactivos',
      showConfirmButton: true,
    };
    this._subscription = new Subscription();
  }

  ngOnInit() {
    this._buildForm();
    this.getPartnerAction();
    this.setDefaultValueToForm();
  }

  private setDefaultValueToForm(): void {
    const dataToPatch: WaterCut = {
      water_cut_date: moment(new Date()).format('YYYY-MM-DD h:mm:ss'),
      reason: this.liableToCut ? 'La deuda del socio es mayor o igual a 3 meses' : 'El medidor asignado al socio se encuentra averiado',
      active: true,
      cut_non_payment: this.liableToCut,
      meter_broken: !this.liableToCut ? true : false,
    } as WaterCut;

    this.waterCutForm.patchValue(dataToPatch);
  }

  private _buildForm(): void {
    this.waterCutForm = this._formBuilder.group({
      water_cut_date: [''],
      reason: [''],
      active: [],
      cut_non_payment: ['', Validators.required],
      meter_broken: ['', Validators.required],
      reconnection_date: ['']
    })
  }

  private getPartnerAction(): void {
    this._partnerActionFireService.getAll(this.partner.id)
      .subscribe(
        (partnerAction: PartnerAction[]) => {
          this.partnerAction = partnerAction[0];
        }
      )
  }

  public closeModal(): void {
    this.modalController.dismiss({
      save: false,
    });
  }

  public registerWaterCut(): void {
    const data: WaterCut = {
      ...this.waterCutForm.value,
      water_meter_id: this.partnerAction.water_meter.measurer.id
    };

    this._propertiesToShow.title = this.liableToCut ? '¿Esta seguro de registrar corte del servicio?' : '¿Esta seguro de registrar el medidor como averiado?';
    const waterMeterSubscription: Subscription = this.messageService.showSwalConfirmAlert(this._propertiesToShow)
      .subscribe(
        (isConfirm: boolean) => {
          if (isConfirm) {
            this._saveData(data);
          }
        }
      );
    this._subscription.add(waterMeterSubscription);
  }

  private _saveData(data: WaterCut): void {
    this._waterCutService.add(this.partnerAction, data)
      .then(() => {
        this.state = false;
        this._propertiesToShow = {
          title: 'Registrado correctamente!',
          text: `Los datos del nuevo medidor se agregaron correctamente.`,
          icon: 'success',
          iconColor: '#0284C9',
          timer: 2000,
          showConfirmButton: false,
          backdrop: false
        };
        this.messageService.showSwalBasicAlert(this._propertiesToShow);
        this.modalController.dismiss({
          save: true
        });
      },
      )
      .catch((error: any) => {
        this.state = false;
        console.log(error);
      }
      );
  }
}