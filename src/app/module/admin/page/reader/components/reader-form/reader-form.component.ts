/* eslint-disable max-len */
/* eslint-disable no-underscore-dangle */
import { Component, OnInit, Input } from '@angular/core';
import { ReaderFormValidator } from '../../validator/reader.form-validator';
import { UntypedFormBuilder } from '@angular/forms';
import { ModalController, ToastController } from '@ionic/angular';
import { Partner } from '@core/models/admin/partners/partner.model';
import { PriceConfig } from '@core/models/price-config.model';
import { PartnerWaterConsumeFireService } from '@core/services/admin/partner/partner-water-consume-fire.service';
import { WaterConsumed } from '@core/models/admin/water-consumed/water-consumed.interface';
import { OptionSelectedByUserEnum } from '@core/enum/option-selected-by-user.enum';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import * as QRCode from 'qrcode';
import moment from 'moment';
import { ResponseApi } from 'functions/src/interface/response-api.iterface';


@Component({
  selector: 'app-reader-form',
  templateUrl: './reader-form.component.html',
  styleUrls: ['./reader-form.component.scss'],
})
export class ReaderFormComponent extends ReaderFormValidator implements OnInit {
  @Input() partner: Partner;
  @Input() optionSelectedByUser: OptionSelectedByUserEnum;
  @Input() waterConsume: WaterConsumed;
  @Input() prices: (PriceConfig & { id: string }) | undefined;
  public loading: boolean;
  priceToPay: number;
  optionToVerify = OptionSelectedByUserEnum;
  debts = false;
  differenceBeetwenDays = 0;
  sewerage_price: number;
  currentReadingLowerPreviousReading: boolean;

  public partnerQrCodeUrl: string;
  public today: string;
  public toolbarTitle: string;

  constructor(
    formBuilder: UntypedFormBuilder,
    private modalController: ModalController,
    private partnerWatrConsumerdFireService: PartnerWaterConsumeFireService,
    private toastController: ToastController
  ) {
    super(formBuilder);
    this.sewerage_price = 0;
    this.currentReadingLowerPreviousReading = false;
    this.priceToPay = 1;
    this.today = moment(new Date()).format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
    this.loading = false;
    this.toolbarTitle = '';
  }

  ngOnInit(): void {
    this._verifyOptionSelectedByUser();
    this.generateQrCodeByPartner();
    this._setTitleToolbar();
  }

  async generateQrCodeByPartner() {
    this.partnerQrCodeUrl = await QRCode.toDataURL(JSON.stringify(this.partner));
  }

  private _setTitleToolbar(): void {
    switch (this.optionSelectedByUser) {
      case OptionSelectedByUserEnum.PAY:
        this.toolbarTitle = 'Cobrar de Consumo de Agua';
        break;
      case OptionSelectedByUserEnum.MODIFY:
        this.toolbarTitle = 'Modificar Datos de Consumo de Agua';
        break;

      default:
        this.toolbarTitle = 'Registrar Nueva Lectura';
        break;
    }
  }

  closeModal() {
    this.modalController.dismiss({
      save: false,
    });
  }

  registerReadering(): void {
    const waterConsumed: WaterConsumed = this.readerForm.getRawValue();
    this.loading = true;

    switch (this.optionSelectedByUser) {
      case OptionSelectedByUserEnum.PAY:

        const updateDataWaterConsume: WaterConsumed = {
          ...waterConsumed,
          id: this.waterConsume.id,
          isPay: true,
          dateToPay: new Date().toISOString() as any
        }
        this.partnerWatrConsumerdFireService
          .update(updateDataWaterConsume, this.partner)
          .then(() => {
            this.loading = false;
            this._generatePDF();
            this.modalController.dismiss({
              save: true
            });
          })
          .catch((error: any) => {
            console.log(error);
          });
        break;
      case OptionSelectedByUserEnum.MODIFY:
        const waterConsumedUpdate: WaterConsumed = { ...waterConsumed, id: this.waterConsume.id }
        this.partnerWatrConsumerdFireService
          .update(waterConsumedUpdate, this.partner)
          .then(() => {
            this.loading = false;
            this.modalController.dismiss({
              save: true
            });
          })
          .catch((error: any) => {
            console.log(error);
          });
        break;

      default:
        const startDate = moment(new Date(waterConsumed.dateToRead as string)).startOf('month').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
        const endDate = moment(new Date(waterConsumed.dateToRead as string)).endOf('month').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
        this.partnerWatrConsumerdFireService.addWaterConsumeReadingByPartner(startDate, endDate, this.partner.id, waterConsumed)
          .subscribe(
            (responseApi: ResponseApi) => {
              if (responseApi.status === 400) {
                this.showToastMessage('bottom', responseApi.message, 'danger');
                this.loading = false;
              }
              if (responseApi.status === 200) {
                this.modalController.dismiss({
                  save: true,
                  data: waterConsumed
                });
                this.showToastMessage('bottom', responseApi.message, 'success');
                this.loading = false;
              }
            }
          )
        break;
    }
  }

  calcWaterConsume(): void {
    if (!this.readerForm.controls.readering_actual.valid || !this.readerForm.controls.readering_last.valid && !this.waterConsume) {
      return;
    }

    if (this.readerForm.getRawValue().readering_actual < this.readerForm.getRawValue().readering_last) {
      this.currentReadingLowerPreviousReading = true;
      return;
    }

    this.currentReadingLowerPreviousReading = false;

    const waterConsume =
      this.readerForm.getRawValue().readering_actual -
      this.readerForm.getRawValue().readering_last;
    this.readerForm.controls.water_consume.setValue(waterConsume);

    const totalPriceToWaterConsume: number = this._calcAmount(waterConsume);

    if (totalPriceToWaterConsume > this.prices.minimal_price) {
      this.readerForm.controls.amount.setValue(totalPriceToWaterConsume);
    } else {
      this.readerForm.controls.amount.setValue(Number(this.prices.minimal_price));
    }
  }

  private _verifyOptionSelectedByUser(): void {
    if (this.partner.sewerage) {
      this.sewerage_price = Number(this.prices.sewerage_price);
      this.readerForm.controls.sewerage.setValue(this.sewerage_price);
    }
    switch (this.optionSelectedByUser) {
      case OptionSelectedByUserEnum.PAY:
        this.readerForm.controls.readering_actual.disabled;
        this.readerForm.controls.readering_last.disabled;
        this.readerForm.patchValue(this.waterConsume);

        this._calcAmount(this.waterConsume.water_consume);
        this._calcDebt();
        break;
      case OptionSelectedByUserEnum.MODIFY:
        this.readerForm.patchValue(this.waterConsume);
        this._calcAmount(this.waterConsume.water_consume);
        break;
      default:
        if (!!this.waterConsume) {
          this.readerForm.controls.readering_last.setValue(this.waterConsume.readering_actual)
          this.readerForm.getRawValue()
          this.readerForm.controls.readering_last.disable();
        }
        break;
    }
  }

  private _calcDebt(): void {
    const actualDate = new Date();
    const dateReadered = new Date(this.waterConsume.dateToRead.toString());
    this.differenceBeetwenDays = actualDate.getDate() - dateReadered.getDate();
    if (this.differenceBeetwenDays > 3) {
      const actualAmount = this.readerForm.getRawValue().amount + this.prices.debts;
      this.readerForm.controls.amount.setValue(actualAmount);
      this.debts = true;
      return;
    }
    this.debts = false;
  }

  private _calcAmount(waterConsume: number): number {
    if (waterConsume <= this.prices.first_range) {
      this.priceToPay = this.prices.first_water_price;
    }
    if (
      waterConsume > this.prices.first_range &&
      waterConsume <= this.prices.second_range
    ) {
      this.priceToPay = this.prices.second_water_price;
    }
    if (waterConsume > this.prices.second_range) {
      this.priceToPay = this.prices.third_water_price;
    }

    return waterConsume * this.priceToPay;
  }

  private _generatePDF(): void {
    let data = document.getElementById('contentToConvert');
    html2canvas(data, { scale: 4, logging: false }).then(canvas => {
      let imgWidth = 430;
      let imgHeight = 0;

      const contentDataURL = canvas.toDataURL('image/png', 1.0);

      let pdf = new jsPDF('p', 'px', 'letter');
      let marginLeft = (pdf.internal.pageSize.width - imgWidth) / 2;
      pdf.addImage(contentDataURL, 'PNG', marginLeft, 10, imgWidth, imgHeight)

      window.open(pdf.output('bloburl') as any, '_blank');
    });
  }

  async showToastMessage(position: 'top' | 'middle' | 'bottom', message: string, color: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 1500,
      color: color,
      position: position,
    });

    await toast.present();
  }
}
