/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable no-underscore-dangle */
/* eslint-disable max-len */
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDatepicker } from '@angular/material/datepicker';
import { PartnerLocation } from '@core/models/admin/location/location.model';
import { Partner, PartnerReader, PayByPartner } from '@core/models/admin/partners/partner.model';
import { GetPaymentsByUserService } from '@core/services/admin/reader/get-payments-by-user.service';
import { PartnerLocationService } from '@core/services/location/partner-location.service';
import { ModalController, Platform, ViewDidEnter, ViewDidLeave } from '@ionic/angular';
import moment, { Moment } from 'moment';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { ReaderFormComponent } from './components/reader-form/reader-form.component';
import { PriceService } from '@core/services/admin/price.service';
import { PriceConfig } from '@core/models/price-config.model';
import { WaterConsumedDataContainer } from './container/water-consumed-data/water-consumed-data.container';
import { ERole, IUser } from '@core/models/admin/user.model';
import { CookieService } from 'ngx-cookie-service';
import { AuthConstants } from '@core/config/auth-constanst';

@Component({
  selector: 'app-reader',
  templateUrl: './reader.page.html',
  styleUrls: ['./reader.page.scss'],
})
export class ReaderPage implements OnInit, ViewDidEnter, ViewDidLeave, OnDestroy {
  @ViewChild(MatDatepicker) picker: MatDatepicker<Moment>;
  public locationsList$: Observable<PartnerLocation[]>;
  startView: 'month' | 'year' | 'multi-year' = 'multi-year';
  locationSelected = 'Tajamar A';
  yearSelected: FormControl;
  partnerSelected: Partner;
  partnersToReadList: PartnerReader[] = [];
  startDate: string;
  endDate: string;
  index: number = -1;
  waterConsumedByPartner: PayByPartner[];
  dataPrice: (PriceConfig & { id: string }) | undefined;
  public lastWaterConsumedByPartner: PayByPartner;
  public isAdminOrDebtCollector: boolean;
  public showLoading: boolean;
  public isViewSmall: boolean;
  public textToSearch: string;

  private _today: Date = new Date();
  private _subscriber: Subscription;
  private _userData: IUser;

  constructor(
    private getPaymentsByUserService: GetPaymentsByUserService,
    private locationService: PartnerLocationService,
    private modalController: ModalController,
    private priceService: PriceService,
    private _cookieService: CookieService,
    private _platform: Platform
  ) {
    this.yearSelected = new FormControl({ value: this._today, disabled: true });
    this._userData = JSON.parse(this._cookieService.get(AuthConstants.user));
    this.isAdminOrDebtCollector = this._userData.role.includes(ERole.ADMIN || ERole.DEBT_COLLECTOR);
    this._subscriber = new Subscription();
    this.showLoading = false;
    this._platform.width() < 992 ? this.isViewSmall = true : this.isViewSmall = false;
    this.watcherSizeWindow();
    this.textToSearch = '';
  }

  public ngOnInit(): void {
    this._initialize();
  }

  private _initialize(): void {
    this.locationsList$ = this.locationService.getAllLocation();
    this._getAllWaterToConsumedByMonth();
  }

  private watcherSizeWindow(): void {
    const resizeSubscription: Subscription = this._platform.resize.subscribe(() => {
      this._platform.width() < 992 ? this.isViewSmall = true : this.isViewSmall = false;
    });

    this._subscriber.add(resizeSubscription);
  }

  ionViewDidLeave(): void {
  }

  ngOnDestroy(): void {
    this._unsubscribe();
  }

  ionViewDidEnter(): void {
    this._getDataPrices();
  }

  selectedOnlyYearAndMonth(chosenDate: Moment): void {
    this.picker.close();
    this._today = moment(chosenDate).toDate();
    this.yearSelected.setValue(this._today);
    this._getAllWaterToConsumedByMonth();
  }

  reloadPartnersToRead(): void {
    this._getAllWaterToConsumedByMonth();
    this.partnerSelected = {} as Partner;
    this.waterConsumedByPartner = {} as PayByPartner[];
  }

  getAllWaterConsumedByPartner(partnerSelected: PartnerReader, index: number): void {
    if (partnerSelected) {
      this.partnerSelected = partnerSelected.partner;
      this.waterConsumedByPartner = partnerSelected.water_consumed;
      this.lastWaterConsumedByPartner = partnerSelected.water_consumed[partnerSelected.water_consumed.length - 1];
      this.index = index;
    }

    if (this.isAdminOrDebtCollector && this.isViewSmall) {
      this.showWaterConsume(this.partnerSelected, this.dataPrice, this.waterConsumedByPartner, this.isAdminOrDebtCollector, this.lastWaterConsumedByPartner);
    }
  }

  private async showWaterConsume(partnerSelected, dataPrice, waterConsumedByPartner, isAdminOrDebtCollector, lastWaterConsumedByPartner) {
    const modal = await this.modalController.create({
      component: WaterConsumedDataContainer,
      componentProps: {
        partnerSelected: partnerSelected,
        dataPrice: dataPrice,
        waterConsumedList: waterConsumedByPartner,
        isReaderView: !isAdminOrDebtCollector,
        lastWaterConsume: lastWaterConsumedByPartner,
        isViewSmall: this.isViewSmall
      },
      mode: 'ios',
      backdropDismiss: false
    });

    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data.save) {
        this.reloadPartnersToRead();
      } else {
        this.partnerSelected = {} as Partner;
        this.waterConsumedByPartner = {} as PayByPartner[];
        this.index = -1;
      }
    });

    await modal.present();
  }

  public async addNewPaymentByReader(partnerSelected: PartnerReader, index: number): Promise<void> {
    this.getAllWaterConsumedByPartner(partnerSelected, index);
    const modal = await this.modalController.create({
      component: WaterConsumedDataContainer,
      componentProps: {
        partnerSelected: this.partnerSelected,
        dataPrice: this.dataPrice,
        waterConsumedList: this.waterConsumedByPartner,
        isReaderView: !this.isAdminOrDebtCollector,
        lastWaterConsume: this.lastWaterConsumedByPartner
      },
      mode: 'ios',
      backdropDismiss: false
    });

    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data.save) {
        this.reloadPartnersToRead();
      } else {
        this.partnerSelected = {} as Partner;
        this.waterConsumedByPartner = {} as PayByPartner[];
        this.index = -1;
      }
    });

    await modal.present();
  }

  async addNewPayment(): Promise<void> {
    const modal = await this.modalController.create({
      component: ReaderFormComponent,
      componentProps: {
        partner: this.partnerSelected,
        prices: this.dataPrice,
        waterConsume: this.lastWaterConsumedByPartner
      },
      mode: 'ios',
      backdropDismiss: false
    });

    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data.save) {
        this._getAllWaterToConsumedByMonth();
      }
    });

    await modal.present();
  }

  public reloadWaterConsumedByPartner(reloadWaterConsume: boolean): void {
    if (reloadWaterConsume) {
      this._getAllWaterToConsumedByMonth();
    }
  }

  private _getAllWaterToConsumedByMonth(): void {
    this.showLoading = true;
    this.startDate = moment(this.isAdminOrDebtCollector ? "2022-01-01" : this._today).startOf('month').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
    this.endDate = moment(this._today).endOf('month').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
    const waterConsumedSubscription: Subscription = this.getPaymentsByUserService.getPaymentsByUserByMonth(this.startDate, this.endDate, this.locationSelected)
      .pipe(
        take(1)
      ).subscribe((data) => {
        this.partnersToReadList = data.data;

        if (this.partnerSelected) {
          const partnerSelectedIndex: number = this.partnersToReadList.findIndex((partnerReader: PartnerReader) => partnerReader.partner.id === this.partnerSelected.id);
          if (partnerSelectedIndex !== -1) {
            this.waterConsumedByPartner = this.partnersToReadList[partnerSelectedIndex].water_consumed;
          }
        }
        this.showLoading = false;
      });
    this._subscriber.add(waterConsumedSubscription);
  }

  private _getDataPrices(): void {
    const dataPriceSubscription: Subscription = this.priceService.getData().subscribe(
      (price: (PriceConfig & { id: string }) | undefined) => {
        this.dataPrice = price;
      }
    );
    this._subscriber.add(dataPriceSubscription);
  }

  private _unsubscribe(): void {
    this._subscriber.unsubscribe();
  }

  public searchDataEvent(event): void {
    this.textToSearch = event.target.value.toLowerCase();
  }
}
