import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReaderPageRoutingModule } from './reader-routing.module';

import { ReaderPage } from './reader.page';
import { SharedModule } from '@shared/shared.module';
import { AngularMaterialModule } from '@shared/angular-material/angular-material.module';
import { ReaderedDataContainer } from './container/readered-data/readered-data.container';
import { ReaderFormComponent } from './components/reader-form/reader-form.component';
import { WaterConsumedDataContainer } from './container/water-consumed-data/water-consumed-data.container';
import { WaterCutFormComponent } from './components/water-cut-form/water-cut-form.component';
import { NgxScannerQrcodeModule } from 'ngx-scanner-qrcode';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReaderPageRoutingModule,
    SharedModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    NgxScannerQrcodeModule
  ],
  declarations: [
    ReaderPage,
    ReaderedDataContainer,
    ReaderFormComponent,
    WaterConsumedDataContainer,
    ReaderFormComponent,
    WaterCutFormComponent
  ]
})
export class ReaderPageModule { }
