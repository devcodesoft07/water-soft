/* eslint-disable @typescript-eslint/semi */
import { ModalController } from '@ionic/angular';
/* eslint-disable @angular-eslint/component-class-suffix */
import { Component, Input, OnInit } from '@angular/core';
import { RegisterExpenseComponent } from '../../components/register-expense/register-expense.component';
import { ExpenseCategory } from '@core/models/admin/expenses/expense-category.model';

@Component({
  selector: 'app-expenses-card',
  templateUrl: './expenses-card.container.html',
  styleUrls: ['./expenses-card.container.scss'],
})
export class ExpensesCardContainer implements OnInit {
  @Input() expenseCategory;

  constructor(
    private modalController: ModalController
  ) { }

  ngOnInit() {}

  onClick(option: any){
  }

  async registerExpense( category: ExpenseCategory) {
    const modal = await this.modalController.create({
      component: RegisterExpenseComponent,
      mode: 'ios',
      componentProps: {
        expenseCategory: category
      }
    });
    await modal.present();
  }
}
