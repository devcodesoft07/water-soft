/* eslint-disable @angular-eslint/component-class-suffix */
import { Component, Input, OnInit } from '@angular/core';
import { ConstanstEnum } from '@core/enum/constants.enum';
import { Expense } from '@core/models/admin/expenses/expense.model';
import { TotalAmount } from '@core/models/total-amount.model';

@Component({
  selector: 'app-expenses-reports-list',
  templateUrl: './expenses-reports-list.container.html',
  styleUrls: ['./expenses-reports-list.container.scss'],
})
export class ExpensesReportsListContainer implements OnInit {
  @Input() expenses: Expense[];
  @Input() totalAmountExpenses: TotalAmount;
  constanstEnum = ConstanstEnum;
  public totalAmountExpensesBolCoin: number;
  public totalAmountExpensesDollarCoin: number;
  constructor() { }

  ngOnInit() { }
  managementExpense(event: any) {
    console.log(event);
  }
}
