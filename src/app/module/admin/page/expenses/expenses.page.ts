/* eslint-disable @typescript-eslint/no-inferrable-types */
/* eslint-disable @typescript-eslint/member-ordering */
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ExpenseCategory } from '@core/models/admin/expenses/expense-category.model';
import { Expense } from '@core/models/admin/expenses/expense.model';
import { ExpensesFireService } from '@core/services/admin/expenses/expenses-fire.service';
import { Observable, Subscription, throwError } from 'rxjs';
import { catchError, map, take, tap } from 'rxjs/operators';
import { IonSlides, Platform } from '@ionic/angular';
import { ExpensesCategoryFireService } from '@core/services/admin/expenses/expenses-category-fire.service';
import moment, { Moment } from 'moment';
import { MatDatepicker } from '@angular/material/datepicker';
import { FormControl } from '@angular/forms';
import { fadeInOut } from '@shared/animations/fade-in-out.animations';

@Component({
  selector: 'app-expenses',
  templateUrl: './expenses.page.html',
  styleUrls: ['./expenses.page.scss'],
  animations: [
    fadeInOut
  ]
})
export class ExpensesPage implements OnInit, OnDestroy {
  @ViewChild(MatDatepicker) picker: MatDatepicker<Moment>;
  @ViewChild('slideActivity') ionSlides: IonSlides;
  public expensesList$: Observable<ExpenseCategory[]>;
  public load: boolean;
  public expenses$: Observable<Expense[]>;
  public expenseCategotySelected: ExpenseCategory | null;
  sliderIndex: number = 0;
  slideOpts;
  public yearSelected: FormControl;
  public startView: 'month' | 'year' | 'multi-year' = 'multi-year';
  public today: Date = new Date();
  public startDate: string;
  public endDate: string;
  public collapsed: boolean = false;
  public iconName: string;
  public buttonSize: string;
  private _resizeSubscription: Subscription;

  constructor(
    private expensesCategoryFireService: ExpensesCategoryFireService,
    private expenseFireService: ExpensesFireService,
    private _platform: Platform
  ) {
    this.yearSelected = new FormControl({ value: this.today, disabled: true });
    this.slideOpts = {
      initialSlide: 0,
      speed: 400,
      allowTouchMove: false
    };
    this.load = false;
    this.expenseCategotySelected = null;
    this.iconName = 'chevron-up-outline';
    this._platform.width() < 768 ? this.buttonSize = 'small' : this.buttonSize = 'default';
  }

  ngOnInit() {
    this._initialize();
  }

  public ngOnDestroy(): void {
    this._resizeSubscription.unsubscribe();
  }

  private _initialize(): void {
    this.expensesList$ = this.expensesCategoryFireService.getAll();
    this.getAllExpenses();
    this.watcherSizeWindow
  }

  private watcherSizeWindow(): void {
    this._resizeSubscription = this._platform.resize.subscribe(() => {
      this._platform.width() < 768 ? this.buttonSize = 'small' : this.buttonSize = 'default';
    });
  }

  public changeSlide(slideIndex: number): void {
    this.sliderIndex = slideIndex;
    this.ionSlides.slideTo(this.sliderIndex);
  }

  private getAllExpenses(): void {
    this.load = true;
    this.startDate = moment(this.today).startOf('month').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
    this.endDate = moment(this.today).endOf('month').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');

    this.expenses$ = this.expenseFireService.getAllByDate(this.startDate, this.endDate).pipe(tap(() => this.load = false));
  }

  public filterSelect(event: any) {
    this.expenseCategotySelected = event.detail.value;
  }

  public chosenMonthHandler(chosenDate: Moment): void {
    this.picker.close();
    this.today = moment(this.today)
      .set('year', moment(chosenDate).year())
      .set('month', moment(chosenDate).month())
      .toDate();
    this.yearSelected.setValue(this.today);
    this.getAllExpenses();
  }

  public generateReport(): void {
    this.load = true;
    const monthName: string = moment(this.today).format('MMMM').toUpperCase();
    this.expenseFireService.generarReportePDF(this.startDate, this.endDate, monthName)
      .pipe(
        take(1),
        map((result) => {
          if (result && result.data) {
            const base64String = result.data;
            const decodedBytes = new Uint8Array(atob(base64String).split('').map(char => char.charCodeAt(0)));
            return new Blob([decodedBytes], { type: 'application/pdf' });
          } else {
            throw new Error('La respuesta no tiene el formato esperado');
          }
        }),
        catchError((error) => {
          console.error('Error al generar el informe PDF:', error);
          return throwError('Error al generar el informe PDF');
        })
      )
      .subscribe((result) => {
        this.load = false;
        const pdfUrl = URL.createObjectURL(result);
        window.open(pdfUrl, '_blank');
      });
  }

  public toggleExpandAndCollapsed(): void {
    this.collapsed = !this.collapsed;
    this.collapsed ? this.iconName = 'chevron-down-outline' : this.iconName = 'chevron-up-outline';
  }
}
