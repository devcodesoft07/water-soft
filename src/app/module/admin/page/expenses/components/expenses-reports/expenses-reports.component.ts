/* eslint-disable quote-props */
/* eslint-disable @typescript-eslint/quotes */
import { Component, OnInit, Input } from '@angular/core';
import { ExpenseCategory } from '@core/models/admin/expenses/expense-category.model';
import { Expense } from '@core/models/admin/expenses/expense.model';
import { ExpensesFireService } from '@core/services/admin/expenses/expenses-fire.service';
import moment from 'moment';

@Component({
  selector: 'app-expenses-reports',
  templateUrl: './expenses-reports.component.html',
  styleUrls: ['./expenses-reports.component.scss'],
})
export class ExpensesReportsComponent implements OnInit {
  @Input() expenseCategories: ExpenseCategory[];
  public expenseList: Expense[];
  public startDate: string;
  public endDate: string;
  public today: Date = new Date();
  public totalAmountExpenses: number;

  constructor(private expenseFireService: ExpensesFireService) {
    this.startDate = moment(new Date()).startOf('year').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
    this.endDate = moment(new Date()).add(1, 'year').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
  }

  ngOnInit() {
    this._initialize();
  }

  searchExpense(event: any) {
    console.log(event);
  }

  filterSelect(event: any) {
    console.log(event);
  }

  private _initialize(): void {
    this.expenseFireService.getAllByDate(this.startDate, this.endDate)
      .subscribe((expenseList: Expense[]) => {
        this.expenseList = expenseList;
        this.totalAmountExpenses = expenseList.reduce((currentExpense, nextExpense) => currentExpense + nextExpense.amount, 0);
      })
  }
}
