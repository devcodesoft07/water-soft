/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/quotes */
/* eslint-disable @typescript-eslint/no-inferrable-types */
/* eslint-disable @typescript-eslint/naming-convention */
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Component, Input, OnInit } from '@angular/core';
import { ExpenseCategory } from '@core/models/admin/expenses/expense-category.model';
import Swal from 'sweetalert2';

import { ExpensesFireService } from '@core/services/admin/expenses/expenses-fire.service';
import { ConstanstEnum } from '@core/enum/constants.enum';

@Component({
  selector: 'app-register-expense',
  templateUrl: './register-expense.component.html',
  styleUrls: ['./register-expense.component.scss'],
})
export class RegisterExpenseComponent implements OnInit {
  @Input() expenseCategory: ExpenseCategory;
  today: string = new Date().toISOString();
  expenseForm: UntypedFormGroup;
  coinStatus: boolean = true;
  passageStatus: boolean = true;
  state: boolean = false;
  passage: string = ' ';
  nameDeceased: string = '';
  defaultDescriptionDeceased = 'Pago por difunto a la familia de ' + this.nameDeceased;
  defaultDescriptionServices = 'Pago por servicios y mantenimiento a.. ';
  defaultDescriptionElectricity = 'Pago de Luz a Elfec ';

  constructor(
    private modalController: ModalController,
    private formBuilder: UntypedFormBuilder,
    private expensesFireService: ExpensesFireService
  ) {
    this.buildForm();
  }

  ngOnInit() {
    this.setValuesToGroupForm();
  }

  closeModal() {
    this.modalController.dismiss({
      save: false
    });
  }

  coinChange(event) {
    this.coinStatus = event.detail.checked;
    const coinValue = this.coinStatus ? ConstanstEnum.BS_COIN : ConstanstEnum.DOLLAR_COIN;
    this.expenseForm.controls.coin.setValue(coinValue);
  }

  async passageChangeStatus(event) {
    this.passageStatus = event.detail.checked;
    if (this.passageStatus) {
      this.passage = '+ Pasaje';
    } else {
      this.passage = '';
    }
  }

  registerExpense() {
    Swal.fire({
      title: '¿Registrar nuevo gasto?',
      text: "Asegurate de que los datos sean correctos",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'No, verificar',
      focusCancel: false,
      backdrop: false,
      confirmButtonText: 'Si, registrar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.state = true;
        this.expenseForm.controls.passage.setValue(this.passageStatus);
        const expenseFormData = this.expenseForm.value;
        this.expensesFireService.add(expenseFormData)
          .then(() => {
            Swal.fire({
              backdrop: false,
              title: '¡Registrado correctamente!',
              text: 'El nuevo gasto se registró correctamente',
              icon: 'success',
              iconColor: '#0284C9',
              showConfirmButton: false,
              timer: 2000
            });
            this.state = false;
            this.modalController.dismiss({
              save: false
            });
          }
          )
          .catch((error: any) => {
            this.state = false;
            Swal.fire({
              text: 'Upss, algo salio mal, ingrese los datos correctos',
              icon: 'error',
              confirmButtonText: 'ok',
              focusCancel: false,
              backdrop: false
            });
            this.modalController.dismiss({
              save: false
            });
          }

          );

      }
    });
  }
  private buildForm() {
    this.expenseForm = this.formBuilder.group({
      amount: [100,
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(255),
          Validators.pattern('[0-9]*')
        ]
      ],
      coin: ['Bs', []],
      expense_category: ['',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(255),
        ]
      ],
      passage: [this.passageStatus,
      [
        Validators.required,
      ]
      ],
      detail: ['',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(255),
        ]
      ],
      date: [this.today,
      [
        Validators.required,
      ]
      ],
      name_deceased: []
    });
  }

  private setValuesToGroupForm(): void {
    let detailValue = '';
    switch (this.expenseCategory.id) {
      case 3:
        detailValue = this.defaultDescriptionDeceased + this.nameDeceased;
        break;
      case 2:
        detailValue = this.passageStatus ? this.defaultDescriptionServices + ' + Pasajes' + this.expenseForm.controls.detail.value : this.defaultDescriptionServices + this.expenseForm.controls.detail.value;
        break;
      case 1:
        detailValue = this.passageStatus ? this.defaultDescriptionElectricity + ' + Pasajes' : this.defaultDescriptionElectricity;
        break;
      default:
        detailValue = this.expenseForm.controls.detail.value;
        break;
    }
    this.expenseForm.controls.expense_category.setValue(this.expenseCategory);
    this.expenseForm.controls.detail.setValue(detailValue);
  }
}
