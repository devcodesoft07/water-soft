import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExpensesPageRoutingModule } from './expenses-routing.module';

import { ExpensesPage } from './expenses.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { ExpensesCardContainer } from './container/expenses-card/expenses-card.container';
import { RegisterExpenseComponent } from './components/register-expense/register-expense.component';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { ExpensesReportsComponent } from './components/expenses-reports/expenses-reports.component';
import { ExpensesReportsListContainer } from './container/expenses-reports-list/expenses-reports-list.container';
import { AngularMaterialModule } from '@shared/angular-material/angular-material.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExpensesPageRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    SweetAlert2Module,
    AngularMaterialModule
  ],
  declarations: [
    ExpensesPage,
    ExpensesCardContainer,
    RegisterExpenseComponent,
    ExpensesReportsComponent,
    ExpensesReportsListContainer
  ]
})
export class ExpensesPageModule {}
