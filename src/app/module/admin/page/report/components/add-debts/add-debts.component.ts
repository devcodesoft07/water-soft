import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Debt } from '@core/models/detbs.model';
import { DebtsService } from '@core/services/admin/debts.service';
import { MessageService } from '@core/services/message/message.service';
import { ModalController } from '@ionic/angular';
import { SweetAlertOptions } from 'sweetalert2';

@Component({
  selector: 'app-add-debts',
  templateUrl: './add-debts.component.html',
  styleUrls: ['./add-debts.component.scss']
})
export class AddDebtsComponent implements OnInit {
  dataForm: UntypedFormGroup;
  state = false;
  alertOptions: SweetAlertOptions;
  constructor(
    private modalController: ModalController,
    private formBuilder: UntypedFormBuilder,
    private debtsService: DebtsService,
    private messageService: MessageService
  ) {
    this.buildForm();
    this.alertOptions = {
      title: 'Multa agregado correctamente',
      text: 'Los datos se registraron correctamente',
      timer: 2000,
      showConfirmButton: false,
      icon: 'success',
      iconColor: '#0284C9',
      backdrop: false
    };
  }

  ngOnInit(): void {
  }

  closeModal(): void {
    this.modalController.dismiss();
  }

  saveData(): void {
    this.state = true;
    const newDebt: Debt = this.dataForm.value;
    this.debtsService.add(newDebt).then(
      () => {
        this.state = false;
        this.messageService.showSwalBasicAlert(this.alertOptions);
        this.closeModal();
      }
    ).catch(() => this.state = false);
    this.modalController.dismiss(newDebt);
  }

  private buildForm(): void {
    this.dataForm = this.formBuilder.group({
      description: ['', [Validators.required]],
      amount: ['', [Validators.required]]
    });
  }
}
