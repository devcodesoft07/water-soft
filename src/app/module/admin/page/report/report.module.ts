import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReportPageRoutingModule } from './report-routing.module';

import { ReportPage } from './report.page';
import { SharedModule } from '@shared/shared.module';
import { DebtsContainer } from './container/debts/debts.container';
import { AddDebtsComponent } from './components/add-debts/add-debts.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReportPageRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ],
  declarations: [ReportPage, DebtsContainer, AddDebtsComponent]
})
export class ReportPageModule {}
