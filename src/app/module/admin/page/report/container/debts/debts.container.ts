/* eslint-disable @angular-eslint/component-class-suffix */
import { Component, Input, OnInit } from '@angular/core';
import { Debt } from '@core/models/detbs.model';

@Component({
  selector: 'app-debts',
  templateUrl: './debts.container.html',
  styleUrls: ['./debts.container.scss']
})
export class DebtsContainer implements OnInit {
  @Input() debts: Debt[];
  constructor() { }

  ngOnInit(): void {
  }

}
