import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Debt } from '@core/models/detbs.model';
import { DebtsService } from '@core/services/admin/debts.service';
import { ModalController } from '@ionic/angular';
import { AddDebtsComponent } from './components/add-debts/add-debts.component';

@Component({
  selector: 'app-report',
  templateUrl: './report.page.html',
  styleUrls: ['./report.page.scss'],
})
export class ReportPage implements OnInit, OnDestroy {
  debts: Debt[];
  state = false;
  private subscription: Subscription = new Subscription();
  constructor(
    private debtsService: DebtsService,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.getDebtsList();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  async addDebts(): Promise<void> {
    const modal = await this.modalController.create({
      component: AddDebtsComponent,
      mode: 'ios',
      backdropDismiss: false
    });

    await modal.present();
  }

  private getDebtsList(): void {
    this.state = true;
    const debtsSubscription: Subscription = this.debtsService.getAll().subscribe(
      (debts: Debt[]) => {
        this.state = false;
        this.debts = debts;
      },
      (error: any) => {
        this.state = false;
      }
    );
    this.subscription.add(debtsSubscription);
  }
}
