import { ModalController } from '@ionic/angular';
/* eslint-disable @angular-eslint/component-class-suffix */
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RegisterIncomesComponent } from '../../components/register-incomes/register-incomes.component';
import { IncomeCatergory } from '@core/models/admin/incomes/income.model';

@Component({
  selector: 'app-income-card',
  templateUrl: './income-card.container.html',
  styleUrls: ['./income-card.container.scss'],
})
export class IncomeCardContainer implements OnInit {
  @Input() incomeCategory: IncomeCatergory;
  @Output() reloadIncomesData: EventEmitter<boolean>;

  constructor(private modalController: ModalController) {
    this.reloadIncomesData = new EventEmitter();
  }

  ngOnInit() { }

  // async registerPaymentWater() {
  //   // if(category.id !== 1 ){
  //   // const modal = await this.modalController.create({
  //   //   component: RegisterIncomesComponent,
  //   //   mode: 'ios',
  //   //   componentProps: {
  //   //     // expenseCategory: category
  //   //   }
  //   // });
  //   // modal.present();
  //   // } else{
  //   const modal = await this.modalController.create({
  //     component: PaymentWaterFormComponent,
  //     mode: 'ios',
  //     backdropDismiss: false,
  //     componentProps: {
  //       // expenseCategory: category
  //     },
  //   });
  //   modal.present();
  //   // }
  // }

  async registerIncomes() {
    const modal = await this.modalController.create({
      component: RegisterIncomesComponent,
      mode: 'ios',
      backdropDismiss: false,
      componentProps: {
        incomeOption: this.incomeCategory,
      },
    });

    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data.save) {
        this.reloadIncomesData.emit(dataReturned.data.save);
      }
    });

    modal.present();
  }
}
