/* eslint-disable @angular-eslint/component-class-suffix */
import { Component, OnInit, Input } from '@angular/core';
import { ConstanstEnum } from '@core/enum/constants.enum';
import { Income } from '@core/models/admin/incomes/income.model';
import { TotalAmount } from '@core/models/total-amount.model';

@Component({
  selector: 'app-income-reports-list',
  templateUrl: './income-reports-list.container.html',
  styleUrls: ['./income-reports-list.container.scss'],
})
export class IncomeReportsListContainer implements OnInit {
  @Input() incomes: Income[];
  @Input() totalAmountIncomes: TotalAmount;
  constanstEnum = ConstanstEnum;

  constructor() { }

  ngOnInit() { }

  managementFine(event: any) {
    console.log(event);
  }

}
