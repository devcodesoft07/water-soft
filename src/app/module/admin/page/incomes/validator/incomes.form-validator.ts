import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ConstanstEnum } from '@core/enum/constants.enum';

export class IncomeFormValidator {
  incomesForm!: UntypedFormGroup;
  today: string = new Date().toISOString();
  constructor(public formBuilder: UntypedFormBuilder) {
    this.buildForm();
  }
  private buildForm(): void {
    this.incomesForm = this.formBuilder.group({
      amount: [
        '',
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(13),
          Validators.pattern('[0-9]*'),
        ],
      ],
      coin: ['Bs', [Validators.required]],
      detail: [
        '',
        [
          Validators.required,
        ],
      ],
      date: [this.today, [Validators.required]],
      partner: [''],
      debt: [''],
      category: ['']
    });
  }
}
