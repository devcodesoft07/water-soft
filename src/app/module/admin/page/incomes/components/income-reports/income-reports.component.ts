/* eslint-disable quote-props */
/* eslint-disable @typescript-eslint/quotes */
/* eslint-disable @typescript-eslint/no-inferrable-types */
import { Component, OnInit, Input } from '@angular/core';
import { IncomeCatergory, IncomesResponseFunctions } from '@core/models/admin/incomes/income.model';

@Component({
  selector: 'app-income-reports',
  templateUrl: './income-reports.component.html',
  styleUrls: ['./income-reports.component.scss'],
})
export class IncomeReportsComponent implements OnInit {
  @Input() public incomesCategories: IncomeCatergory[];
  @Input() public incomesData: IncomesResponseFunctions;
  public today: Date = new Date();

  constructor() { }

  public ngOnInit(): void { }

  searchFine(event: any) {
    console.log(event);
  }

  filterSelect(event: any) {
    console.log(event);
  }
}
