import { ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payment-water-form',
  templateUrl: './payment-water-form.component.html',
  styleUrls: ['./payment-water-form.component.scss'],
})
export class PaymentWaterFormComponent implements OnInit {
  today: string = new Date().toISOString();

  constructor(
    private modalController: ModalController
  ) { }

  ngOnInit() {}

  closeModal() {
    this.modalController.dismiss({
      save: false
    });
  }
  dataSearch(event){

  }
}
