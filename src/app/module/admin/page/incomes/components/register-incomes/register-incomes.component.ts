import { UntypedFormBuilder } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Component, Input, OnInit } from '@angular/core';
import {
  FinesOfPartners,
  Income,
  IncomeCatergory,
} from '@core/models/admin/incomes/income.model';
import Swal from 'sweetalert2';
import { ErrorsMessage } from '@core/models/errors.model';
import { IncomeType } from '@core/enum/income-type.enum';
import { IncomeFormValidator } from '../../validator/incomes.form-validator';
import { DollarType } from '@core/enum/dollar-type.enum';
import { IncomesFireService } from '@core/services/admin/incomes/incomes-fire.service';
import { DocumentReference } from '@angular/fire/compat/firestore';

@Component({
  selector: 'app-register-incomes',
  templateUrl: './register-incomes.component.html',
  styleUrls: ['./register-incomes.component.scss'],
})
export class RegisterIncomesComponent
  extends IncomeFormValidator
  implements OnInit
{
  @Input() incomeOption: IncomeCatergory;
  coinStatus = true;
  listOfPartnersWithFines: FinesOfPartners[];
  partnerFinesList: any;
  incometype = IncomeType;

  constructor(
    private modalController: ModalController,
    public formBuilder: UntypedFormBuilder,
    private incomesFireService: IncomesFireService
  ) {
    super(formBuilder);
  }

  ngOnInit() {
    this.incomesForm.controls.category.setValue(this.incomeOption);
  }

  closeModal() {
    this.modalController.dismiss({
      save: false,
    });
  }

  onSelect(event: any) {
    const partnerDebtor = this.listOfPartnersWithFines.filter(
      (socio) => socio.id === event.value
    );
    this.partnerFinesList = partnerDebtor[0].fines;
  }

  fineSelect(event: any) {
    const finePartner = this.partnerFinesList.filter(
      (fineM) => fineM.id === event.value
    );
    this.incomesForm.controls.amount.setValue(finePartner[0].amount);
  }

  coinChange(event) {
    this.coinStatus = event.detail.checked;
    if (!this.coinStatus) {
      this.incomesForm.controls.coin.setValue(DollarType.DOLLAR);
    } else {
      this.incomesForm.controls.coin.setValue(DollarType.BOLIVIAN);
    }
  }

  registerIncomes() {
    Swal.fire({
      title: 'Esta seguro de registrar este Ingreso?',
      text: 'Los datos ingresados no podran ser modificados!',
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      focusCancel: false,
      backdrop: false,
      confirmButtonText: 'Registrar',
    }).then((result) => {
      if (result.isConfirmed) {
        const incomesData = this.incomesForm.value;
        this.incomesFireService.add(incomesData).then(
          (res: DocumentReference<Income>) => {
            this.swalResponse(true);
          },
          (error: ErrorsMessage) => {
            Swal.close();
            this.swalResponse(false);
          }
        );
      }
    });
  }

  private swalResponse(status: boolean): void {
    const titleToShow = status? 'Registrado': 'Error...';
    const textToShow = status? 'El registro del Ingreso se realizo correctamente': 'Algo salio mal, contactese con administración';
    const iconToShow = status? 'success': 'error';
    Swal.fire({
      backdrop: false,
      title: titleToShow,
      text: textToShow,
      icon: iconToShow,
      showConfirmButton: false,
      timer: 3000,
    });
    this.modalController.dismiss({
      save: status,
    });
  }
}
