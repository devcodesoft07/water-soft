import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IncomesPageRoutingModule } from './incomes-routing.module';

import { IncomesPage } from './incomes.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { IncomeCardContainer } from './container/income-card/income-card.container';
import { RegisterIncomesComponent } from './components/register-incomes/register-incomes.component';
import { PaymentWaterFormComponent } from './components/payment-water-form/payment-water-form.component';
import { AngularMaterialModule } from '@shared/angular-material/angular-material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { IncomeReportsComponent } from './components/income-reports/income-reports.component';
import { IncomeReportsListContainer } from './container/income-reports-list/income-reports-list.container';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IncomesPageRoutingModule,
    SharedModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    NgxChartsModule
  ],
  declarations: [
    IncomesPage,
    IncomeCardContainer,
    RegisterIncomesComponent,
    PaymentWaterFormComponent,
    IncomeReportsComponent,
    IncomeReportsListContainer
  ]
})
export class IncomesPageModule {}
