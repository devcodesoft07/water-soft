/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/no-inferrable-types */
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDatepicker } from '@angular/material/datepicker';
import { IncomeCatergory, IncomesResponseFunctions } from '@core/models/admin/incomes/income.model';
import { ResponseApiFunctions } from '@core/models/response.model';
import { IncomesCategoriesFireService } from '@core/services/admin/incomes/incomes-fire-categories.service';
import { IncomesFireService } from '@core/services/admin/incomes/incomes-fire.service';
import { IonSlides, Platform } from '@ionic/angular';
import { fadeInOut } from '@shared/animations/fade-in-out.animations';
import moment, { Moment } from 'moment';
import { Observable, Subscription } from 'rxjs';
import { map, take, tap } from 'rxjs/operators';
@Component({
  selector: 'app-incomes',
  templateUrl: './incomes.page.html',
  styleUrls: ['./incomes.page.scss'],
  animations: [
    fadeInOut
  ]
})
export class IncomesPage implements OnInit, OnDestroy {
  @ViewChild(MatDatepicker) picker: MatDatepicker<Moment>;
  @ViewChild('slideActivity') ionSlides: IonSlides;
  public incomesList$: Observable<IncomeCatergory[]>;
  public incomesCategoriesToRegister: IncomeCatergory[];
  public incomeCategorySelected: IncomeCatergory | null;
  public incomesDataToChart$: Observable<IncomesResponseFunctions>;
  public state: boolean;
  sliderIndex: number = 0;
  slideOpts;
  public startDate: string;
  public endDate: string;
  public incomesData$: Observable<ResponseApiFunctions<IncomesResponseFunctions>>;
  public yearSelected: FormControl;
  public startView: 'month' | 'year' | 'multi-year' = 'multi-year';
  public today: Date = new Date();
  public collapsed: boolean = false;
  public iconName: string;
  public buttonSize: string;
  private resizeSubscription: Subscription;

  constructor(
    private incomeFireService: IncomesFireService,
    private incomeCategoryFireService: IncomesCategoriesFireService,
    private platform: Platform
  ) {
    this.yearSelected = new FormControl({ value: this.today, disabled: true });
    this.slideOpts = {
      initialSlide: 0,
      speed: 400,
      allowTouchMove: false
    };
    this.state = false;
    this.incomeCategorySelected = null;
    this.iconName = 'chevron-up-outline';
    this.platform.width() < 768 ? this.buttonSize = 'small' : this.buttonSize = 'default';
  }

  ngOnInit() {
    this._initialize();
    this.watcherSizeWindow();
  }

  ngOnDestroy(): void {
    this.resizeSubscription.unsubscribe();
  }

  private watcherSizeWindow(): void {
    this.resizeSubscription = this.platform.resize.subscribe(() => {
      this.platform.width() < 768 ? this.buttonSize = 'small' : this.buttonSize = 'default';
    });
  }

  private _initialize(): void {
    this.incomesList$ = this.incomeCategoryFireService.getAll()
      .pipe(
        tap((incomeCategories: IncomeCatergory[]) => this.incomesCategoriesToRegister = incomeCategories.filter((incomeCategory: IncomeCatergory) => incomeCategory.showEntryToRegister)),
        map((incomeCategories: IncomeCatergory[]) => incomeCategories)
      )
    this.getIncomesByMonthAndYear();
    this._getIncomesByYearToChart();
  }

  private _getIncomesByYearToChart(): void {
    const startDate = moment(new Date()).startOf('year').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
    const endDate = moment(new Date()).endOf('year').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');

    this.incomesDataToChart$ = this.incomeFireService.getAllByDate(startDate, endDate).pipe(map((responseIncomes: ResponseApiFunctions<IncomesResponseFunctions>) => responseIncomes.data));
  }

  public getIncomesByMonthAndYear(): void {
    this.state = true;
    this.startDate = moment(new Date(this.today)).startOf('month').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
    this.endDate = moment(new Date(this.today)).endOf('month').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
    this.incomesData$ = this.incomeFireService.getAllByDate(this.startDate, this.endDate).pipe(tap(() => this.state = false));
  }

  public changeSlide(slideIndex: number): void {
    this.sliderIndex = slideIndex;
    this.ionSlides.slideTo(this.sliderIndex);
  }

  public filterSelect(event: any) {
    this.incomeCategorySelected = event.detail.value;
  }

  public chosenMonthHandler(chosenDate: Moment): void {
    this.picker.close();
    this.today = moment(this.today)
      .set('year', moment(chosenDate).year())
      .set('month', moment(chosenDate).month())
      .toDate();
    this.yearSelected.setValue(this.today);
    this.getIncomesByMonthAndYear();
  }

  public generateReport(): void {
    this.state = true;
    const monthName: string = moment(this.today).format('MMMM').toUpperCase();
    this.incomeFireService.generarReportToIncomesData(this.startDate, this.endDate, monthName)
      .pipe(take(1))
      .subscribe((result) => {
        this.state = false;
        const pdfUrl = URL.createObjectURL(result);
        window.open(pdfUrl, '_blank');
      });
  }

  public reloadIncomesDataEvent(event: boolean): void {
    if (event) {
      this.getIncomesByMonthAndYear();
      this._getIncomesByYearToChart();
    }
  }

  public toggleExpandAndCollapsed(): void {
    this.collapsed = !this.collapsed;
    this.collapsed ? this.iconName = 'chevron-down-outline' : this.iconName = 'chevron-up-outline';
  }
}
