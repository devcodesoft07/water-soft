/* eslint-disable arrow-body-style */
/* eslint-disable @typescript-eslint/no-inferrable-types */
import { PartnerService } from '@core/services/admin/partner/partner.service';
import { Component, OnInit } from '@angular/core';
import { Partner } from '@core/models/admin/partners/partner.model';
import { ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { PartnerFireService } from '@core/services/admin/partner/partner-fire.service';
import { Router } from '@angular/router';
import { PartnerLocation } from '@core/models/admin/location/location.model';
import { PartnerLocationService } from '@core/services/location/partner-location.service';
import { ResponseApiFunctions } from '@core/models/response.model';
import { fadeInOut } from '@shared/animations/fade-in-out.animations';

@Component({
  selector: 'app-partners',
  templateUrl: './partners.page.html',
  styleUrls: ['./partners.page.scss'],
  animations: [
    fadeInOut
  ]
})
export class PartnersPage implements OnInit {
  public chartData$: Observable<ResponseApiFunctions<any>>;
  public partnersList$!: Observable<Partner[]>;
  public state: boolean = false;
  public locationsList$: Observable<PartnerLocation[]>;
  public locationSelected: string;
  public statusSelected: boolean | string | null;
  public textToSearch: string;
  public collapsed: boolean = false;
  public iconName: string;

  constructor(
    private partnerService: PartnerService,
    private partnerFireService: PartnerFireService,
    private modalController: ModalController,
    private router: Router,
    private locationService: PartnerLocationService
  ) {
    this.statusSelected = null;
    this.textToSearch = '';
    this.locationSelected = 'Tajamar A';
    this.iconName = 'chevron-up-outline';
  }

  ngOnInit() {
    this._initialize();
  }

  private _initialize(): void {
    this.locationsList$ = this.locationService.getAllLocation();
    this.chartData$ = this.partnerFireService.getPartnersDataToChart();
    this.getAllFirePartners();
  }

  deletePartner() {
    this.getAllFirePartners();
  }

  public downloadPdf(): void {
    this.state = true;
    this.partnerFireService.getPartnersReport(new Date().toString(), new Date().toString(), this.locationSelected)
      .pipe(take(1))
      .subscribe((result) => {
        this.state = false;
        const pdfUrl = URL.createObjectURL(result);
        window.open(pdfUrl, '_blank');
      });
  }

  goToPartnerRegister(): void {
    this.router.navigate(['admin/socios/registrar']);
  }
  // TODO ----> implement assign new water meter
  // async openDialogToAssignMeter() {
  //   const modal = await this.modalController.create({
  //     component: AssignMeterComponent,
  //     componentProps: {
  //       partner: {}
  //     },
  //     mode: 'ios',
  //     backdropDismiss: false
  //   });
  //   await modal.present();
  // }

  private getAllFirePartners() {
    this.state = true;
    this.partnersList$ = this.partnerFireService.getAll(this.statusSelected, this.locationSelected)
      .pipe(
        tap(() => this.state = false)
      );
  }

  public filterSelect(event: any, option: string): void {
    if (option === 'status') {
      this.statusSelected = event.detail.value;
    }
    if (option === 'community') {
      this.locationSelected = event.detail.value;
    }
    this.getAllFirePartners();
  }

  public searchWaterMeter(event: any): void {
    this.textToSearch = event.detail.value;
  }

  public toggleExpandAndCollapsed(): void {
    this.collapsed = !this.collapsed;
    this.collapsed ? this.iconName = 'chevron-down-outline' : this.iconName = 'chevron-up-outline';
  }
}
