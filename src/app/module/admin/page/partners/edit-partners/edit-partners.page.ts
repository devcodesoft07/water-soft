/* eslint-disable @typescript-eslint/naming-convention */
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { Partner } from '@core/models/admin/partners/partner.model';
import { ErrorsMessage } from '@core/models/errors.model';
import { PartnerService } from '@core/services/admin/partner/partner.service';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import moment from 'moment';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-edit-partners',
  templateUrl: './edit-partners.page.html',
  styleUrls: ['./edit-partners.page.scss'],
})
export class EditPartnersPage implements OnInit {
  partnerId: number;
  partner: Partner;

  isLinear = false;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  partnersForm: UntypedFormGroup;
  options: any = {
    title: 'Modificado correctamente!',
    text: 'Los datos del socio se modificaron correctamente.',
    icon: 'success',
    iconColor: '#0284C9',
    backdrop: false,
    timer: 2500,
    showCancelButton: false,
    showConfirmButton: false
  };
  constructor(
    private breakpointObserver: BreakpointObserver,
    private activateRoute: ActivatedRoute,
    private partnerService: PartnerService,
    private formBuilder: UntypedFormBuilder,
    private loadingController: LoadingController
  ) {
    this.activateRoute.params.subscribe(
      (param: Params) => {
        this.partnerId = param.id;
        console.log('Parter id', param.id);
      },
      (error: any) => {
        console.log(error);
      }
    );
    this.buildForm();
  }

  buildForm() {
    this.partnersForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      ci_number: [, [Validators.required]],
      complement: [''],
      issued: ['', [Validators.required]], // FirsDataDorm patch value
      first_phone: [, [Validators.required]],
      spouse: [''],
      community: ['', [Validators.required]],
      membership: ['', [Validators.required]],
      sewerage: ['', [Validators.required]],
      second_phone: [],
      third_phone: [], // SecondDataForm patch value
    });
  }

  ngOnInit() {
    this.getPartnerData();
  }

  async getPartnerData() {
    const loading = await this.loadingController.create({
      message: 'Cargando datos...'
    });
    await loading.present();
    this.partnerService.getPartner(this.partnerId)
    .subscribe(
      async (res: Partner) => {
        this.partner = res;
        await loading.dismiss();
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  getFirstData(data: Partner): void {
    this.partnersForm.patchValue(data);
  }

  getSecondData(data: Partner, succesSwal?: SwalComponent): void {
    this.partnersForm.patchValue(data);
    this.savePartner(succesSwal);
  }

  savePartner(succesSwal: SwalComponent): void {
    const partner: Partner = this.partnersForm.value;
    this.partnersForm.controls.membership.setValue(moment(partner.membership).format('YYYY-MM-DD h:mm:ss'));
    Swal.fire({
      title: 'Verificando Datos...!',
      text: 'Espere un momento, estamos verificando la consistencia de los datos.',
      backdrop: false,
      timerProgressBar: true,
      didOpen: () => {
        Swal.showLoading();
      },
      willOpen: () => {
        this.partnerService.editPartner(partner, this.partnerId)
        .subscribe(
          (res: Partner) => {
            // console.log(res);
            succesSwal.fire();
            Swal.close();
          },
          (error: ErrorsMessage) => {
            console.log(error.message);
            Swal.fire({
              icon: 'error',
              title: '¡Oops...!',
              text: 'Algo salio mal, si el error persiste contactese con administración. ' + JSON.stringify(error.message),
              showConfirmButton: true,
              backdrop: false,
              confirmButtonColor: '#0284C9'
            });
          }
        );
      },
    }).then((result) => {
      /* Read more about handling dismissals below */
      if (result.isDismissed) {
        // console.log('I was closed by the timer');
      }
    });
  }
}
