import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditPartnersPage } from './edit-partners.page';

const routes: Routes = [
  {
    path: '',
    component: EditPartnersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditPartnersPageRoutingModule {}
