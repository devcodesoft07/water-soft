import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditPartnersPageRoutingModule } from './edit-partners-routing.module';

import { EditPartnersPage } from './edit-partners.page';
import { SharedModule } from '@shared/shared.module';
import { AngularMaterialModule } from '@shared/angular-material/angular-material.module';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditPartnersPageRoutingModule,
    SharedModule,
    AngularMaterialModule,
    SweetAlert2Module,
    ReactiveFormsModule,
  ],
  declarations: [EditPartnersPage]
})
export class EditPartnersPageModule {}
