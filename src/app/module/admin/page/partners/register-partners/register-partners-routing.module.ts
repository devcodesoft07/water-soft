import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterPartnersPage } from './register-partners.page';
import { canDeactivateGuard } from './guards/add-partner.guard';

const routes: Routes = [
  {
    path: '',
    component: RegisterPartnersPage,
    canDeactivate: [canDeactivateGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterPartnersRoutingModule { }
