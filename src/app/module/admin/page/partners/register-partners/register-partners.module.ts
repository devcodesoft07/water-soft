import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterPartnersRoutingModule } from './register-partners-routing.module';
import { IonicModule } from '@ionic/angular';
import { AngularMaterialModule } from '@shared/angular-material/angular-material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@shared/shared.module';

import { RegisterPartnersPage } from './register-partners.page';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { AssignMeterComponent } from '../components/assign-meter/assign-meter.component';
import { QRCodeModule } from 'angular2-qrcode';

@NgModule({
  declarations: [
    RegisterPartnersPage,
    AssignMeterComponent
  ],
  imports: [
    CommonModule,
    RegisterPartnersRoutingModule,
    IonicModule,
    SharedModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    SweetAlert2Module,
    QRCodeModule
  ]
})
export class RegisterPartnersModule { }
