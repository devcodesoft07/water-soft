/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit } from '@angular/core';

import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';

import { Observable, Subject } from 'rxjs';
import { first, map, shareReplay } from 'rxjs/operators';
import { MatStep } from '@angular/material/stepper';
import { IdentityCardPartner, Partner } from '@core/models/admin/partners/partner.model';
import { ErrorsMessage } from '@core/models/errors.model';

import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AssignMeterComponent } from '../components/assign-meter/assign-meter.component';
import { PartnerFireService } from '@core/services/admin/partner/partner-fire.service';
import { DocumentReference } from '@angular/fire/compat/firestore';
import { PartnerActionFireService } from '@core/services/admin/action-partner/partner-action-fire.service';
import { PartnerAction } from '@core/models/admin/partners/partner-action.model';
import { AuthConstants } from '@core/config/auth-constanst';
import { WaterMeterFireService } from '@core/services/admin/water-meter/water-meter-fire.service';
import { WaterMeter } from '@core/models/admin/water-meter.model';
import { PriceService } from '@core/services/admin/price.service';
import { PriceConfig } from '@core/models/price-config.model';
import { IncomesFireService } from '@core/services/admin/incomes/incomes-fire.service';
import { Income, IncomeCatergory } from '@core/models/admin/incomes/income.model';
import { IncomesCategoriesFireService } from '@core/services/admin/incomes/incomes-fire-categories.service';
import { take } from 'rxjs/operators';
import moment from 'moment';

@Component({
  selector: 'app-register-partners',
  templateUrl: './register-partners.page.html',
  styleUrls: ['./register-partners.page.scss']
})
export class RegisterPartnersPage implements OnInit {
  isLinear = false;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  indexStepper: number;

  isFirstComplete: boolean = false;
  isSecondComplete: boolean = false;
  isThirdComplete: boolean = false;

  partnersForm: UntypedFormGroup;

  newPartner: Partner;
  newPartnerAction: PartnerAction;
  actionForm: UntypedFormGroup;
  priceData: PriceConfig;
  waterMeterSelected: WaterMeter;
  public incomeCategories: IncomeCatergory[];

  constructor(
    private breakpointObserver: BreakpointObserver,
    private formBuilder: UntypedFormBuilder,
    private partnerFireService: PartnerFireService,
    private waterMeterFireService: WaterMeterFireService,
    private actionFireService: PartnerActionFireService,
    private modalController: ModalController,
    private router: Router,
    private priceFireService: PriceService,
    private _incomesFireService: IncomesFireService,
    private _incomesCategoriesFireService: IncomesCategoriesFireService
  ) {
    this.buildForm();
    // this.getDataFromLocalStorage();
  }

  buildForm() {
    this.partnersForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      ci_number: [, [Validators.required]],
      complement: [''],
      issued: ['', [Validators.required]], // FirsDataDorm patch value
      first_phone: [, [Validators.required]],
      spouse: [''],
      gender: ['', Validators.required],
      community: ['', [Validators.required]],
      membership: ['', [Validators.required]],
      sewerage: ['', [Validators.required]],
      second_phone: [],
      third_phone: [], // SecondDataForm patch value
    });

    this.actionForm = this.formBuilder.group({
      partner_id: [],
      payment_method: [''],
      quotas_number: [],
      quota: [],
      payment_way: [''],
      voucher: [''],
      payment_date: [''],
      next_payment: [],
      amount: [],
      price: [],
    });
  }

  ngOnInit(): void {
    this.getPriceData();
    this._getIncomeCategories();
  }

  canDeactivate(): Subject<boolean> {
    if (this.isFirstComplete || this.isSecondComplete || this.newPartner) {
      const deactivateSubject = new Subject<boolean>();

      Swal.fire({
        title: 'Esta seguro de salir del registro de socios?',
        text: this.newPartner ? 'Debe asignar un medidor para finalizar el registro, los datos del socio serán eliminados' : 'Los datos ingresados no se guardaran!',
        icon: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        focusCancel: false,
        backdrop: false,
        confirmButtonText: 'Continuar',
      }).then((result) => {
        if (result.isConfirmed) {
          deactivateSubject.next(true);
          this.partnersForm.reset();

          if (this.newPartner) {
            // this._deleteNewPartner();
          }
        } else {
          deactivateSubject.next(false);
        }
      });
      return deactivateSubject;
    }
  }

  private _getIncomeCategories(): void {
    this._incomesCategoriesFireService.getAll()
      .pipe(
        take(1)
      )
      .subscribe((incomeCategories: IncomeCatergory[]) => {
        this.incomeCategories = incomeCategories;
      })
  }

  selectedStepperChange(event: StepperSelectionEvent): void {
    this.indexStepper = event.selectedIndex as unknown as number;
    // localStorage.setItem('indexStepper', String(this.indexStepper));
  }

  private _deleteNewPartner(): void {
    this.partnerFireService.delete(this.newPartner)
      .then(() => console.warn("Partner has deleted"))
      .catch(() => console.warn("Partner not deleted"))
  }

  getFirstData(data: Partner): void {
    this.verifyTabStep(AuthConstants.FIRST_COMPLETE, data);
    this.partnersForm.patchValue(data);
    this.isFirstComplete = true;
  }

  getSecondData(data: Partner, secondMatStep: MatStep): void {
    this.verifyTabStep(AuthConstants.SECOND_COMPLETE, data);
    this.partnersForm.patchValue(data);
    console.log("SECC ", {
      data,
      form: this.partnersForm.value
    })


    const partnerDF: Partner = {
      ...this.partnersForm.value,
      identity_card: this.partnersForm.value.identity_card
    };

    this.newPartner = partnerDF;

    // this.savePartner(secondMatStep);
    this.isSecondComplete = true;
    secondMatStep._stepper.next();
  }

  getThirdData(data: any, thirdMatStep: MatStep) {
    this.actionForm.patchValue(data);
    // this.actionForm.controls.partner_id.setValue(this.newPartner.id);
    this.verifyTabStep(AuthConstants.THIRD_COMPLETE, data);
    this.waterMeterSelected = data.water_meter;

    this.savePartner(thirdMatStep);

    // this.registerActionPartner(partnerAction, thirdMatStep);
  }

  saveWaterMeterToAction(data: PartnerAction): void {

    Swal.fire({
      title: 'Verificando Datos...!',
      text: 'Espere un momento, estamos verificando la consistencia de los datos.',
      backdrop: false,
      timerProgressBar: true,
      didOpen: () => {
        Swal.showLoading();
      },
      willOpen: () => {
        const partnerAction: PartnerAction = {
          ...data,
          water_meter: {
            measurer: { ...this.waterMeterSelected, active: true }
          }
        }

        this.actionFireService.update(this.newPartner.id, partnerAction)
          .then(() => {
            this.updateWaterMeter(partnerAction.water_meter.measurer);
          })
          .catch((error: ErrorsMessage) => {
            Swal.fire({
              icon: 'error',
              title: '¡Oops...!',
              text: 'Algo salio mal, si el error persiste contactese con administración. ' + JSON.stringify(error.message),
              showConfirmButton: true,
              backdrop: false,
              confirmButtonColor: '#0284C9'
            });
          });
      },
    }).then((result) => {
    });
  }

  private updateWaterMeter(waterMeter: WaterMeter): void {
    const waterMeterUpdated: WaterMeter = {
      ...waterMeter,
      active: true,
      used: true,
      partner: this.newPartner,
      partner_action: this.newPartnerAction
    };
    this.waterMeterFireService.update(waterMeterUpdated).then(
      () => {
        this.isFirstComplete = false;
        this.isSecondComplete = false;
        this.isThirdComplete = false;
        this.indexStepper = 0;
        this.partnersForm.reset();
        this.actionForm.reset();
        localStorage.removeItem(AuthConstants.FIRST_COMPLETE);
        localStorage.removeItem(AuthConstants.SECOND_COMPLETE);
        localStorage.removeItem(AuthConstants.THIRD_COMPLETE);
        localStorage.removeItem('indexStepper');
        localStorage.removeItem('firstData');
        this.newPartner = undefined;
        this.newPartnerAction = undefined;
        Swal.fire({
          icon: 'success',
          title: 'Registro de socio exitosamente.',
          text: 'Genial!, se terminó de registrar a un nuevo socio',
          iconColor: '#0284C9',
          timer: 2000,
          showConfirmButton: false,
          backdrop: false
        });
        this.router.navigate(['admin/socios/']);
      }
    );
  }

  private verifyTabStep(optionKey: string, data: any): void {
    this[optionKey] = data ? true : false;
    // localStorage.setItem(optionKey, this[optionKey] ? 'true' : 'false');
  }

  private savePartner(thirdMatStep: MatStep): void {
    const telephonesByUser: string[] = [];
    telephonesByUser.push(this.partnersForm.value.first_phone);
    telephonesByUser.push(this.partnersForm.value.second_phone);
    telephonesByUser.push(this.partnersForm.value.third_phone);
    const identityCard: IdentityCardPartner = {
      ci_number: this.partnersForm.value.ci_number,
      complement: this.partnersForm.value.complement,
      issued: this.partnersForm.value.issued,
    };
    const partnerDF: Partner = {
      ...this.partnersForm.value,
      identity_card: identityCard,
      telephones: telephonesByUser
    };

    console.log("DFFF", partnerDF)
    Swal.fire({
      title: 'Verificando Datos...!',
      text: 'Espere un momento, estamos verificando la consistencia de los datos.',
      backdrop: false,
      timerProgressBar: true,
      didOpen: () => {
        Swal.showLoading();
      },
      willOpen: () => {
        this.partnerFireService.add(partnerDF)
          .then(async (firePartner: DocumentReference<Partner>) => {
            const fireIdDataPartner: string = firePartner.id;
            const fireDataPartner: Partner = (await firePartner.get()).data();
            this.newPartner = {
              ...fireDataPartner,
              id: fireIdDataPartner
            };

            this.actionForm.controls.partner_id.setValue(fireIdDataPartner);
            const partnerAction: any = this.actionForm.value;


            // localStorage.setItem(AuthConstants.PARTNER, JSON.stringify(this.newPartner));

            this.registerActionPartner(partnerAction, thirdMatStep);
            // secondMatStep._stepper.next();
            Swal.close();
          })
          .catch((error: ErrorsMessage) => {
            Swal.fire({
              icon: 'error',
              title: '¡Oops...!',
              text: 'Algo salio mal, si el error persiste contactese con administración. ' + JSON.stringify(error.message),
              showConfirmButton: true,
              backdrop: false,
              confirmButtonColor: '#0284C9'
            });
          });
      },
    }).then((result) => {
    });
  }

  private async registerActionPartner(partnerAction: any, thirdMatStep: MatStep) {
    Swal.fire({
      title: 'Verificando Datos...!',
      text: 'Espere un momento, estamos verificando la consistencia de los datos.',
      backdrop: false,
      timerProgressBar: true,
      didOpen: () => {
        Swal.showLoading();
      },
      willOpen: () => {
        this.actionFireService.add(this.newPartner.id, partnerAction)
          .then(async (firePartnerAction: DocumentReference<PartnerAction>) => {
            const fireIdDataPartnerAction: string = firePartnerAction.id;
            const fireDataPartnerAction: PartnerAction = (await firePartnerAction.get()).data();
            this.newPartnerAction = {
              ...fireDataPartnerAction,
              id: fireIdDataPartnerAction
            };
            console.log("AS", this.newPartnerAction)
            this.saveWaterMeterToAction(this.newPartnerAction);
            this._registerIncomeByAction();

            // localStorage.setItem(AuthConstants.PARTNER_ACTION, JSON.stringify(this.newPartnerAction));
            thirdMatStep._stepper.next();
            Swal.close();
          })
          .catch((error: ErrorsMessage) => {
            Swal.fire({
              icon: 'error',
              title: '¡Oops...!',
              text: 'Algo salio mal, si el error persiste contactese con administración. ' + JSON.stringify(error.message),
              showConfirmButton: true,
              backdrop: false,
              confirmButtonColor: '#0284C9'
            });
          });
      },
    }).then((result) => {
    });
  }

  private _registerIncomeByAction(): void {

    const newIncome: Income = {
      amount: this.priceData.action_price,
      category: this.incomeCategories.find((incomeCategory: IncomeCatergory) => incomeCategory.type === 'ACTION_SALE'),
      coin: '$us',
      date: moment(new Date()).format('YYYY-MM-DDTHH:mm:ss.SSS[Z]'),
      detail: 'Venta de Acción',
      partner: [{ id: this.newPartnerAction.partner_id }]
    } as Income;

    this._incomesFireService.add(newIncome)
      .then(() => console.log("ACTION SUCCESS"))
  }

  private async getDataFromLocalStorage() {
    this.newPartner = await localStorage.getItem('newPartner') ? JSON.parse(localStorage.getItem('newPartner')) : undefined;
    this.newPartnerAction = await localStorage.getItem('newPartnerAction') ?
      JSON.parse(localStorage.getItem('newPartnerAction')) : undefined;
    this.indexStepper = localStorage.getItem('indexStepper') ? Number(localStorage.getItem('indexStepper')) : 0;
    this.isFirstComplete = localStorage.getItem(AuthConstants.FIRST_COMPLETE) ?
      (localStorage.getItem(AuthConstants.FIRST_COMPLETE) === 'true' ? true : false) :
      false;
    this.isSecondComplete = localStorage.getItem(AuthConstants.SECOND_COMPLETE) ?
      (localStorage.getItem(AuthConstants.SECOND_COMPLETE) === 'true' ? true : false) :
      false;
    this.isThirdComplete = localStorage.getItem(AuthConstants.THIRD_COMPLETE) ?
      (localStorage.getItem(AuthConstants.THIRD_COMPLETE) === 'true' ? true : false) :
      false;
  }

  private getPriceData() {
    this.priceFireService.getData().pipe(first()).subscribe(
      (price: PriceConfig) => {
        this.priceData = price;
      }
    );
  }

  private async openDialogToAssignMeter() {
    const modal = await this.modalController.create({
      component: AssignMeterComponent,
      componentProps: {
        partner: this.newPartner,
        backdropDismiss: false
      },
      mode: 'ios'
    });
    await modal.present();
  }
}
