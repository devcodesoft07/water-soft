import { CanDeactivateFn } from "@angular/router";
import { RegisterPartnersPage } from "../register-partners.page";

export const canDeactivateGuard: CanDeactivateFn<RegisterPartnersPage> = (component: RegisterPartnersPage) => {
  return component.canDeactivate ? component.canDeactivate() : true;
};

