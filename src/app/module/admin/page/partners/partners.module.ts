import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PartnersPageRoutingModule } from './partners-routing.module';
import { AngularMaterialModule } from '@shared/angular-material/angular-material.module';
import { SharedModule } from '@shared/shared.module';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

import { StatusChangeComponent } from './components/status-change/status-change.component';
import { PartnersListContainer } from './container/partners-list/partners-list.container';

import { PartnersPage } from './partners.page';
import { QRCodeModule } from 'angular2-qrcode';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PartnersPageRoutingModule,
    SharedModule,
    AngularMaterialModule,
    SweetAlert2Module,
    ReactiveFormsModule,
    QRCodeModule
  ],
  declarations: [
    PartnersPage,
    PartnersListContainer,
    StatusChangeComponent
  ]
})
export class PartnersPageModule {}
