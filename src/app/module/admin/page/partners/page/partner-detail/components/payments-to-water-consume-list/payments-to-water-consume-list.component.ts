import { Component, Input } from '@angular/core';
import { PayByPartner } from '@core/models/admin/partners/partner.model';

@Component({
  selector: 'app-payments-to-water-consume-list',
  templateUrl: './payments-to-water-consume-list.component.html',
  styleUrls: ['./payments-to-water-consume-list.component.scss']
})
export class PaymentsToWaterConsumeListComponent {
  @Input() public waterConsumeData: PayByPartner[];
  displayedColumns: string[] = ['month-of-consume', 'date-to-read', 'date-to-pay', 'water-consume', 'amount', 'is-pay'];
}
