import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PartnerDetailPageRoutingModule } from './partner-detail-routing.module';

import { SharedModule } from '@shared/shared.module';
import { AngularMaterialModule } from '@shared/angular-material/angular-material.module';
import { PartnerDetailPage } from './partner-detail.page';
import { PaymentsToWaterConsumeListComponent } from './components/payments-to-water-consume-list/payments-to-water-consume-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PartnerDetailPageRoutingModule,
    SharedModule,
    AngularMaterialModule,
    ReactiveFormsModule
  ],
  declarations: [
    PartnerDetailPage,
    PaymentsToWaterConsumeListComponent
  ]
})
export class PartnerDetailPageModule { }
