/* eslint-disable no-underscore-dangle */
import { Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { MatDatepicker } from '@angular/material/datepicker';
import { ActivatedRoute, Params } from '@angular/router';
import { StatusWaterMeterEnum } from '@core/enum/status-water-meter.enum';
import { PartnerAction } from '@core/models/admin/partners/partner-action.model';
import { Partner } from '@core/models/admin/partners/partner.model';
import { WaterConsumedResponse } from '@core/models/admin/water-consumed/water-consumed.interface';
import { PartnerActionFireService } from '@core/services/admin/action-partner/partner-action-fire.service';
import { PartnerFireService } from '@core/services/admin/partner/partner-fire.service';
import { PartnerWaterConsumeFireService } from '@core/services/admin/partner/partner-water-consume-fire.service';
import moment, { Moment } from 'moment';
import { map, switchMap, take } from 'rxjs/operators';

@Component({
  selector: 'app-partner-detail',
  templateUrl: './partner-detail.page.html',
  styleUrls: ['./partner-detail.page.scss'],
})
export class PartnerDetailPage implements OnInit {
  @ViewChild(MatDatepicker) picker: MatDatepicker<Moment>;
  public title = 'Cantidad de consumo de agua en m3';
  public startView: 'month' | 'year' | 'multi-year' = 'multi-year';
  public yearSelected: UntypedFormControl;
  public partner!: Partner;
  public waterDataConsume: WaterConsumedResponse;
  public statusWaterMeterEnum = StatusWaterMeterEnum;

  private _today: Date = new Date();
  private _partnerId: string;
  private _partnerAction: PartnerAction;

  startDate: string;
  endDate: string;
  constructor(
    private partnerFireService: PartnerFireService,
    private activateRoute: ActivatedRoute,
    private _partnerWaterConsumeFireService: PartnerWaterConsumeFireService,
    private _partnerActionFireService: PartnerActionFireService
  ) {
    this.yearSelected = new UntypedFormControl(this._today);
  }

  ngOnInit() {
    this._init();

    this._getDataPartner();
  }

  selectedOnlyYear(chosenDate: Moment): void {
    this.picker.close();
    this._today = moment(this._today)
      .set('year', moment(chosenDate).year())
      .toDate();
    this.yearSelected.setValue(this._today);

    this._getWaterConsumeByPartnerForDate();
  }

  private _getDataPartner(): void {
    this.partnerFireService.getData(this._partnerId)
      .subscribe((partner: Partner) => {
        this.partner = partner;
      });
  }

  private _init(): void {
    this.activateRoute.params
      .pipe(
        take(1),
        switchMap((params: Params) => {
          this._partnerId = params.id;
          return this._partnerActionFireService.getAll(this._partnerId)
            .pipe(
              map((partnerActions: PartnerAction[]) => {
                this._partnerAction = partnerActions[0];
                return this._partnerAction;
              })
            )
        })
      ).subscribe(() => this._getWaterConsumeByPartnerForDate());
  }

  private _getWaterConsumeByPartnerForDate(): void {
    this.startDate = moment(this._today).startOf('year').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');
    this.endDate = moment(this.startDate).add(1, 'year').format('YYYY-MM-DDTHH:mm:ss.SSS[Z]');

    this._partnerWaterConsumeFireService.getWaterConsumeByPartnerForDate(this.startDate, this.endDate, this._partnerId, this._partnerAction.id)
      .pipe(
        take(1)
      )
      .subscribe(
        (res: any) => {
          this.waterDataConsume = res.data;
        }
      )
  }
}
