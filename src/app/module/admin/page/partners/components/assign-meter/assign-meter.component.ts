/* eslint-disable @typescript-eslint/naming-convention */
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { PartnerAction, WaterMeterAction } from '@core/models/admin/partners/partner-action.model';
import { Partner } from '@core/models/admin/partners/partner.model';
import { WaterMeter } from '@core/models/admin/water-meter.model';
import { WaterMeterFireService } from '@core/services/admin/water-meter/water-meter-fire.service';

import { ModalController } from '@ionic/angular';
import { FileUploadComponent } from '@shared/components/file-upload/file-upload.component';
import moment from 'moment';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-assign-meter',
  templateUrl: './assign-meter.component.html',
  styleUrls: ['./assign-meter.component.scss'],
})
export class AssignMeterComponent implements OnInit, OnDestroy {
  @Input() partnerAction: PartnerAction;
  @Input() partner: Partner;
  @Input() waterMeterPrice = 190;
  @Output() waterMeterToActionEventEmitter: EventEmitter<PartnerAction>;
  meterAssignForm: UntypedFormGroup;
  date: Date = new Date();
  observation = 'Disponible';
  listWaterMeter: WaterMeter[];
  dataQr = 'Ninguna';
  private subscription: Subscription = new Subscription();
  constructor(
    private modalController: ModalController,
    private formBuilder: UntypedFormBuilder,
    private waterMeterFireService: WaterMeterFireService,
  ) {
    this.waterMeterToActionEventEmitter = new EventEmitter<PartnerAction>();
    this.buildForm();
  }

  ngOnInit() {
    this.getWaterMetersAvailable();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  updateQrData(waterMeterId: string) {
    // this.meterAssignForm.controls.meter_id.setValue(waterMeterId);
    this.dataQr = JSON.stringify(this.meterAssignForm.value);
  }

  async fileUpload() {
    const modal = await this.modalController.create({
      component: FileUploadComponent,
      mode: 'ios',
      componentProps: {
        category: 'measurer',
        partner: this.partner
      }
    });

    await modal.present();

    await modal.onDidDismiss()
      .then(
        (res: any) => {
          const voucher = res.data.voucher;
          if (voucher) {
            this.meterAssignForm.controls.voucher.setValue(voucher);
          }
        }
      )
      .catch(
        (error: any) => {
          console.log(error);
        }
      );
  }

  dismiss(): void {
    this.modalController.dismiss();
  }

  meterAssignToPartner() {
    const waterMeterSelectedByUser = this.meterAssignForm.value;
    const waterMeterToAction: WaterMeterAction = {
      measurer: waterMeterSelectedByUser.water_meter,
      // sold: waterMeterSelectedByUser.sold,
      // category: waterMeterSelectedByUser.category,
      // voucher: waterMeterSelectedByUser.voucher,
      // payment_way: waterMeterSelectedByUser.payment_way,
      // payment: waterMeterSelectedByUser.payment,
      // sale_date: waterMeterSelectedByUser.sale_date
    };
    const partnerActionUpdated: PartnerAction = {
      ...this.partnerAction,
      water_meter: waterMeterToAction
    };
    this.waterMeterToActionEventEmitter.emit(partnerActionUpdated);
  }


  private getWaterMetersAvailable() {
    const waterMeterSubs: Subscription = this.waterMeterFireService.getAll().subscribe(
      (waterMeterList: WaterMeter[]) => {
        this.listWaterMeter = waterMeterList;
      },
      (error: any) => {
        console.log(error);
      }
    );
    this.subscription.add(waterMeterSubs);
  }

  private buildForm() {
    this.meterAssignForm = this.formBuilder.group({
      water_meter: [],
      // sold: [false],
      // category: ['casa'],
      // voucher: ['https://thumbs.dreamstime.com/b/bill-pagar-79388606.jpg'],
      // payment_way: ['presencial'],
      // payment: [190],
      // sale_date: [moment(this.date).format(ConstanstEnum.DATE_FORMATE)]
    });
  }

}
