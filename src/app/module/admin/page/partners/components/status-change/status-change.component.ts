import { ModalController } from '@ionic/angular';
import { Component, Input, OnInit } from '@angular/core';
import { Partner } from '@core/models/admin/partners/partner.model';

@Component({
  selector: 'app-status-change',
  templateUrl: './status-change.component.html',
  styleUrls: ['./status-change.component.scss'],
})
export class StatusChangeComponent implements OnInit {

  @Input() dataPartner: Partner;

  constructor(
    private modalController: ModalController
  ) { }

  ngOnInit() {}

  closeModal() {
    this.modalController.dismiss({
      save: false
    });
  }
}
