import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PartnersPage } from './partners.page';

const routes: Routes = [
  {
    path: '',
    component: PartnersPage
  },
  {
    path: 'registrar',
    loadChildren: () => import('./register-partners/register-partners.module').then(m => m.RegisterPartnersModule)
  },
  {
    path: 'editar/:id',
    loadChildren: () => import('./edit-partners/edit-partners.module').then( m => m.EditPartnersPageModule)
  },
  {
    path: 'detalle/:id',
    loadChildren: () => import('./page/partner-detail/partner-detail.module').then( m => m.PartnerDetailPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PartnersPageRoutingModule {}
