/* eslint-disable @typescript-eslint/naming-convention */
import { UntypedFormBuilder } from '@angular/forms';
/* eslint-disable @angular-eslint/component-class-suffix */
import { Component, Input, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { Partner } from '@core/models/admin/partners/partner.model';
import { DynamicColorService } from '@core/services/dynamic-color/dynamic-color.service';
import { PopoverController } from '@ionic/angular';
import { PartnerService } from '@core/services/admin/partner/partner.service';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import Swal from 'sweetalert2';
import { ErrorsMessage } from '@core/models/errors.model';
import { ToggleChangeEventDetail } from '@ionic/core';
import { ModalController } from '@ionic/angular';
import { EditMeasurerComponent } from '@module/admin/page/water-mater/components/edit-measurer/edit-measurer.component';
import { WaterMeterService } from '@core/services/admin/water-meter/water-meter.service';
import * as QRCode from 'qrcode';
import jsPDF from 'jspdf';
import { StatusWaterMeterEnum } from '@core/enum/status-water-meter.enum';

@Component({
  selector: 'app-partners-list',
  templateUrl: './partners-list.container.html',
  styleUrls: ['./partners-list.container.scss']
})
export class PartnersListContainer implements OnInit {
  @ViewChild('qrCanvas', { static: false }) qrCanvas: any;
  @Input() partnersList: Partner[];
  @Output() reloadEventEmitter: EventEmitter<any> = new EventEmitter();
  @ViewChild('deleteSwal') public readonly deleteSwal!: SwalComponent;

  public statusWaterMeterEnum = StatusWaterMeterEnum;

  displayedColumns: string[] = ['name', 'code', 'comunnity', 'observations', 'qr-code', 'status'];
  optionsChange: any = {
    title: '¡Cambiado correctamente!',
    text: 'Los datos del Socio se guardaron correctamente.',
    icon: 'success',
    iconColor: '#0284C9',
    backdrop: false,
    timer: 2500,
    showCancelButton: false,
    showConfirmButton: false
  };
  options: any = {
    title: '¡Eliminado correctamente!',
    text: 'Los datos del Socio se eliminaron correctamente.',
    icon: 'success',
    iconColor: '#0284C9',
    backdrop: false,
    timer: 2500,
    showCancelButton: false,
    showConfirmButton: false
  };

  constructor(
    public dynamicColorService: DynamicColorService,
    private popoverController: PopoverController,
    private partnerService: PartnerService,
    private modalController: ModalController,
    private formBuilder: UntypedFormBuilder,
    private waterMeterService: WaterMeterService,
  ) { }

  ngOnInit(): void { }

  deleteData(confirmDeletePartnerSwal: SwalComponent, id: number): void {
    Swal.fire({
      title: '¡Eliminando...!',
      text: 'Espere un momento, los datos se estan eliminando',
      backdrop: false,
      didOpen: () => {
        Swal.showLoading();
      },
      willOpen: () => {
        this.partnerService.deletePartner(id).subscribe(
          (res: boolean) => {
            if (res) {
              Swal.close();
              console.log('respuesta service', res, 'id', id);
              this.reloadEventEmitter.emit(true);
              confirmDeletePartnerSwal.fire();
            }
          },
          (error: ErrorsMessage) => {
            Swal.close();
            console.log(error);
          }
        );
      },
    }).then((result) => {
      /* Read more about handling dismissals below */
      if (result.isDismissed) {
        Swal.fire({
          // title: '¿Estás seguro de eliminar el medidor?',
          text: 'Upss, algo salio mal, revise su conexion a internet e intentelo nuevamente',
          icon: 'error',
          reverseButtons: true,
          confirmButtonText: 'ok',
          focusCancel: false,
          // showCancelButton: true,
          backdrop: false
        });
        this.popoverController.dismiss({
          reloadPage: true
        });
      }
    });
  }

  notificationsSwale(partner: any) {
    Swal.fire({
      title: '! Advertencia !',
      text: partner.debtor.observation,
      icon: 'warning',
      reverseButtons: true,
      confirmButtonText: 'ok',
      focusCancel: false,
      backdrop: false
    });
  }

  async changeStatePartner(event: any, partner: Partner, confirSwal: SwalComponent) {
    const detail: ToggleChangeEventDetail = event.detail;
    console.log('Detail', detail);
    if (detail.checked) {
      await confirSwal.fire();
    } else {
      this.changeToInactive(partner, 'statusPartner');
    }
  }

  async changeToInactive(partner: Partner, option: string) {
    const modal = await this.modalController.create({
      component: EditMeasurerComponent,
      mode: 'ios',
      componentProps: {
        dataPartner: partner,
        optionPartner: option
      }
    });
    await modal.present();
    await modal.onDidDismiss()
      .then(
        (res: any) => {
          const reload = res.data.save ? res.data.save : undefined;
          if (reload) {
            this.reloadEventEmitter.emit(true);
          }
        }
      )
      .catch(
        (error: any) => {
          console.log(error);
        }
      );
  }

  async generateQrCodeToPartner(partner: Partner) {
    const url: string = await QRCode.toDataURL(JSON.stringify(partner));
    const pdf = new jsPDF();
    const partnerFullName: string = `${partner.last_name} ${partner.name}`
    pdf.setFontSize(10);
    pdf.text(partnerFullName, 11, 15);
    pdf.addImage(url, 'PNG', 10, 20, 40, 40);
    pdf.output('dataurlnewwindow');
  }

}
