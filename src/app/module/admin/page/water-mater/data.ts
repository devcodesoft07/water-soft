/* eslint-disable no-var */
export var single = [

  // Traer active === true
  {
    name: 'Activos',
    value: 280
  },

  // Traer i y solo si la obserevacion es distinto de En Corte y Disponible
  // y el active es falso
  {
    name: 'Inactivos',
    value: 280
  },

  // Treaer observation === 'En Corte'
  {
    name: 'En Corte',
    value: 60
  },
    // Treaer observation === 'Disponible'

  {
    name: 'Disponible',
    value: 10
  }
];
