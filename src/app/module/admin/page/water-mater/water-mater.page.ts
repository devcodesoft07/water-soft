/* eslint-disable object-shorthand */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable @typescript-eslint/consistent-type-assertions */
/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/no-inferrable-types */
import { Component, ElementRef, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { WaterMeter } from '@core/models/admin/water-meter.model';
import { Paginator } from '@core/models/paginator.model';
import { WaterMeterService } from '@core/services/admin/water-meter/water-meter.service';
import { ModalController } from '@ionic/angular';
import { AddMeasurerComponent } from './components/add-measurer/add-measurer.component';
import { LegacyPageEvent as PageEvent } from '@angular/material/legacy-paginator';
import { DateFilter } from '@core/models/date-filter.model';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import Swal from 'sweetalert2';
import { WaterMeterFireService } from '@core/services/admin/water-meter/water-meter-fire.service';
import { Subscription } from 'rxjs';
import { take, tap } from 'rxjs/operators';


export interface Filter {
  value: string;
  label: string;
}
@Component({
  selector: 'app-water-mater',
  templateUrl: './water-mater.page.html',
  styleUrls: ['./water-mater.page.scss']
})
export class WaterMaterPage implements OnInit, OnDestroy {
  @ViewChild('content') content!: ElementRef;
  single: any[];
  view: any = [undefined, 230];

  // options
  gradient: boolean = true;
  showLegend: boolean = true;
  showLabels: boolean = true;
  isDoughnut: boolean = false;

  colorScheme: any = {
    domain: ['#79BBD9', '#ACC9DC', '#D7E5EE', '#AAAAAA']
  };

  date: Date = new Date();
  // month: number;
  year: number | null;
  statusSelected: boolean | string | null;

  filters: string[] = ['Activo', 'Inactivo', 'En Corte', 'Disponibles'];

  state: boolean = false;

  selectedList: UntypedFormControl = new UntypedFormControl();
  filtersOficial: string[] = [];
  waterMeters: WaterMeter[];
  isContent: boolean;

  meterNumberToSearch: string = '';

  active: boolean = true;
  observation: string = '';
  page: number;
  paginator: Paginator;

  isEmptySearch: boolean = false;
  public textToFilter: string;

  isDownload: boolean;

  private subscriptions: Subscription;
  constructor(
    private modalController: ModalController,
    private waterMeterService: WaterMeterService,
    private waterMeterFireService: WaterMeterFireService,
  ) {
    this.subscriptions = new Subscription();
    const pageLocalStorage: string | undefined = localStorage.getItem('page');
    if (pageLocalStorage) {
      this.page = (pageLocalStorage as unknown as number);
    } else {
      this.page = 1;
    }
    this.year = null;
    this.textToFilter = '';
  }

  ngOnInit() {
    this.getWaterMeters();
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  getWaterMeters(status?: boolean | string | null, year?: number) {
    this.state = true;
    this.waterMeterFireService.getAll(status, year)
      .pipe(
        take(1),
        tap((waterMeters: WaterMeter[]) => this.waterMeters = waterMeters)
      )
      .subscribe(() => {
        this.state = false;
      });
  }

  // REVIEW
  getWaterMetersWithFilter() {
    // this.state = true;
    // this.waterMeterService.getWaterMeterWithParams(this.page, this.month, this.year)
    //   .subscribe(
    //     (res: PaginatorWithChart) => {
    //       this.state = false;
    //       this.single = res.chart;
    //       this.isContent = this.single.length !== 0 ? true : false;
    //       if (res.data.data.length !== 0) {
    //         this.waterMeters = res.data.data;
    //         this.isEmptySearch = false;
    //       } else {
    //         this.waterMeters = [];
    //         this.isEmptySearch = true;
    //       }
    //       this.paginator = res.data;
    //     },
    //     (error: ErrorsMessage) => {
    //       this.state = false;
    //       console.log(error);
    //     },
    //   );
  }

  onResize(event) {
    const widthRsponsive = event.target.innerWidth;
    if (widthRsponsive >= 768) {
      this.view = [600, 230];
    }
    if (widthRsponsive >= 1440) {
      this.view = [700, 230];
    }
  }

  async addMeasurer() {
    const modal = await this.modalController.create({
      component: AddMeasurerComponent,
      mode: 'ios',
      backdropDismiss: false,
      componentProps: {
        waterMeters: this.waterMeters
      }
    });

    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data.save) {
        this.getWaterMeters();
      }
    });

    await modal.present();
  }

  searchWaterMeter(event: any): void {
    this.textToFilter = event.detail.value;
    // TODO implemented search water mater
    // const textSearch = event.detail.value;
    // this.meterNumberToSearch = textSearch;
    // this.state = true;
    // if (this.meterNumberToSearch !== '') {
    //   this.waterMeterService.searchWaterMeters(this.meterNumberToSearch).subscribe(
    //     (res: Paginator) => {
    //       this.state = false;
    //       if (res.data.length !== 0) {
    //         this.waterMeters = res.data;
    //         this.isEmptySearch = false;
    //         this.paginator = res;
    //       } else {
    //         this.waterMeters = [];
    //         this.isEmptySearch = true;
    //       }
    //       this.paginator = res;
    //     },
    //     (error: ErrorsMessage) => {
    //       this.state = false;
    //       console.log(error);
    //     }
    //   );
    // } else {
    //   // this.getWaterMetersWithFilter();
    // }
  }

  // TO DO
  changePage(event: PageEvent): void {
    this.page = event.pageIndex + 1;
    // this.getWaterMetersWithFilter();
  }

  async deleteSelected(selected: string) {
    const selection: string[] = this.filtersOficial;
    const index = selection.indexOf(selected);
    selection.splice(index, 1);
    this.filtersOficial = await selection;
    this.selectedList.setValue(selection);
  }

  filterDate(date: DateFilter): void {
    this.year = date.year;
    this.getWaterMeters(this.statusSelected, this.year)
  }

  reset(date: DateFilter): void {
    this.year = null;
    this.getWaterMeters(this.statusSelected, this.year);
  }

  // TO DO
  public downloadAsPDF() {
    this.isDownload = true;
    if (this.isDownload) {
      setTimeout(() => {
        const div = document.getElementById('content');
        const options = {
          background: 'white',
          scale: 3,
          onclone: function (doc) {
            doc.getElementById('content').style.display = 'block';
          },
        };

        html2canvas(div, options)
          .then((canvas) => {

            const img = canvas.toDataURL('image/PNG');
            const doc = new jsPDF('p', 'mm', 'a4', true);
            const fileWidth = 208;
            const fileHeight = canvas.height * fileWidth / canvas.width;
            const position = 0;
            doc.addImage(img, 'PNG', 0, position, fileWidth, fileHeight);
            // Add image Canvas to PDF
            // const bufferX = 0;
            // const bufferY = 0;
            // const imgProps = (<any>doc).getImageProperties(img);
            // imgProps.height = 2574;
            // imgProps.width = 3750;
            // const pdfWidth = 21.59;
            // const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
            // doc.addImage(img, 'PNG', bufferX, bufferY, pdfWidth, pdfHeight, undefined, 'MEDIUM');


            return doc;
          }).then((doc) => {
            doc.save('Medidores_Gestion' + this.year + '_OdevCode.pdf');
            this.isDownload = false;
          }).catch(
            (error: any) => {
              this.isDownload = false;
              Swal.fire({
                title: 'Ocurrio un problema inesperado',
                text: 'No podemos decargar el pdf, si el error persiste contactese con administración',
                icon: 'error',
                showConfirmButton: true
              });
            }
          );
      }, 2000);
    }

  }

  filterSelect(event: any) {
    this.statusSelected = event.detail.value;
    this.getWaterMeters(this.statusSelected, this.year);
  }

}
