import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WaterMaterPageRoutingModule } from './water-mater-routing.module';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '@shared/shared.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

import { WaterMaterPage } from './water-mater.page';

import { AddMeasurerComponent } from './components/add-measurer/add-measurer.component';

import { WaterMaterListContainer } from './container/water-mater-list/water-mater-list.container';
import { EditMeasurerComponent } from './components/edit-measurer/edit-measurer.component';
import { AngularMaterialModule } from '@shared/angular-material/angular-material.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    WaterMaterPageRoutingModule,
    SharedModule,
    NgxChartsModule,
    ReactiveFormsModule,
    SweetAlert2Module,
    AngularMaterialModule
  ],
  declarations: [
    WaterMaterPage,
    WaterMaterListContainer,
    AddMeasurerComponent,
    EditMeasurerComponent
  ],
})
export class WaterMaterPageModule {}
