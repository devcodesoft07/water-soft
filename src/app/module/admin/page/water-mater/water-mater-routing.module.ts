import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WaterMaterPage } from './water-mater.page';

const routes: Routes = [
  {
    path: '',
    component: WaterMaterPage
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WaterMaterPageRoutingModule {}
