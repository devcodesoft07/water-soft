/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/no-inferrable-types */
/* eslint-disable @typescript-eslint/naming-convention */
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { WaterMeter } from '@core/models/admin/water-meter.model';
import { SweetAlertOptions } from 'sweetalert2';
import { MessageService } from '@core/services/message/message.service';
import { Subscription } from 'rxjs';
import { WaterMeterFireService } from '@core/services/admin/water-meter/water-meter-fire.service';
@Component({
  selector: 'app-add-measurer',
  templateUrl: './add-measurer.component.html',
  styleUrls: ['./add-measurer.component.scss'],
})
export class AddMeasurerComponent implements OnInit, OnDestroy {
  @Input() waterMeters: WaterMeter[];

  measurerForm: UntypedFormGroup;
  today: string = new Date().toISOString();
  meterPattern: string = '^[a-zA-ZÀ-ÿ\u00f1\u00d10-9 .-]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d10-9 .-]*)*[a-zA-ZÀ-ÿ\u00f1\u00d10-9 .-]+$';
  state: boolean = false;
  errors: any;
  showMoreConfigurations: boolean = true;
  public existWaterMeterNumber: boolean;

  private _propertiesToShow: SweetAlertOptions;
  private _subscription: Subscription;
  constructor(
    private formBuilder: UntypedFormBuilder,
    private modalController: ModalController,
    private waterMeterService: WaterMeterFireService,
    private messageService: MessageService,
  ) {
    this.buildForm();
    this._propertiesToShow = {
      title: '¿Estas seguro de registrar este medidor?',
      text: 'Verifica que los datos sean correctos',
      showConfirmButton: true,
    };
    this._subscription = new Subscription();
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

  ngOnInit() {
  }

  get meterNumber(): any {
    return this.measurerForm.get('meter_number');
  }

  private buildForm(): void {
    this.measurerForm = this.formBuilder.group({
      water_meter_number: ['', [Validators.required]],
      active: [false],
      used: [false],
      register_date: [this.today],
    });
  }

  closeModal() {
    this.modalController.dismiss({
      save: false
    });
  }

  addMeasurer(): void {
    this.state = true;
    this._propertiesToShow = {
      title: '¿Estas seguro de registrar este medidor?',
      text: 'Verifica que los datos esten correctamente configurados',
      showConfirmButton: true,
    };
    const data: WaterMeter = this.measurerForm.value;
    const waterMeterSubscription: Subscription = this.messageService.showSwalConfirmAlert(this._propertiesToShow)
      .subscribe(
        (isConfirm: boolean) => {
          if (isConfirm) {
            this.saveData(data);
          }
        }
      );
    this._subscription.add(waterMeterSubscription);
  }

  public onInputChange(event: any): void {
    this.existWaterMeterNumber = !!this.waterMeters.find((waterMeter: WaterMeter) => waterMeter.water_meter_number === event.target.value);
  }

  private saveData(waterMeter: WaterMeter): void {
    this.waterMeterService.add(waterMeter)
      .then(() => {
        this.state = false;
        this._propertiesToShow = {
          title: '¡Agregado correctamente!',
          text: `Los datos del nuevo medidor se agregaron correctamente.`,
          icon: 'success',
          iconColor: '#0284C9',
          timer: 2000,
          showConfirmButton: false,
          backdrop: false
        };
        this.messageService.showSwalBasicAlert(this._propertiesToShow);
        this.modalController.dismiss({
          save: true
        });
      },
      )
      .catch((error: any) => {
        this.state = false;
        console.log(error);
      }
      );
  }
}
