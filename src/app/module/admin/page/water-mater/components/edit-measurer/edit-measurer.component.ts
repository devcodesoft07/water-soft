/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-inferrable-types */
import { Component, Input, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Partner } from '@core/models/admin/partners/partner.model';
import { WaterMeter } from '@core/models/admin/water-meter.model';
import { ErrorsMessage } from '@core/models/errors.model';
import { WaterMeterFireService } from '@core/services/admin/water-meter/water-meter-fire.service';
import { WaterMeterService } from '@core/services/admin/water-meter/water-meter.service';
import { MessageService } from '@core/services/message/message.service';
import { ModalController, ToastController } from '@ionic/angular';
import { SwalComponent, SwalPortalTargets } from '@sweetalert2/ngx-sweetalert2';
import { Subscription } from 'rxjs';
import Swal, { SweetAlertOptions } from 'sweetalert2';

@Component({
  selector: 'app-edit-measurer',
  templateUrl: './edit-measurer.component.html',
  styleUrls: ['./edit-measurer.component.scss'],
})
export class EditMeasurerComponent implements OnInit {
  @Input() data: WaterMeter;
  @Input() opt: string;
  @Input() dataPartner: Partner;
  @Input() optionPartner: string;
  measurerForm: UntypedFormGroup;
  today: string = new Date().toISOString();

  meterPattern: string = '^[a-zA-ZÀ-ÿ\u00f1\u00d10-9 .-]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d10-9 .-]*)*[a-zA-ZÀ-ÿ\u00f1\u00d10-9 .-]+$';
  state: boolean = false;
  errors: any;
  private _propertiesToShow: SweetAlertOptions;
  private _subscription: Subscription;

  constructor(
    private formBuilder: UntypedFormBuilder,
    private modalController: ModalController,
    private toastController: ToastController,
    private waterMeterService: WaterMeterService,
    public readonly swalTargets: SwalPortalTargets,
    private _waterMeterService: WaterMeterFireService,
    private messageService: MessageService
  ) {
    this.buildForm();
    this._propertiesToShow = {
      title: '¿Estas seguro de actualizar los datos de este medidor?',
      text: 'Verifica que los datos estén correctamente configurados',
      showConfirmButton: true,
    };
    this._subscription = new Subscription();
  }

  ngOnInit() {
    if (this.optionPartner === 'statusPartner') {
    } else {
      this.measurerForm.patchValue(this.data);
    }

    this.measurerForm.controls.selection.valueChanges.subscribe(
      (res: string) => {
        if (res !== 'Otro') {
          this.measurerForm.controls.observation.setValue(res);
        }
      }
    );
    this.measurerForm.controls.active.valueChanges.subscribe(
      (res: boolean) => {
        if (res) {
          this.measurerForm.controls.observation.setValue('Ninguna');
        }
      }
    );
    if (this.opt === 'changeStatus' || this.optionPartner === 'statusPartner') {
      this.measurerForm.controls.active.setValue(false);
    }
  }

  buildForm(): void {
    this.measurerForm = this.formBuilder.group({
      id: [''],
      partner_action: [],
      used: [],
      partner: [],
      expenses: [],
      register_date: [],
      water_meter_number: ['', [Validators.required, Validators.maxLength(9), Validators.pattern(this.meterPattern)]],
      unit_cost: ['',
        [Validators.required, Validators.min(1), Validators.max(99999999999999.99)]
      ],
      bought: [this.today, [Validators.required]],
      active: [false],

      observation: ['', [Validators.maxLength(255)]],
      selection: ['Disponible']
    });
  }

  closeModal() {
    this.modalController.dismiss({
      save: false
    });
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Solo se permiten 2 decimales',
      duration: 2000,
      color: 'warning',
      mode: 'ios'
    });
    toast.present();
  }

  validateDecimal(event: any): void {
    const unitCost: string = this.measurerForm.controls.unit_cost.value + '';
    const unitCostDecimal: string[] = unitCost.split('.' || ',');
    if (unitCostDecimal.length >= 2) {
      if (unitCostDecimal[1].length > 2) {
        const unitCostRound = parseFloat(unitCost).toFixed(2);
        this.measurerForm.controls.unit_cost.setValue(unitCostRound);
        this.presentToast();
      }
    };
  }

  saveChanges(): void {
    this.state = true;
    const data: WaterMeter = this.measurerForm.value;
    const waterMeterSubscription: Subscription = this.messageService.showSwalConfirmAlert(this._propertiesToShow)
      .subscribe(
        (isConfirm: boolean) => {
          if (isConfirm) {
            this.saveData(data);
          }
        }
      );
    this._subscription.add(waterMeterSubscription);
  }

  private saveData(waterMeter: WaterMeter): void {
    this._waterMeterService.update(waterMeter)
      .then(() => {
        this.state = false;
        this._propertiesToShow = {
          title: 'Actualizado correctamente!',
          text: `Los datos del medidor se actualizaron correctamente.`,
          icon: 'success',
          iconColor: '#0284C9',
          timer: 2000,
          showConfirmButton: false,
          backdrop: false
        };
        this.messageService.showSwalBasicAlert(this._propertiesToShow);
        this.modalController.dismiss({
          save: true
        });
      },
      )
      .catch((error: any) => {
        this.state = false;
        console.log(error);
      }
      );
  }

  confirmToActive(seccesSwal: SwalComponent): void {
    Swal.fire({
      title: '¡Cambiando Estado...!',
      text: 'Espere un momento, validando datos...',
      backdrop: false,
      didOpen: () => {
        Swal.showLoading();
      },
      willOpen: () => {
        this.measurerForm.controls.active.setValue(false);
        setTimeout(() => {
          this.waterMeterService.updateWaterMeter(this.measurerForm.value).subscribe(
            (res: WaterMeter) => {
              Swal.close();
              seccesSwal.fire();
            },
            (error: ErrorsMessage) => {
              console.log(error);
            }
          );
        }, 2000);
      },
    }).then((result) => {
      /* Read more about handling dismissals below */
      if (result.isDismissed) {
        // console.log('I was closed by the timer');
        this.modalController.dismiss({
          save: true
        });
      }
    });
  }

  cancelBtn(): void {
    this.modalController.dismiss({
      save: true
    });
  }
}
