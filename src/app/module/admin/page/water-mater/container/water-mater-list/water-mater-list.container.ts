/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-inferrable-types */
/* eslint-disable @angular-eslint/component-class-suffix */
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { WaterMeter } from '@core/models/admin/water-meter.model';
import { ErrorsMessage } from '@core/models/errors.model';
import { WaterMeterService } from '@core/services/admin/water-meter/water-meter.service';
import { PopoverController } from '@ionic/angular';
import { ToggleChangeEventDetail } from '@ionic/core';
import { MenuListComponent } from '@shared/components/menu-list/menu-list.component';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { ModalController } from '@ionic/angular';
import { EditMeasurerComponent } from '@module/admin/page/water-mater/components/edit-measurer/edit-measurer.component';
import Swal from 'sweetalert2';
import { StatusWaterMeterEnum } from '@core/enum/status-water-meter.enum';

@Component({
  selector: 'app-water-mater-list',
  templateUrl: './water-mater-list.container.html',
  styleUrls: ['./water-mater-list.container.scss']
})
export class WaterMaterListContainer implements OnInit {
  @Input() waterMeterList: WaterMeter[];
  @Output() reloadEventEmitter: EventEmitter<any> = new EventEmitter();
  waterMeter: WaterMeter;
  options: any = {
    title: '¡Cambiado correctamente!',
    text: 'Los datos del medidor guardaron correctamente.',
    icon: 'success',
    iconColor: '#0284C9',
    backdrop: false,
    timer: 2500,
    showCancelButton: false,
    showConfirmButton: false
  };

  public statusWaterMeterEnum = StatusWaterMeterEnum;

  constructor(
    public popoverController: PopoverController,
    private formBuilder: UntypedFormBuilder,
    private waterMeterService: WaterMeterService,
    private modalController: ModalController
  ) { }

  ngOnInit(): void {
  }

  async changeState(event: any, waterMeter: WaterMeter, confirSwal: SwalComponent) {
    const detail: ToggleChangeEventDetail = event.detail;
    this.waterMeter = await waterMeter;
    if (detail.checked) {
      await confirSwal.fire();
    } else {
      this.changeToInactive(waterMeter, 'changeStatus');
    }
  }

  async changeToInactive(waterMeter: WaterMeter, option: string) {
    const modal = await this.modalController.create({
      component: EditMeasurerComponent,
      mode: 'ios',
      componentProps: {
        data: waterMeter,
        opt: option
      }
    });
    await modal.present();


    await modal.onDidDismiss()
      .then(
        (res: any) => {
          const reload = res.data.save ? res.data.save : undefined;
          if (reload) {
            this.reloadEventEmitter.emit(true);
          }
        }
      )
      .catch(
        (error: any) => {
          console.log(error);
        }
      );
  }

  async managementMeasure(ev: any, data: WaterMeter) {
    const popover = await this.popoverController.create({
      component: MenuListComponent,
      event: ev,
      componentProps: {
        waterMeter: data,
        option: 'update'
      }
    });
    await popover.present();

    await popover.onDidDismiss()
      .then(
        (res: any) => {
          const reloading = res.data ? res.data.reloadPage : false;
          if (reloading) {
            this.reloadEventEmitter.emit(true);
          }
        }
      )
      .catch(
        (error: any) => {
          console.log(error);
        }
      );
  }

  confirmToActive(seccesSwal: SwalComponent): void {
    const stateChangeForm: UntypedFormGroup = this.formBuilder.group({
      id: [''],
      meter_number: [''],
      unit_cost: [''],
      bought: [''],
      active: [''],
      observation: [''],
    });
    stateChangeForm.patchValue(this.waterMeter);

    Swal.fire({
      title: '¡Cambiando Estado...!',
      text: 'Espere un momento, validando datos...',
      backdrop: false,
      didOpen: () => {
        Swal.showLoading();
      },
      willOpen: () => {
        stateChangeForm.controls.active.setValue(true);
        stateChangeForm.controls.observation.setValue('Ninguna');
        setTimeout(() => {
          this.waterMeterService.updateWaterMeter(stateChangeForm.value).subscribe(
            (res: WaterMeter) => {
              Swal.close();
              seccesSwal.fire();
            },
            (error: ErrorsMessage) => {
              console.log(error);
            }
          );
        }, 2000);
      },
    }).then((result) => {
      /* Read more about handling dismissals below */
      if (result.isDismissed) {
        // console.log('I was closed by the timer');
        this.reloadEventEmitter.emit(true);
      }
    });
  }
}
