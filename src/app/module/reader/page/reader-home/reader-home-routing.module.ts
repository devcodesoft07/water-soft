import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReaderHomePage } from './reader-home.page';

const routes: Routes = [
  {
    path: '',
    component: ReaderHomePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReaderHomePageRoutingModule {}
