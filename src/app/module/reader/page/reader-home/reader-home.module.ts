import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReaderHomePageRoutingModule } from './reader-home-routing.module';

import { ReaderHomePage } from './reader-home.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReaderHomePageRoutingModule
  ],
  declarations: [ReaderHomePage]
})
export class ReaderHomePageModule {}
