import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReaderPage } from './reader.page';

const routes: Routes = [
  {
    path: '',
    component: ReaderPage
  },
  {
    path: 'reader-home',
    loadChildren: () => import('./page/reader-home/reader-home.module').then( m => m.ReaderHomePageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReaderPageRoutingModule {}
