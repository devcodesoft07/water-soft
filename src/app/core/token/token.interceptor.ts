/* eslint-disable @typescript-eslint/naming-convention */
import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';

import { catchError } from 'rxjs/operators';
import { StorageService } from '@core/services/storage/storage.service';
import { AuthConstants } from '@core/config/auth-constanst';

@Injectable(
  // { providedIn: 'root' }
  )
export class InterceptorToken implements HttpInterceptor {
  token: string;

  constructor(
    private router: Router,
    private localStorageService: StorageService,

  ) {}

  intercept(
    req: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    // this.localStorageService.get('authToken').then(token => this.token = token);
    // const token: string = localStorage.getItem('authToken');
    this.localStorageService.get(AuthConstants.auth).then(
      (res: any)=>{
        this.token = res;
      }
    ).catch(
      (res: any)=>{
        this.token = null || undefined;
      }
    );

    // const token: string = localStorage.getItem('authToken');
    let re = req;
    if (this.token) {
      re = req.clone({
        setHeaders: {
          'Access-Control-Allow-Headers': 'Origin',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + this.token,
        },
      });
    }
    return next.handle(re)
      .pipe(
        catchError(
          (error) => {
            console.warn(error);
            if(error === 'Token has expired' || error === 'Unauthenticated.'){
              this.localStorageService.clear();
              this.router.navigate(['/']);
            }
            return throwError(error);
          }
        )
      );
  }
}
