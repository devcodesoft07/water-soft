export enum FirebaseErrorEnum {
  WRONG_PASSWORD = 'auth/wrong-password',
  INVALID_CREDENTIAL = 'auth/invalid-credential',
  INVALID_EMAIL = 'auth/invalid-email',
  USER_NOT_FOUND = 'auth/user-not-found',
}
