/* eslint-disable @typescript-eslint/naming-convention */
export enum ConstanstEnum {
  PATTERN_ONLY_TEXT = '^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1 ]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1 ]+$',
  PATTERN_ONLY_TEXT_NUMBER = '[A-Za-z0-9]*',
  DATE_FORMATE = 'YYYY-MM-DD h:mm:ss',
  PATTERN_ONLY_NUMBER = '^[0-9]*$',
  PATTERN_PHONE = '^[6-7,4]+[0-9]+$',
  BS_COIN = 'Bs',
  DOLLAR_COIN = '$us',
}
