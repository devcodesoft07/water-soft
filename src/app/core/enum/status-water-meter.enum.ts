export enum StatusWaterMeterEnum {
    ACTIVE = 'Activo',
    INACTIVE = 'Inactivo',
    BROKEN = 'Med. Averiado',
    CUT_NOT_PAYMENT = 'En Corte',
    DEFAULT = 'Ninguna',
    DEBTOR = 'Deudor',
    FREE = 'Libre',
    ASIGNE = 'Asignado'
}