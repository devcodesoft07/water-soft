/* eslint-disable @typescript-eslint/naming-convention */
export interface User {
  id: number;
  name: string;
  last_name: string;
  role_id: string;
  email: string;
  email_verified_at: string;
}
