export interface Response {
  status: boolean;
  message: string;
  data: any;
}

export interface ResponseApiFunctions<T> {
  message: string;
  status: number;
  data: T;
}
