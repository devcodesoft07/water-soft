import { Paginator } from './paginator.model';

export interface PaginatorWithChart {
  data: Paginator;
  chart: any;
  meter?: Paginator;
}
