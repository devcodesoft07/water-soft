export interface Debt {
  id?: string;
  description: string;
  amount: number;
}
