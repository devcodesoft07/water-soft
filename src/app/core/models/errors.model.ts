import { HttpHeaders } from '@angular/common/http';
import { InternalError } from './internal-error.model';

export interface ErrorsMessage {
	error?: InternalError;
	status: number;
	message?: string;
  header?: HttpHeaders;
  name?: string;
  ok?: boolean;
  statusText?: string;
  url?: string;
}
