import { PartnerAction } from './partners/partner-action.model';
import { Partner } from './partners/partner.model';

/* eslint-disable @typescript-eslint/naming-convention */
export interface WaterMeter {
  id: string;
  water_meter_number: string;
  active: boolean;
  used: boolean;
  register_date: Date;
  partner?: Partner;
  partner_action?: PartnerAction;
  meter_broken?: boolean;
  water_cut?: boolean;
  observation?: string;
}
