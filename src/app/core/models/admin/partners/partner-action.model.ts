import { WaterMeter } from '../water-meter.model';

/* eslint-disable @typescript-eslint/naming-convention */
export interface PartnerAction {
  id?: string;
  payment_method: string;
  quotas_number: number;
  quota: number;
  payment_way: string;
  voucher: string;
  payment_date: Date;
  next_payment: Date;
  water_meter: WaterMeterAction;
  partner_id: string;
}

export interface WaterMeterAction {
  measurer: WaterMeter;
  // sold: boolean;
  // category: string;
  // voucher: string;
  // payment_way: string;
  // payment: number;
  // sale_date: Date;
}
