export interface QuotasPlan {
  datePayment: Date;
  money: number;
  isPaitOut: boolean;
}
