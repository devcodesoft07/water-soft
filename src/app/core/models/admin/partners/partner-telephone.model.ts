/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable id-blacklist */
import { CreateUpdateModel } from '../create-update.model';

export interface PartnerTelephone extends CreateUpdateModel {
  id: number;
  number: string;
  pivot: Pivot;
}


interface Pivot {
  partner_id: number;
  telephone_id: number;
}
