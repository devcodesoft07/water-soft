import { WaterCut } from '../water-cut/WaterCut';
import { WaterMeter } from '../water-meter.model';
import { PartnerCommunity } from './partner-community.model';
import { Timestamp } from '@angular/fire/firestore';

/* eslint-disable @typescript-eslint/naming-convention */
export interface Partner {
  id?: string;
  name: string;
  last_name: string;
  identity_card: IdentityCardPartner;
  spouse: string;
  community: PartnerCommunity;
  telephones: string[];
  membership: Date;
  sewerage: boolean;
  active: boolean;
  gender?: string;
  waterMeter?: WaterMeter;
  waterCut?: WaterCut;
}

export interface IdentityCardPartner {
  ci_number: number;
  complement: string;
  issued: string;
}

export interface PartnerReader {
  partner: Partner;
  water_consumed: PayByPartner[];
}

export interface PayByPartner {
  id?: string;
  dateToPay: Date | Timestamp;
  amount: number;
  debts: number;
  water_consume: number;
  isPay: boolean;
  dateToRead: Date | Timestamp | string;
  sewerage: number;
}
