import { CreateUpdateModel } from '../create-update.model';

/* eslint-disable @typescript-eslint/naming-convention */
export interface PartnerCommunity extends CreateUpdateModel {
  id: number;
  name: string;
  district_id: number;
  district: string;
  location: string;
}
