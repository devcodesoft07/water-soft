import { CreateUpdateModel } from '../create-update.model';

/* eslint-disable @typescript-eslint/naming-convention */
export interface IdentityCardPartner extends CreateUpdateModel {
  ci_number: number;
  complement: string;
  issued: string;
  partner_id: string;
}
