import { CommonDataCharts } from "./common-data-charts";

export interface DataLineChart extends CommonDataCharts {
  series: CommonDataCharts[]
}