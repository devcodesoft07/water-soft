import { Debt } from '@core/models/detbs.model';
import { Partner, PayByPartner } from '../partners/partner.model';

/* eslint-disable @typescript-eslint/naming-convention */
export interface Income {
  amount: number;
  coin: string;
  detail: string;
  date: Date | string;
  category: IncomeCatergory;
  partner: Partner[];
  debt: Debt[];
}

export interface IncomeCatergory {
  id: string;
  name: string;
  description: string;
  image: string;
  showEntryToRegister: boolean;
  type: string;
}

export interface RegisterOtherPayment extends Omit<Income, 'fine_id'> {
  id: number;
}

export interface RegisterFinePayment
  extends Omit<Income, 'amount' | 'detail' | 'fine_id'> {
  id: number;
  fine: Fine;
}

interface Fine {
  id: number;
  amount: number;
  detail: string;
  partner: string;
  paid: boolean;
  date: Date;
}

export interface FinesOfPartners {
  id: number;
  name: string;
  fines: any[];
}

export interface IncomesResponseFunctions {
  incomesData: Income[];
  totalIncomesByWaterConsume: PayByPartner[]
}