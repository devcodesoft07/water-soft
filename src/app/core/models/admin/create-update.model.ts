/* eslint-disable @typescript-eslint/naming-convention */
export interface CreateUpdateModel {
  created_at: Date;
  updated_at: Date;
}
