export interface PartnerLocation {
  id?: number;
  location: string;
  district: string;
}
