export interface WaterCut {
  id: string;
  reason: string;
  active: boolean;
  meter_broken: boolean;
  cut_non_payment: boolean;
  water_cut_date: string;
  water_meter_id: string;
  reconnection_date: string;
}