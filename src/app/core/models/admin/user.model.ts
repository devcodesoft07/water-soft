import { Timestamp } from '@angular/fire/firestore';

export interface IUser {
  id: string;
  name: string;
  role: ERole;
  avatar: string;
  email: string;
  password: string;
  dateStart: Timestamp | Date;
}

export enum ERole {
  SUBSCRIBER = 'SUBSCRIBER',
  ADMIN = 'ADMIN',
  READER = 'READER',
  DEBT_COLLECTOR = 'DEBT_COLLECTOR'
}

export interface IAuthUser {
  email: string;
  password: string;
}

export interface IAuthResponse {
  access_token: string;
  token_type: string;
  expires_at: string;
}
