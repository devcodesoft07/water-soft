/* eslint-disable @typescript-eslint/no-empty-interface */
/* eslint-disable @typescript-eslint/naming-convention */

import { ExpenseCategory } from './expense-category.model';
export interface Expense {
  amount: number;
  coin: string;
  expense_category: ExpenseCategory;
  passage: boolean;
  detail: string;
  date: Date;
}

export interface RegisterExpenseDTO extends Omit<Expense, 'expense_category_id'>{
  id: number;
  category: Category;
}

interface Category{
  id: number;
  name: string;
  description: string;
}
