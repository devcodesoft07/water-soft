import { Timestamp } from 'firebase/firestore';
import { WaterCut } from '../water-cut/WaterCut';
import { PayByPartner } from '../partners/partner.model';

export interface WaterConsumedResponse {
  historicalWaterCut: WaterCut[];
  waterConsumeByMonths: PayByPartner[];
}

export interface WaterConsumed {
  id?: string;
  dateToPay: Date | Timestamp;
  dateToRead: Date | string;
  readering_last: number;
  readering_actual: number;
  amount: number;
  debts: number;
  water_consume: number;
  isPay: boolean;
  sewerage: number;
}
