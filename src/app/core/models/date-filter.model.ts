export interface DateFilter {
  month?: number;
  year: number;
}
