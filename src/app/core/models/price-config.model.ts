export interface PriceConfig {
  action_price: number;
  water_meter_price: number;
  first_water_price: number;
  second_water_price: number;
  third_water_price: number;
  first_range: number;
  second_range: number;
  minimal_price: number;
  debts: number;
  sewerage_price: number;
}
