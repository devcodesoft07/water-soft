export interface InternalError {
  status: boolean;
  message: string;
  errors: any;
}
