export class AuthConstants {
    public static readonly auth = 'authToken';
    public static readonly titleNav = 'title_nav';
    public static readonly user = 'user';
    public static readonly rol = 'rol';
    public static readonly idProcess = 'idProcessGroup';
    public static readonly DATA_STATE = 'data_state';
    public static readonly FIRST_COMPLETE = 'isFirstComplete';
    public static readonly SECOND_COMPLETE = 'isSecondComplete';
    public static readonly THIRD_COMPLETE = 'isThirdComplete';
    public static readonly PARTNER = 'newPartner';
    public static readonly PARTNER_ACTION = 'newPartnerAction';
    public static readonly AUTH = 'authToken';
    public static readonly CREDENTIALS = 'credentials';
    public static readonly USER = 'user';
}
