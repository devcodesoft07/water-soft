import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DynamicColorService {

  constructor() { }

  public changeBackgroundRow(index: number): string {
    return (index % 2 === 0) ? '#ffffff' : '#D7DDE402';
  }
}
