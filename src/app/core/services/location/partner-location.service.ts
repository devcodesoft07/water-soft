import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PartnerLocation } from '@core/models/admin/location/location.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PartnerLocationService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getAllLocation(): Observable<PartnerLocation[]> {
    return this.httpClient.get<PartnerLocation[]>('assets/json/location.json')
    .pipe(
      map(
        (res: any) => res.data as PartnerLocation[]
      )
    );
  }
}
