import { AuthConstants } from './../../config/auth-constanst';
import { User } from './../../models/user.model';
import { StorageService } from './../storage/storage.service';
import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';
import { Router } from '@angular/router';
import { AuthUser } from '../../models/auth.model';
import { Observable, throwError  } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Response } from '../../models/response.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private httpService: HttpService,
    private storageService: StorageService,
    private router: Router
  ) {
  }

  login(data: AuthUser): Observable<any>{
    return this.httpService.post('login', data)
    .pipe(
      catchError(
        this.handleError
      ),
      map(
        (res: Response) => {
          // console.log('rspuesta',res);
          const token: string = res.data.token;
          const dataUser: User = res.data.user;
          // console.log("DATAUSER",dataUser);
          const rol: string = res.data.user.role_id;
          this.storageService.store(AuthConstants.titleNav, 'Home');
          this.storageService.store(AuthConstants.auth, token);
          // localStorage.setItem('authToken',token);
          this.storageService.store(AuthConstants.user, dataUser);
          this.storageService.store(AuthConstants.rol, rol);


          this.router.navigate(['admin']);
          return 'Bienvenido';
        },
      )
    );
  }

  logout(): Observable<any>{
    const authToken: string = localStorage.getItem('authToken') ? localStorage.getItem('authToken') : 'No existe token';
    const tokenDecode: string = JSON.parse(unescape(atob(authToken)));
    return this.httpService.post('logout', {
      token: tokenDecode
    })
    .pipe(
      catchError(
        this.handleError
      ),
      map(
        (res: Response) => {
          this.storageService.remove(AuthConstants.auth);
          this.storageService.remove(AuthConstants.user);
          this.storageService.remove(AuthConstants.rol);
          this.storageService.remove(AuthConstants.titleNav);
          return true;
        },
      )
    );
  }

  private handleError(httpResponse: HttpErrorResponse) {
    const error = httpResponse.error;
     return throwError(error);
  }

}
