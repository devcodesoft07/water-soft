import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpService } from '../http/http.service';
import firebase from 'firebase/compat/app';
import { CookieService } from 'ngx-cookie-service';
import { AuthConstants } from '../../config/auth-constanst';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { IUser, IAuthResponse, IAuthUser } from '../../models/admin/user.model';
import { GoogleAuthProvider } from 'firebase/auth';


@Injectable({
  providedIn: 'root'
})
export class AuthFireService {
  private provider: GoogleAuthProvider;
  constructor(
    private httpService: HttpService,
    private fireAuthService: AngularFireAuth,
    private cookieService: CookieService,
    private fireStoreService: AngularFirestore
  ) {
    this.provider = new GoogleAuthProvider();
  }

  login(credentials: IAuthUser): Observable<IAuthResponse> {
    return this.httpService.post('login', credentials)
    .pipe(
      map(
        (res: any) => res as IAuthResponse
      )
    );
  }

  loginFire(credentials: IAuthUser): Promise<firebase.auth.UserCredential> {
    return this.fireAuthService.signInWithEmailAndPassword(credentials.email, credentials.password);
  }

  registerFire(credentials: IAuthUser): Promise<firebase.auth.UserCredential> {
    return this.fireAuthService.createUserWithEmailAndPassword(credentials.email, credentials.password);
  }

  logoutFire(): Promise<void> {
    return this.fireAuthService.signOut();
  }

  getUserCredendials(): firebase.auth.UserCredential | IUser {
    let user: firebase.auth.UserCredential | IUser = {
      credential: null,
      user: null
    };
    if (this.cookieService.check(AuthConstants.AUTH)) {
      user = JSON.parse(this.cookieService.get(AuthConstants.AUTH)) as IUser;
    }
    return user;
  }

  getUserByUid(uid: string | undefined): Observable<IUser & {id: string} | undefined> {
    return this.fireStoreService.collection<IUser>('users').doc(uid).valueChanges({ idField: 'id' });
  }

  authStateChange(): Promise<any> {
    return this.fireAuthService.onAuthStateChanged(
      (res: any) => {
        console.log(res);
      }
    );
  }

  public signInWithGoogle(): Promise<firebase.auth.UserCredential> {
    return this.fireAuthService.signInWithPopup(this.provider);
  }

  public isAuthenticated(): Observable<boolean> {
    const isAuthenticated = this.cookieService.check(AuthConstants.AUTH);
    const isUserData = this.cookieService.check(AuthConstants.user);
    return of(isAuthenticated && isUserData);
  }
}

