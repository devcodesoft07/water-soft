import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { PriceConfig } from '@core/models/price-config.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PriceService {
  collectionName = 'price';
  id = 'fLOSyEip1e7E16RuzCyr';
  constructor(
    private firestoreService: AngularFirestore
  ) { }

  getData(): Observable<(PriceConfig & { id: string }) | undefined> {
    return this.firestoreService.collection<PriceConfig>(this.collectionName).doc(this.id).valueChanges({ idField: 'id' });
  }

  update(data: PriceConfig): Promise<void> {
    return this.firestoreService.collection<PriceConfig>(this.collectionName).doc(this.id).update(data);
  }
}
