import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/compat/firestore';
import { PartnerAction } from '@core/models/admin/partners/partner-action.model';
import { Partner } from '@core/models/admin/partners/partner.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PartnerActionFireService {
  collectionName = 'partner';
  subCollectionName = 'action';
  constructor(
    private firestoreService: AngularFirestore
  ) { }

  getAll(partnerId: string): Observable<PartnerAction[]> {
    return this.firestoreService.collection<Partner>(this.collectionName)
    .doc(partnerId)
    .collection<PartnerAction>(this.subCollectionName)
    // return this.firestoreService.collection<PartnerAction>(this.collectionName, ref => ref.orderBy('register_date', 'asc'))
    .valueChanges({ idField: 'id' });
  }

  getData(partnerId: string, id: string): Observable<(PartnerAction & { id: string }) | undefined> {
    return this.firestoreService
    .collection<Partner>(this.collectionName)
    .doc(partnerId)
    .collection<PartnerAction>(this.subCollectionName).doc(id).valueChanges({ idField: 'id' });
  }

  add(partnerId: string, data: PartnerAction): Promise<DocumentReference<PartnerAction>> {
    return this.firestoreService
    .collection<Partner>(this.collectionName)
    .doc(partnerId)
    .collection<PartnerAction>(this.subCollectionName).add(data);
  }

  update(partnerId: string, data: PartnerAction | any): Promise<void> {
    return this.firestoreService
    .collection<Partner>(this.collectionName)
    .doc(partnerId)
    .collection<PartnerAction>(this.subCollectionName).doc(data.id).update(data);
  }

  delete(partnerId: string, data: PartnerAction | any): Promise<void> {
    return this.firestoreService
    .collection<Partner>(this.collectionName)
    .doc(partnerId)
    .collection<PartnerAction>(this.subCollectionName).doc(data.id).delete();
  }
}
