import { Injectable } from '@angular/core';
import { PartnerAction } from '@core/models/admin/partners/partner-action.model';
import { HttpService } from '@core/services/http/http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PartnerActionService {

  constructor(private httpService: HttpService) { }

  public addPartnerAction(partnerAction: PartnerAction): Observable<any> {
    return this.httpService.post('actions', partnerAction);
  }
}
