import { Injectable } from '@angular/core';
import { ExpenseCategory } from '@core/models/admin/expenses/expense-category.model';
import { Expense, RegisterExpenseDTO } from '@core/models/admin/expenses/expense.model';
import { Response } from '@core/models/response.model';
import { HttpService } from '@core/services/http/http.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class ExpensesService {

  constructor(
    private httpService: HttpService
  ) { }

  getAllCategories(): Observable<ExpenseCategory[]>{
    return this.httpService.get('expense-categories')
    .pipe(
      map(
        (res: Response) => res.data as ExpenseCategory[]
      )
    );
  }

  registerExpense(expenseData: Expense): Observable<RegisterExpenseDTO>{
    return this.httpService.post('expenses', expenseData)
    .pipe(
      map(
        (res: Response) => res.data as RegisterExpenseDTO
      )
    );
  }
}
