import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { ExpenseCategory } from '@core/models/admin/expenses/expense-category.model';

@Injectable({
  providedIn: 'root'
})
export class ExpensesCategoryFireService {
  collectionName = 'expenses-category';
  constructor(
    private firestoreService: AngularFirestore
  ) { }

  getAll(): Observable<ExpenseCategory[]> {
    return this.firestoreService.collection<ExpenseCategory>(this.collectionName)
    // return this.firestoreService.collection<ExpenseCategory>(this.collectionName, ref => ref.orderBy('register_date', 'asc'))
    .valueChanges({ idField: 'id' });
  }

  getData(id: string): Observable<(ExpenseCategory & { id: string }) | undefined> {
    return this.firestoreService.collection<ExpenseCategory>(this.collectionName).doc(id).valueChanges({ idField: 'id' });
  }

  add(data: ExpenseCategory): Promise<DocumentReference<ExpenseCategory>> {
    return this.firestoreService.collection<ExpenseCategory>(this.collectionName).add(data);
  }

  update(data: ExpenseCategory | any): Promise<void> {
    return this.firestoreService.collection<ExpenseCategory>(this.collectionName).doc(data.id).update(data);
  }

  delete(data: ExpenseCategory | any): Promise<void> {
    return this.firestoreService.collection<ExpenseCategory>(this.collectionName).doc(data.id).delete();
  }
}
