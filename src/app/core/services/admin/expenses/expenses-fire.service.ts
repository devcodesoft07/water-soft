import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/compat/firestore';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import { Expense } from '@core/models/admin/expenses/expense.model';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ExpensesFireService {
  collectionName = 'expenses';
  constructor(
    private firestoreService: AngularFirestore,
    private functions: AngularFireFunctions
  ) { }

  getAll(): Observable<Expense[]> {
    return this.firestoreService.collection<Expense>(this.collectionName)
      // return this.firestoreService.collection<Expense>(this.collectionName, ref => ref.orderBy('register_date', 'asc'))
      .valueChanges({ idField: 'id' });
  }

  getAllByDate(startDate: string, endDate: string): Observable<Expense[]> {
    return this.firestoreService
      .collection<Expense>(this.collectionName, (ref) => {
        return ref.orderBy('date', 'desc').where('date', '>=', startDate).where('date', '<=', endDate);
      })
      .valueChanges({ idField: 'id' });
    // return this.firestoreService.collection<Expense>(this.collectionName)
  }

  getData(id: string): Observable<(Expense & { id: string }) | undefined> {
    return this.firestoreService.collection<Expense>(this.collectionName).doc(id).valueChanges({ idField: 'id' });
  }

  add(data: Expense): Promise<DocumentReference<Expense>> {
    return this.firestoreService.collection<Expense>(this.collectionName).add(data);
  }

  update(data: Expense | any): Promise<void> {
    return this.firestoreService.collection<Expense>(this.collectionName).doc(data.id).update(data);
  }

  delete(data: Expense | any): Promise<void> {
    return this.firestoreService.collection<Expense>(this.collectionName).doc(data.id).delete();
  }

  generarReportePDF(startDate: string, endDate: string, monthName: string): Observable<any> {
    const callable = this.functions.httpsCallable('getExpensesReportByMonth');
    return callable({
      startDate,
      endDate,
      monthName
    });
  }
}
