/* eslint-disable max-len */
import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/compat/firestore';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import { Partner } from '@core/models/admin/partners/partner.model';
import { WaterConsumed } from '@core/models/admin/water-consumed/water-consumed.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PartnerWaterConsumeFireService {
  collectionName = 'partner';
  subCollectionName = 'water-consume';
  constructor(
    private firestoreService: AngularFirestore,
    private functions: AngularFireFunctions
  ) { }

  getAll(partner: Partner): Observable<WaterConsumed[]> {
    return this.firestoreService.collection<Partner>(this.collectionName).doc(partner.id).collection<WaterConsumed>(this.subCollectionName)
      // return this.firestoreService.collection<Partner>(this.collectionName, ref => ref.orderBy('register_date', 'asc'))
      .valueChanges({ idField: 'id' });
  }

  getData(partner: Partner, id: string): Observable<(WaterConsumed & { id: string }) | undefined> {
    return this.firestoreService.collection<Partner>(this.collectionName).doc(partner.id).collection<WaterConsumed>(this.subCollectionName).doc(id).valueChanges({ idField: 'id' });
  }

  add(data: WaterConsumed, partner: Partner): Promise<DocumentReference<WaterConsumed>> {
    return this.firestoreService.collection<Partner>(this.collectionName).doc(partner.id).collection<WaterConsumed>(this.subCollectionName).add(data);
  }

  update(data: WaterConsumed | any, partner: Partner): Promise<void> {
    return this.firestoreService.collection<Partner>(this.collectionName).doc(partner.id).collection(this.subCollectionName).doc(data.id).update(data);
  }

  delete(data: WaterConsumed | any, partner: Partner): Promise<void> {
    return this.firestoreService.collection<Partner>(this.collectionName).doc(partner.id).collection(this.subCollectionName).doc(data.id).delete();
  }

  getWaterConsumeByPartnerForDate(dateStart, dateEnd, partnerId, partnerActionId): Observable<any> {
    const callable = this.functions.httpsCallable('getWaterConsumeByPartnerForDate');
    return callable({
      dateStart,
      dateEnd,
      partnerId,
      partnerActionId
    })
  }

  getWaterConsumeByPartnerIdForDate(dateStart, dateEnd, partnerId): Observable<any> {
    const callable = this.functions.httpsCallable('getPaymentsByPartnerId');
    return callable({
      dateStart,
      dateEnd,
      partnerId
    })
  }

  addWaterConsumeReadingByPartner(dateStart, dateEnd, partnerId, data): Observable<any> {
    const callable = this.functions.httpsCallable('addWaterConsumeReadingByPartner');
    return callable({
      dateStart,
      dateEnd,
      partnerId,
      data
    })
  }
}
