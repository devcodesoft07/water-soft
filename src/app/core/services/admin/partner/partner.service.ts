import {HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Partner } from '@core/models/admin/partners/partner.model';
import { Paginator } from '@core/models/paginator.model';
import { Response } from '@core/models/response.model';
import { HttpService } from '@core/services/http/http.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '@evironments/environment';


@Injectable({
  providedIn: 'root'
})
export class PartnerService {
  api: string = environment.apiUrl;

  constructor(
    private httpService: HttpService,
    private httpClient: HttpClient
    ) {

  }

  getAllPartners(parameter: number): Observable<Paginator> {
    let params = new HttpParams();
    params = params.set('eliminated', parameter );

    return this.httpClient.get(this.api + 'partners', {params})
    .pipe(
      map(
        (res: Response) => res.data as Paginator
      )
    );
  }

  // getAllPartnersOficial(): Observable<Paginator> {
  //   return this.httpService.get('partners')
  //   .pipe(
  //     map(
  //       (res: Response) => res.data as Paginator
  //     )
  //   );
  // }

  getPartner(id: number): Observable<Partner> {
    return this.httpService.get('partners/' + id)
    .pipe(
      map(
        (res: Response) => res.data as Partner
      )
    );
  }

  addPartner(partner: Partner): Observable<Partner> {
    return this.httpService.post('partners', partner)
    .pipe(
      map(
        (res: Response) => res.data as Partner
      )
    );
  }

  editPartner(partner: Partner, id: number): Observable<Partner> {
    return this.httpService.put('partners/' + id, partner)
    .pipe(
      map(
        (res: Response) => res.data as Partner
      )
    );
  }

  verifyExistPartner(ciNumber: number): Observable<boolean> {
    return this.httpService.get('identity-cards/' + ciNumber)
    .pipe(
      map(
        (res: Response) => res.data as boolean
      )
    );
  }

  deletePartner(id: number): Observable<boolean>{
    return this.httpService.delete('partners/'+id)
    .pipe(
      map(
        (res: Response | any) => {
          console.log(res);
          return true;
        }
      )
    );
  }

  assignMeterToPartner(dataForm: any): Observable<any> {
    return this.httpService.post('meter-sales', dataForm)
    .pipe(
      map(
        (res: Response | any) => res
      )
    );
  }
}
