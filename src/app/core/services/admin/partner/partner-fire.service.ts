import { Injectable } from '@angular/core';
import { AngularFirestore, CollectionReference, DocumentReference, Query, QueryFn } from '@angular/fire/compat/firestore';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import { Partner } from '@core/models/admin/partners/partner.model';
import { ResponseApiFunctions } from '@core/models/response.model';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PartnerFireService {
  collectionName = 'partner';
  constructor(
    private firestoreService: AngularFirestore,
    private functions: AngularFireFunctions
  ) { }

  getAll(status: boolean | string | null = null, community: string): Observable<Partner[]> {

    const queryFn: QueryFn = (ref: CollectionReference) => {
      let query: Query = ref;

      if ((status || !status) && typeof status == 'boolean') {
        query = query.where('active', '==', status);
      }

      if (typeof status == 'string' && status === 'En Corte') {
        query = query.where('waterCut.active', '==', true);
      }

      if (community && community !== 'Todos los Socios') {
        query = query.where('community.location', '==', community);
      }

      return query.orderBy('last_name', 'asc');
    };

    return this.firestoreService.collection(this.collectionName, queryFn).snapshotChanges().pipe(
      map(snapshot => {
        return snapshot.map(docChange => {
          const data = docChange.payload.doc.data();
          return { id: docChange.payload.doc.id, ...data as any };
        });
      })
    );
  }

  getData(id: string): Observable<(Partner & { id: string }) | undefined> {
    return this.firestoreService.collection<Partner>(this.collectionName).doc(id).valueChanges({ idField: 'id' });
  }

  add(data: Partner): Promise<DocumentReference<Partner>> {
    const newPartner: Partner = {
      name: data.name,
      last_name: data.last_name,
      identity_card: data.identity_card,
      spouse: data.spouse,
      community: data.community,
      telephones: data.telephones,
      gender: data.gender,
      membership: data.membership,
      sewerage: data.sewerage,
      active: true
    };
    return this.firestoreService.collection<Partner>(this.collectionName).add(newPartner);
  }

  update(data: Partner | any): Promise<void> {
    return this.firestoreService.collection<Partner>(this.collectionName).doc(data.id).update(data);
  }

  delete(data: Partner | any): Promise<void> {
    return this.firestoreService.collection<Partner>(this.collectionName).doc(data.id).delete();
  }

  getPartnersDataToChart(): Observable<ResponseApiFunctions<any>> {
    const callable = this.functions.httpsCallable('getPartnersDataToChart');
    return callable({})
  }

  getPartnersReport(dateStart: string, dateEnd: string, location: string): Observable<any> {
    const callable = this.functions.httpsCallable('getPartnersReportService');
    return callable({
      dateStart,
      dateEnd,
      location
    }).pipe(
      map((result) => {
        if (result && result.data) {
          const base64String = result.data;
          const decodedBytes = new Uint8Array(atob(base64String).split('').map(char => char.charCodeAt(0)));
          return new Blob([decodedBytes], { type: 'application/pdf' });
        } else {
          throw new Error('La respuesta no tiene el formato esperado');
        }
      }),
      catchError((error) => {
        console.error('Error al generar el informe PDF:', error);
        return throwError('Error al generar el informe PDF');
      })
    );
  }
}
