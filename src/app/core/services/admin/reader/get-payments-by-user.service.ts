import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import { environment } from '@evironments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class GetPaymentsByUserService {
  private url: string = environment.apiUrlFireFunctions;
  constructor(
    private httpClient: HttpClient,
    private functions: AngularFireFunctions
  ) {}

  getPaymentsByUserByMonth(
    dateStart,
    dateEnd,
    location: string = 'Tajamar A'
  ): Observable<any> {
    const callable = this.functions.httpsCallable('getPayments');
    return callable({
      dateStart,
      dateEnd,
      location,
    });
  }

  getPaymentsByUserByMonthOnRequest(
    dateStart,
    dateEnd,
    location: string = 'Tajamar A'
  ): Observable<any> {
    let httpParams = new HttpParams();
    httpParams = httpParams.append('dateStart', dateStart);
    httpParams = httpParams.append('dateEnd', dateEnd);
    httpParams = httpParams.append('location', location);
    return this.httpClient.get(`${this.url}/getPayments`, {
      params: httpParams,
    });
  }
}
