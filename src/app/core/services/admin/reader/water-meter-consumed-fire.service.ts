import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/compat/firestore';
import { Partner } from '@core/models/admin/partners/partner.model';
import { WaterConsumed } from '@core/models/admin/water-consumed/water-consumed.interface';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class WaterMeterConsumedFireService {
  private collectionName: string;
  private subCollectionName: string;
  constructor(
    private firestoreService: AngularFirestore
  ) {
    this.collectionName = 'partner';
    this.subCollectionName = 'water-consume';
  }

  getAll(partner: Partner): Observable<WaterConsumed[]> {
    return this.firestoreService.collection<Partner>(this.collectionName)
      .doc(partner.id)
      .collection<WaterConsumed>(this.subCollectionName, (ref) => {
        return ref.orderBy('dateToRead', 'desc');
      })
      .valueChanges({ idField: 'id' });
  }

  getData(partner: Partner, id: string): Observable<(WaterConsumed & { id: string }) | undefined> {
    return this.firestoreService.collection<Partner>(this.collectionName).doc(partner.id)
      .collection<WaterConsumed>(this.subCollectionName).doc(id).valueChanges({ idField: 'id' });
  }

  add(partner: Partner, data: WaterConsumed): Promise<DocumentReference<WaterConsumed>> {
    return this.firestoreService.collection<Partner>(this.collectionName).doc(partner.id)
      .collection<WaterConsumed>(this.subCollectionName).add(data);
  }

  update(partner: Partner, data: WaterConsumed | any): Promise<void> {
    return this.firestoreService.collection<Partner>(this.collectionName).doc(partner.id)
      .collection<WaterConsumed>(this.subCollectionName).doc(data.id).update(data);
  }

  delete(partner: Partner, data: WaterConsumed | any): Promise<void> {
    return this.firestoreService.collection<Partner>(this.collectionName).doc(partner.id)
      .collection<WaterConsumed>(this.subCollectionName).doc(data.id).delete();
  }

}
