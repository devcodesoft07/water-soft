import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { WaterMeter } from '@core/models/admin/water-meter.model';
import { PaginatorWithChart } from '@core/models/paginator-chart.model';
import { Paginator } from '@core/models/paginator.model';
import { Response } from '@core/models/response.model';
import { HttpService } from '@core/services/http/http.service';
import { environment } from '@evironments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WaterMeterService {
  api: string = environment.apiUrl;
  constructor(
    private httpService: HttpService,
    private httpClient: HttpClient,
    private firestore: AngularFirestore
  ) { }

  getWaterMeters(page: number): Observable<Paginator> {
    let params = new HttpParams();
    params = params.set('page', page);
    // .set('month', month)
    // .set('year', year);
    return this.httpClient.get(this.api + 'meters', { params })
      .pipe(
        map(
          (res: Response) => res.data as Paginator
        )
      );
  }

  getWaterMetersWithFilters(optionSelected: boolean | string): Observable<PaginatorWithChart> {
    let params = new HttpParams();
    if (typeof (optionSelected) === 'number') {
      params = params.set('active', optionSelected);
    } else {
      params = params.set('observation', optionSelected);
    }
    return this.httpClient.get(this.api + 'meters', { params })
      .pipe(
        map(
          (res: Response) => {
            const resServer = res.data;
            const data: PaginatorWithChart = {
              data: resServer.meters,
              chart: resServer.charts
            };
            return data;
          }
        )
      );
  }

  getWaterMeterWithParams(
    page: number,
    month?: number,
    year?: number,
    active?: boolean,
    observation?: string
  ): Observable<PaginatorWithChart> {
    let params = new HttpParams();
    params = params.set('page', page)
      .set('year', year);
    return this.httpClient.get(this.api + 'meters', { params })
      .pipe(
        map(
          (res: Response) => {
            const resServer = res.data;
            const data: PaginatorWithChart = {
              data: resServer.meters,
              chart: resServer.charts
            };
            return data;
          }
        )
      );
  }

  searchWaterMeters(meterNumber: string): Observable<Paginator> {
    let params = new HttpParams();
    params = params.set('meterNumber', meterNumber);
    return this.httpClient.get(this.api + 'searhMeterNumber', { params })
      .pipe(
        map(
          (res: Response) => res.data as Paginator
        )
      );
  }

  addWaterMeter(data: WaterMeter): Observable<WaterMeter> {
    return this.httpService.post('meters', data).pipe(
      map(
        (res: Response) => res.data
      )
    );
  }
  deleteWaterMeter(data: WaterMeter): Observable<boolean> {
    return this.httpService.delete('meters/' + data.id).pipe(
      map(
        (res: Response | any) => {
          return true;
        }
      )
    );
  }
  updateWaterMeter(data: WaterMeter): Observable<WaterMeter> {
    return this.httpService.put('meters/' + data.id, data).pipe(
      map(
        (res: Response) => res.data
      )
    );
  }


  getWaterMeterWithObservation(observation: string): Observable<WaterMeter[]> {
    let params = new HttpParams();
    params = params.set('observation', observation);
    return this.httpClient.get(this.api + 'meters', { params })
      .pipe(
        map(
          (res: Response) => {
            const listMeter = res.data.meters.data as WaterMeter[];
            return listMeter;
          }
        )
      );
  }
}
