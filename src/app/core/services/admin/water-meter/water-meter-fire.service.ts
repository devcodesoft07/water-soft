import { Injectable } from '@angular/core';
import { AngularFirestore, CollectionReference, DocumentReference, Query, QueryFn } from '@angular/fire/compat/firestore';
import { WaterMeter } from '@core/models/admin/water-meter.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class WaterMeterFireService {
  collectionName = 'water-meter';
  constructor(
    private firestoreService: AngularFirestore
  ) { }

  getAll(status: boolean | string | null = null, year: number | null = null): Observable<WaterMeter[]> {
    // return this.firestoreService.collection<WaterMeter>(this.collectionName)

    // return this.firestoreService.collection<WaterMeter>(this.collectionName, ref => ref.orderBy('register_date', 'desc').where('active', '==', true))
    //   .valueChanges({ idField: 'id' });
    const queryFn: QueryFn = (ref: CollectionReference) => {
      let query: Query = ref;

      if ((status || !status) && typeof status == 'boolean') {
        query = query.where('active', '==', status);
      }

      if (typeof status == 'string' && status === 'Disponible') {
        query = query.where('used', '==', false);
      }

      if (typeof status == 'string' && status === 'En Corte') {
        query = query.where('water_cut', '==', true);
      }

      if (year) {
        const startDate = String(year);
        const endDate = String(year + 1);
        query = query.where('register_date', '>=', startDate).where('register_date', '<', endDate);
      }

      return query.orderBy('register_date', 'desc');
    };

    return this.firestoreService.collection(this.collectionName, queryFn).snapshotChanges().pipe(
      map(snapshot => {
        return snapshot.map(docChange => {
          const data = docChange.payload.doc.data();
          return { id: docChange.payload.doc.id, ...data as any };
        });
      })
    );
  }

  getData(id: string): Observable<(WaterMeter & { id: string }) | undefined> {
    return this.firestoreService.collection<WaterMeter>(this.collectionName).doc(id).valueChanges({ idField: 'id' });
  }

  add(data: WaterMeter): Promise<DocumentReference<WaterMeter>> {
    return this.firestoreService.collection<WaterMeter>(this.collectionName).add(data);
  }

  update(data: WaterMeter | any): Promise<void> {
    return this.firestoreService.collection<WaterMeter>(this.collectionName).doc(data.id).update(data);
  }

  delete(data: WaterMeter | any): Promise<void> {
    return this.firestoreService.collection<WaterMeter>(this.collectionName).doc(data.id).delete();
  }
}
