import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/compat/firestore';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import { Income, IncomesResponseFunctions } from '@core/models/admin/incomes/income.model';
import { ResponseApiFunctions } from '@core/models/response.model';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class IncomesFireService {
  collectionName = 'income';
  constructor(
    private firestoreService: AngularFirestore,
    private functions: AngularFireFunctions
  ) { }

  getAll(): Observable<Income[]> {
    return this.firestoreService.collection<Income>(this.collectionName)
      .valueChanges({ idField: 'id' });
  }

  getData(id: string): Observable<(Income & { id: string }) | undefined> {
    return this.firestoreService.collection<Income>(this.collectionName).doc(id).valueChanges({ idField: 'id' });
  }

  add(data: Income): Promise<DocumentReference<Income>> {
    return this.firestoreService.collection<Income>(this.collectionName).add(data);
  }

  update(data: Income | any): Promise<void> {
    return this.firestoreService.collection<Income>(this.collectionName).doc(data.id).update(data);
  }

  delete(data: Income | any): Promise<void> {
    return this.firestoreService.collection<Income>(this.collectionName).doc(data.id).delete();
  }

  getAllByDate(dateStart, dateEnd,): Observable<ResponseApiFunctions<IncomesResponseFunctions>> {
    const callable = this.functions.httpsCallable('getIncomesServiceByDate');
    return callable({
      dateStart,
      dateEnd
    })
  }

  generarReportToIncomesData(startDate: string, endDate: string, monthName: string): Observable<Blob> {
    const callable = this.functions.httpsCallable('getIncomesReportByMonth');
    return callable({
      startDate,
      endDate,
      monthName
    }).pipe(
      map((result) => {
        if (result && result.data) {
          const base64String = result.data;
          const decodedBytes = new Uint8Array(atob(base64String).split('').map(char => char.charCodeAt(0)));
          return new Blob([decodedBytes], { type: 'application/pdf' });
        } else {
          throw new Error('La respuesta no tiene el formato esperado');
        }
      }),
      catchError((error) => {
        console.error('Error al generar el informe PDF:', error);
        return throwError('Error al generar el informe PDF');
      })
    );
  }
}
