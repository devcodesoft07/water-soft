import { FinesOfPartners, RegisterFinePayment } from './../../../models/admin/incomes/income.model';
import { HttpService } from '@core/services/http/http.service';
import { Injectable } from '@angular/core';
import { Income, RegisterOtherPayment } from '@core/models/admin/incomes/income.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Response } from '@core/models/response.model';
import { Paginator } from '@core/models/paginator.model';

@Injectable({
  providedIn: 'root'
})
export class IncomesService {

  constructor(
    private httpService: HttpService
  ) { }

  registerOtherIncomes(incomeData: Income): Observable<RegisterOtherPayment>{
    return this.httpService.post('revenues', incomeData)
    .pipe(
      map(
        (res: Response) => res.data as RegisterOtherPayment
      )
    );
  }

  registerFinePayment(incomeData: Income): Observable<RegisterFinePayment>{
    return this.httpService.post('fine-payments', incomeData)
    .pipe(
      map(
        (res: Response) => res.data as RegisterFinePayment
      )
    );
  }

  getAllFinesOfPartners(): Observable<FinesOfPartners[]>{
    return this.httpService.get('fines-partner')
    .pipe(
      map(
        (res: Paginator) => res.data as FinesOfPartners[]
      )
    );
  }
}
