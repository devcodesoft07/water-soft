import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/compat/firestore';
import { IncomeCatergory } from '@core/models/admin/incomes/income.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IncomesCategoriesFireService {
  collectionName = 'income-category';
  constructor(
    private firestoreService: AngularFirestore
  ) { }

  getAll(): Observable<IncomeCatergory[]> {
    return this.firestoreService.collection<IncomeCatergory>(this.collectionName)
    // return this.firestoreService.collection<IncomeCatergory>(this.collectionName, ref => ref.orderBy('register_date', 'asc'))
    .valueChanges({ idField: 'id' });
  }

  getData(id: string): Observable<(IncomeCatergory & { id: string }) | undefined> {
    return this.firestoreService.collection<IncomeCatergory>(this.collectionName).doc(id).valueChanges({ idField: 'id' });
  }

  add(data: IncomeCatergory): Promise<DocumentReference<IncomeCatergory>> {
    return this.firestoreService.collection<IncomeCatergory>(this.collectionName).add(data);
  }

  update(data: IncomeCatergory | any): Promise<void> {
    return this.firestoreService.collection<IncomeCatergory>(this.collectionName).doc(data.id).update(data);
  }

  delete(data: IncomeCatergory | any): Promise<void> {
    return this.firestoreService.collection<IncomeCatergory>(this.collectionName).doc(data.id).delete();
  }
}
