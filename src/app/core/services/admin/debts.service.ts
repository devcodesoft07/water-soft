import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/compat/firestore';
import { Debt } from '@core/models/detbs.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DebtsService {
  collectionName = 'debts';
  constructor(
    private firestoreService: AngularFirestore
  ) { }

  getAll(): Observable<Debt[]> {
    return this.firestoreService.collection<Debt>(this.collectionName)
    // return this.firestoreService.collection<Debts>(this.collectionName, ref => ref.orderBy('register_date', 'asc'))
    .valueChanges({ idField: 'id' });
  }

  getData(id: string): Observable<(Debt & { id: string }) | undefined> {
    return this.firestoreService.collection<Debt>(this.collectionName).doc(id).valueChanges({ idField: 'id' });
  }

  add(data: Debt): Promise<DocumentReference<Debt>> {
    return this.firestoreService.collection<Debt>(this.collectionName).add(data);
  }

  update(data: Debt | any): Promise<void> {
    return this.firestoreService.collection<Debt>(this.collectionName).doc(data.id).update(data);
  }

  delete(data: Debt | any): Promise<void> {
    return this.firestoreService.collection<Debt>(this.collectionName).doc(data.id).delete();
  }
}
