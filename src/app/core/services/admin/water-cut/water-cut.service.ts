import { Injectable } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/compat/firestore';
import { PartnerAction } from '@core/models/admin/partners/partner-action.model';
import { Partner } from '@core/models/admin/partners/partner.model';
import { WaterCut } from '@core/models/admin/water-cut/WaterCut';
import { WaterMeter } from '@core/models/admin/water-meter.model';

@Injectable({
  providedIn: 'root'
})
export class WaterCutService {
  private _collectionName: string;
  private _subCollectionName: string;
  private _historyWaterCutCollectionName: string
  private _waterMeterCollectionName: string;

  constructor(
    private firestoreService: AngularFirestore
  ) {
    this._collectionName = 'partner';
    this._subCollectionName = 'action';
    this._historyWaterCutCollectionName = 'history-water-cut';
    this._waterMeterCollectionName = 'water-meter';
  }

  addWaterCut(partnerAction: PartnerAction, data: WaterCut): Promise<DocumentReference<WaterCut>> {
    return this.firestoreService
      .collection<Partner>('partner')
      .doc(partnerAction.partner_id)
      .collection<PartnerAction>('action')
      .doc(partnerAction.id)
      .collection<WaterCut>('history-water-cut')
      .add(data);
  }

  public add(partnerAction: PartnerAction, data: WaterCut): Promise<void> {

    const batch = this.firestoreService.firestore.batch();

    const partnerReference = this.firestoreService.collection<Partner>(this._collectionName).doc(partnerAction.partner_id).ref;

    const waterMeterReference = this.firestoreService.collection<WaterMeter>(this._waterMeterCollectionName).doc(partnerAction.water_meter.measurer.id).ref;

    const actionReference = this.firestoreService.collection<Partner>(this._collectionName).doc(partnerAction.partner_id).collection<PartnerAction>(this._subCollectionName).doc(partnerAction.id).ref;

    const collectionRef = this.firestoreService.collection<Partner>(this._collectionName).doc(partnerAction.partner_id).collection<PartnerAction>(this._subCollectionName).doc(partnerAction.id).collection<WaterCut>(this._historyWaterCutCollectionName).ref;

    const newDocumentRef = collectionRef.doc();

    batch.set(newDocumentRef, data);

    const actionUpdate: PartnerAction = {
      water_meter: {
        measurer: {
          active: false
        }
      }
    } as PartnerAction;

    const waterMeterUpdate: WaterMeter = {
      active: false,
      partner: {
        active: false
      },
      water_cut: data.cut_non_payment,
      meter_broken: data.meter_broken,
      observation: data.reason
    } as WaterMeter;

    const partnerUpdate: Partner = {
      active: false,
      waterCut: {
        ...data,
        id: newDocumentRef.id
      }
    } as Partner;

    batch.set(partnerReference, partnerUpdate, { merge: true });
    batch.set(actionReference, actionUpdate, { merge: true });
    batch.set(waterMeterReference, waterMeterUpdate, { merge: true });
    return batch.commit();
  }
}