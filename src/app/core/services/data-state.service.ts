import { Injectable } from '@angular/core';
import { AuthConstants } from '@core/config/auth-constanst';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataStateService {

  public dataObserver$: Observable<any>;
  private data: any;
  private objectDataBehaviour: BehaviorSubject<any>;

  constructor() {
    this.data = {};
    this.objectDataBehaviour = new BehaviorSubject<any>(this.data);
    this.dataObserver$ = this.objectDataBehaviour.asObservable();
    this.initialize();
  }

  public setData(dataToSave: any): void {
    this.data = dataToSave;
    localStorage.setItem(AuthConstants.DATA_STATE, JSON.stringify(this.data));
    this.objectDataBehaviour.next(this.data);
  }

  public resetData(): void {
    this.data = {};
    this.objectDataBehaviour.next(this.data);
  }

  private initialize(): void {
    const data: string | null = localStorage.getItem(AuthConstants.DATA_STATE);
    if (data) {
      this.data = JSON.parse(data);
      this.objectDataBehaviour.next(this.data);
    }
  }
}
