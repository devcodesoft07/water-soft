/* eslint-disable @typescript-eslint/member-ordering */
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReactiveService {

  private objectSource = new BehaviorSubject<any>({});
  private listSource = new BehaviorSubject<any[]>([]);

  $getObjectSource: any = this.objectSource.asObservable();
  $getListSource: any = this.listSource.asObservable();

  constructor() { }

  sendObjectSource(data: any) {
    this.objectSource.next(data);
  }

  sendListSource(list: any[]) {
    this.listSource.next(list);
  }

}
