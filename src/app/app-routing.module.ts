import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@shared/guard/auth/auth.guard';
import { PathNotFoundComponent } from './path-not-found/path-not-found.component';
import { ERole } from '@core/models/admin/user.model';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'admin',
    canActivate: [AuthGuard],
    data: { roles: [ERole.ADMIN, ERole.READER] },
    loadChildren: () => import('./module/admin/admin.module').then(m => m.AdminPageModule)
  },
  {
    path: 'reader',
    canActivate: [AuthGuard],
    data: { roles: [ERole.ADMIN, ERole.READER] },
    loadChildren: () => import('./module/reader/reader.module').then(m => m.ReaderPageModule)
  },
  { path: '**', component: PathNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
