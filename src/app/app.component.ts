import { Component, LOCALE_ID } from '@angular/core';
import { Router } from '@angular/router';
import localeEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
registerLocaleData(localeEs, 'es');


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'es'
    }
  ]
})
export class AppComponent {
  // res: string = this.router.url.split('/').join(' ');
  constructor(
    private router: Router
  ) {}
  openModal(){}
}
