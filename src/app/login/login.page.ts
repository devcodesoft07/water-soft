/* eslint-disable no-underscore-dangle */
/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/no-inferrable-types */
import { LoginService } from './../core/services/login/login.service';
import {
  Component,
  OnInit
} from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators
} from '@angular/forms';
import { AlertController } from '@ionic/angular';
import { StorageService } from '@core/services/storage/storage.service';
import { AuthFireService } from '@core/services/login/login-fire.service';
import Swal from 'sweetalert2';
import { CookieService } from 'ngx-cookie-service';
import { ERole, IAuthUser, IUser } from '@core/models/admin/user.model';
import firebase from 'firebase/compat/app';
import { FirebaseError } from 'firebase/app';
import { AuthConstants } from '@core/config/auth-constanst';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FirebaseErrorEnum } from '@core/enum/firebase-error.enum';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: UntypedFormGroup;
  messageError: any;
  // eslint-disable-next-line @typescript-eslint/ban-types
  remember: Boolean = false;
  showPassword = false;
  passwordToggleIcon = 'eye-off';
  email: string;
  password: string;
  loginStatus: boolean = false;
  private _subscribers: Subscription;


  constructor(
    private formBuilder: UntypedFormBuilder,
    private loginService: LoginService,
    private alertController: AlertController,
    private storageService: StorageService,
    private authFireService: AuthFireService,
    private cookieService: CookieService,
    private router: Router
  ) {
    this._subscribers = new Subscription();
    this.buildForm();
  }

  ngOnInit() {
  }

  togglePassword(): void {
    this.showPassword = !this.showPassword;
    if (this.passwordToggleIcon === 'eye-off') {
      this.passwordToggleIcon = 'eye';
    } else {
      this.passwordToggleIcon = 'eye-off';
    }
  }

  get emailUser(): any {
    return this.loginForm.get('email');
  }

  get passwordUser(): any {
    return this.loginForm.get('password');
  }

  loginFire(): void {
    this._verifyIsRemember();
    const data: IAuthUser = this.loginForm.value;
    Swal.fire({
      title: 'Iniciando Sesión...!',
      html: 'Espere un momento, los datos se estan validando',
      allowEscapeKey: false,
      allowOutsideClick: false,
      backdrop: false,
      didOpen: () => {
        Swal.showLoading();
        this.authFireService.loginFire(data)
          .then(
            (res: firebase.auth.UserCredential) => {
              this.cookieService.set(AuthConstants.AUTH, JSON.stringify(res.user));
              this._getUserById(res);
            },
            (error: FirebaseError) => {
              const message: string = this._generateErroMessage(error.code);
              Swal.close();
              Swal.fire({
                icon: 'error',
                title: 'Error administración...',
                text: message,
                timer: 2000,
              });
            }
          );
      }
    });
  }

  private _verifyIsRemember(): void {
    if (this.remember) {
      this.storageService.store('email', this.loginForm.value.email);
      this.storageService.store('password', this.loginForm.value.password);
    }
  }

  private _getUserById(res: firebase.auth.UserCredential): void {
    const user$: Subscription = this.authFireService.getUserByUid(res.user?.uid)
      .pipe(
        take(1)
      )
      .subscribe((user: IUser | undefined) => {
        this.cookieService.set(AuthConstants.USER, JSON.stringify(user));
        if (user.role === ERole.READER) {
          this.router.navigate(['admin/lecturador']);
        } else {
          this.router.navigate(['admin']);
        }
        Swal.close();
      }
      );
    this._subscribers.add(user$);
  }

  private buildForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', { validators: [Validators.required, Validators.minLength(2), Validators.maxLength(50), Validators.email] }],
      password: ['', {
        validators: [Validators.required, Validators.minLength(8)]
      }]
    });
  }

  private _generateErroMessage(code: string): string {
    if (code === FirebaseErrorEnum.WRONG_PASSWORD || code === FirebaseErrorEnum.INVALID_EMAIL || code === FirebaseErrorEnum.INVALID_CREDENTIAL || code === FirebaseErrorEnum.USER_NOT_FOUND) {
      return 'Correo electronico / contraseña incorrectas';
    }
    return 'Si el error persiste contactese con administración...';
  }
}
