import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SharedModule } from './shared/shared.module';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

import { PathNotFoundComponent } from './path-not-found/path-not-found.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InterceptorToken } from '@core/token/token.interceptor';
// import { InterceptorMessage } from '@core/services/message/message.interceptor';
import { RegisterPartnersModule } from './module/admin/page/partners/register-partners/register-partners.module';

import { AngularFireModule } from '@angular/fire/compat';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { AngularFireFunctionsModule, USE_EMULATOR } from '@angular/fire/compat/functions';
import { environment } from '../environments/environment';
import { CookieService } from 'ngx-cookie-service';
@NgModule({
    declarations: [
        AppComponent,
        PathNotFoundComponent
    ],
    imports: [
        BrowserModule,
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFirestoreModule,
        AngularFireFunctionsModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        HttpClientModule,
        SharedModule,
        SweetAlert2Module.forRoot({
            provideSwal: () => import('sweetalert2').then(({ default: swal }) => swal.mixin({
                // example: set global options here
                cancelButtonText: 'Cancelar',
                customClass: {
                    confirmButton: 'sweet-btn confirm-btn__container',
                    cancelButton: 'sweet-btn cancel-btn__container'
                }
            }))
        }),
        BrowserAnimationsModule,
        RegisterPartnersModule,
    ],
    providers: [
        { provide: RouteReuseStrategy,
            useClass: IonicRouteStrategy,
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: InterceptorToken,
            multi: true
        },
        { provide: LOCALE_ID, useValue: 'es' },
        // { provide: FUNC, useValue: ['localhost', 5001] },
        // { provide: USE_ST, useValue: ['localhost', 5003] },
        CookieService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
