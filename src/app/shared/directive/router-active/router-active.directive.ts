/* eslint-disable arrow-body-style */
import { Directive, Input, OnDestroy, OnInit } from '@angular/core';
import { IonItem } from '@ionic/angular';
import { NavigationEnd, Router, RouterLink } from '@angular/router';
import { filter, map, tap } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[appRouterActive]'
})
export class RouterActiveDirective implements OnInit, OnDestroy {
  private handler: Subscription;

  constructor(
    private router: Router,
    private ionItem: IonItem,
    private rl: RouterLink
  ) {
  }

  ngOnInit(): void {

    this.handler = this.router.events.pipe(
      filter(x => {
        return x instanceof NavigationEnd;
      }),
      map(() => {
        return this.rl && this.router.isActive(this.rl.urlTree, false);
      }),
      tap(isActive => {
        this.ionItem.color = isActive ? 'primary' : 'dark';
      })
    ).subscribe();
  }

  ngOnDestroy() {
    this.handler.unsubscribe();
  }

}
