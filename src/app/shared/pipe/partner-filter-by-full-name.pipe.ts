import { Pipe, PipeTransform } from '@angular/core';
import { Partner } from '@core/models/admin/partners/partner.model';

@Pipe({
  name: 'partnerFilterByFullName'
})
export class PartnerFilterByFullNamePipe implements PipeTransform {

  transform(partnerList: Partner[], textToFilter: string): Partner[] {
    if (textToFilter === '') {
      return partnerList;
    }

    return partnerList.filter((partner: Partner) => (partner.last_name.concat(' ').concat(partner.name).toLowerCase()).includes(textToFilter.toLowerCase()));
  }
}
