import { Pipe, PipeTransform } from "@angular/core";
import { PartnerReader } from "@core/models/admin/partners/partner.model";

@Pipe({
  name: 'filterPartnerByNameOrWaterMeter'
})
export class FilterPartnerByNameOrWaterMeterPipe implements PipeTransform {

  transform(partnerList: PartnerReader[], textToFilter: string): PartnerReader[] {

    if (textToFilter === '') {
      return partnerList;
    }

    const partnersFilter: PartnerReader[] = partnerList.filter((partner: PartnerReader) => {
      return (
        (partner?.partner?.waterMeter?.water_meter_number && partner?.partner?.waterMeter?.water_meter_number.toLocaleLowerCase().includes(textToFilter.toLowerCase())) ||
        partner?.partner.last_name.concat(' ').concat(partner?.partner.name).toLowerCase().includes(textToFilter.toLocaleLowerCase())
      )
    })

    return partnersFilter;
  }
}
