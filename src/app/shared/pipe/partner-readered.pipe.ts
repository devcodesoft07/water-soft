import { Pipe, PipeTransform } from '@angular/core';
import { PartnerReader } from '@core/models/admin/partners/partner.model';

@Pipe({
  name: 'partnerReadered'
})
export class PartnerReaderedPipe implements PipeTransform {

  transform(partners: PartnerReader[], isPay: boolean = true): number {
    return partners?.filter((data: PartnerReader) => {

      if (isPay) {
        return data.water_consumed.length > 0;
      }
      return data.water_consumed.length === 0;
    }).length;
  }

}
