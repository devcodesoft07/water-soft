/* eslint-disable arrow-body-style */
import { Pipe, PipeTransform } from '@angular/core';
import { WaterMeter } from '@core/models/admin/water-meter.model';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(waterMeterList: WaterMeter[], meterNumber: string, filters: string[]): WaterMeter[] {
    meterNumber = meterNumber.toLowerCase();
    if (meterNumber === '') {
      return waterMeterList;
    } else {
      return waterMeterList.filter(measurer => {
        return measurer.water_meter_number.toLowerCase().includes(
          meterNumber
        );
      });
    }
  }
}
