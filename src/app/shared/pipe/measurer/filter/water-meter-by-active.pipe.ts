import { Pipe, PipeTransform } from '@angular/core';
import { WaterMeter } from '@core/models/admin/water-meter.model';

@Pipe({
  name: 'waterMeterByActive'
})
export class WaterMeterByTypePipe implements PipeTransform {

  transform(waterMeterList: WaterMeter[], active: boolean = false): WaterMeter[] {
    return waterMeterList?.filter((waterMeter: WaterMeter) => waterMeter.active === active && !waterMeter.partner && !waterMeter.used);
  }

}
