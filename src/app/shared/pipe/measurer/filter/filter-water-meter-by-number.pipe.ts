import { Pipe, PipeTransform } from "@angular/core";
import { WaterMeter } from "@core/models/admin/water-meter.model";

@Pipe({
  name: 'filterWaterMeterByNumber'
})
export class FilterWaterMeterByNumberPipe implements PipeTransform {

  transform(waterMeterList: WaterMeter[], textToFilter: string): WaterMeter[] {
    const textToFilterToLowercase: string = textToFilter.toLowerCase();

    if (textToFilter !== '') {
      return waterMeterList.filter((waterMeter: WaterMeter) => waterMeter.water_meter_number.toLowerCase().includes(textToFilterToLowercase));
    }

    return waterMeterList;
  }
}
