import { Pipe, PipeTransform } from '@angular/core';
import { CommonDataCharts } from '@core/models/admin/charts/common-data-charts';
import { ExpenseCategory } from '@core/models/admin/expenses/expense-category.model';
import { Expense } from '@core/models/admin/expenses/expense.model';

@Pipe({
  name: 'buildExpensesToPieChart'
})
export class BuildExpensesToPieChartPipe implements PipeTransform {

  transform(expenses: Expense[], expenseCategories: ExpenseCategory[]): CommonDataCharts[] {

    const expensesForCategories: CommonDataCharts[] = expenseCategories.map((expenseCategory: ExpenseCategory) => {
      const newValue: CommonDataCharts = {
        name: expenseCategory.description.toLocaleUpperCase(),
        value: `${this._getExpensesAmountByCategories(expenses, expenseCategory)}`
      }
      return newValue;
    });

    return expensesForCategories;
  }

  private _getExpensesAmountByCategories(expenses: Expense[], expenseCategory: ExpenseCategory): number {
    let totalAmountToExpensesByCategory: number = 0;
    const expensesByCategory: Expense[] | undefined = expenses?.filter((expense: Expense) => expense.expense_category.description === expenseCategory.description);
    if (expensesByCategory !== undefined) {
      const totalToExpenses: number = expensesByCategory.reduce((total, currentValue: Expense) => total + (Number(currentValue.amount) || 0), 0);
      totalAmountToExpensesByCategory += totalToExpenses;
    }
    return totalAmountToExpensesByCategory;
  }
}
