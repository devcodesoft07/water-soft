import { Pipe, PipeTransform } from '@angular/core';
import { CommonDataCharts } from '@core/models/admin/charts/common-data-charts';
import { Expense } from '@core/models/admin/expenses/expense.model';
import moment from 'moment';

@Pipe({
  name: 'buildExpensesToShowChart'
})
export class BuildExpensesToShowChartPipe implements PipeTransform {

  transform(value: Expense[]): CommonDataCharts[] {
    return this._getAllMonths().map((month: string) => {
      const newData: CommonDataCharts = {
        name: month,
        value: `${this._getExpensesAmountByMonth(value, month)}`
      }
      return newData;
    })
  }

  private _getAllMonths(): string[] {
    const months = [];

    for (let month = 0; month < 12; month++) {
      const fecha = new Date(2023, month, 1);
      const monthName = fecha.toLocaleString('es-ES', { month: 'long' });
      months.push(monthName);
    }

    return months;
  }

  private _getExpensesAmountByMonth(expenses: Expense[], month: string): number {
    let totalAmountExpenses: number = 0;

    const expensesByMonthFilter: Expense[] | undefined = expenses.filter((expense: Expense) => moment(expense.date).format('MMMM') === month);

    if (expensesByMonthFilter !== undefined) {
      const amountExpenses: number = expensesByMonthFilter.reduce((total, currentValue: Expense) => total + (Number(currentValue.amount) || 0), 0);

      totalAmountExpenses += Number(amountExpenses);
    }

    return totalAmountExpenses;
  }
}
