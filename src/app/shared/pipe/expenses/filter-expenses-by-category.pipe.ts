import { Pipe, PipeTransform } from "@angular/core";
import { ExpenseCategory } from "@core/models/admin/expenses/expense-category.model";
import { Expense } from "@core/models/admin/expenses/expense.model";

@Pipe({
  name: 'filterExpensesByCategory'
})
export class FilterExpensesByCategoryPipe implements PipeTransform {
  transform(expenses: Expense[], category: ExpenseCategory | null): Expense[] {

    if (category !== null) {
      const expensesByCategory: Expense[] = expenses.filter((expense: Expense) => expense.expense_category.id === category.id);

      return expensesByCategory;
    }

    return expenses;
  }
}