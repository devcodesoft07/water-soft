import { Pipe, PipeTransform } from "@angular/core";
import { ConstanstEnum } from "@core/enum/constants.enum";
import { Expense } from "@core/models/admin/expenses/expense.model";
import { TotalAmount } from "@core/models/total-amount.model";

@Pipe({
  name: 'calculateTotalAmountExpenses'
})
export class CalculateTotalAmountExpensesPipe implements PipeTransform {

  transform(expenses: Expense[]): TotalAmount {
    const expensesDollarCoin: Expense[] = expenses.filter((expense: Expense) => expense.coin === ConstanstEnum.DOLLAR_COIN);
    const expensesBolCoin: Expense[] = expenses.filter((expense: Expense) => expense.coin === ConstanstEnum.BS_COIN);

    const totalAmountBolCoin = expensesBolCoin.reduce((currentExpense, nextExpense) => Number(currentExpense) + Number(nextExpense.amount), 0);
    const totalAmountDollarCoin = expensesDollarCoin.reduce((currentExpense, nextExpense) => Number(currentExpense) + Number(nextExpense.amount), 0);

    const totalAmountToExpenses: TotalAmount = {
      totalAmountBolCoin: totalAmountBolCoin,
      totalAmountDollarCoin: totalAmountDollarCoin
    }

    return totalAmountToExpenses;
  }
}