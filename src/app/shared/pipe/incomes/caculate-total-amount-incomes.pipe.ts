import { Pipe, PipeTransform } from "@angular/core";
import { ConstanstEnum } from "@core/enum/constants.enum";
import { Income } from "@core/models/admin/incomes/income.model";
import { TotalAmount } from "@core/models/total-amount.model";

@Pipe({
  name: 'calculateTotalAmountIncomes'
})
export class CalculateTotalAmountIncomesPipe implements PipeTransform {

  transform(incomes: Income[]): TotalAmount {

    const incomesDollarCoin: Income[] = incomes.filter((income: Income) => income.coin === ConstanstEnum.DOLLAR_COIN);
    const incomesBolCoin: Income[] = incomes.filter((income: Income) => income.coin === ConstanstEnum.BS_COIN);

    const totalAmountBolCoin = incomesBolCoin.reduce((currentIncome, nextIncome) => Number(currentIncome) + Number(nextIncome.amount), 0);
    const totalAmountDollarCoin = incomesDollarCoin.reduce((currentIncome, nextIncome) => Number(currentIncome) + Number(nextIncome.amount), 0);

    const totalAmountToIncomes: TotalAmount = {
      totalAmountBolCoin: totalAmountBolCoin,
      totalAmountDollarCoin: totalAmountDollarCoin
    }

    return totalAmountToIncomes;
  }
}