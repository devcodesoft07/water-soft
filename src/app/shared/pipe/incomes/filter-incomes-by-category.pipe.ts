import { Pipe, PipeTransform } from "@angular/core";
import { Income, IncomeCatergory } from "@core/models/admin/incomes/income.model";

@Pipe({
  name: 'filterIncomesByCategory'
})
export class FilterIncomesByCategoryPipe implements PipeTransform {
  transform(incomes: Income[], category: IncomeCatergory | null): Income[] {

    if (category !== null) {
      const incomesByCategory: Income[] = incomes.filter((income: Income) => income.category.type === category.type);

      return incomesByCategory;
    }

    return incomes;
  }
}