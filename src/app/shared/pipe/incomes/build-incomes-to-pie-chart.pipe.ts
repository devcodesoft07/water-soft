import { Pipe, PipeTransform } from '@angular/core';
import { CommonDataCharts } from '@core/models/admin/charts/common-data-charts';
import { Income, IncomeCatergory } from '@core/models/admin/incomes/income.model';

@Pipe({
  name: 'buildIncomesToPieChart'
})
export class BuildIncomesToPieChartPipe implements PipeTransform {

  transform(incomes: Income[], incomeCategories: IncomeCatergory[]): CommonDataCharts[] {

    const incomesForCategories: CommonDataCharts[] = incomeCategories.map((incomeCategory: IncomeCatergory) => {
      const newValue: CommonDataCharts = {
        name: incomeCategory.name,
        value: `${this._getIncomesAmountByCategories(incomes, incomeCategory)}`
      }
      return newValue;
    });

    return incomesForCategories;
  }

  private _getIncomesAmountByCategories(incomes: Income[], incomeCategory: IncomeCatergory): number {
    const incomesByCategory: Income[] | undefined = incomes.filter((income: Income) => income.category.type === incomeCategory.type);
    if (incomeCategory !== undefined) {
      const totalToIncomes: number = incomesByCategory.reduce((total, currentValue: Income) => total + (Number(currentValue.amount) || 0), 0);
      return totalToIncomes;
    } else {
      return 1;
    }
  }
}
