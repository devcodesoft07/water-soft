import { Pipe, PipeTransform } from "@angular/core";
import { ConstanstEnum } from "@core/enum/constants.enum";
import { Income, IncomesResponseFunctions } from "@core/models/admin/incomes/income.model";
import { TotalAmount } from "@core/models/total-amount.model";

@Pipe({
  name: 'calculateTotalAmountToChartIncomes'
})
export class CalculateTotalAmountToChartIncomesPipe implements PipeTransform {

  transform(incomes: IncomesResponseFunctions): TotalAmount {

    const totalWaterConsume: number = incomes.totalIncomesByWaterConsume.reduce((currentWaterConsume, nextWaterConsume) => currentWaterConsume + (Number(nextWaterConsume.amount) + Number(nextWaterConsume.sewerage) + Number(nextWaterConsume.debts)), 0);
    const totalIncomeDollarCoin: Income[] = incomes.incomesData.filter((incomeData: Income) => incomeData.coin === ConstanstEnum.DOLLAR_COIN);
    const totalIncomesBolCoin: Income[] = incomes.incomesData.filter((incomeData: Income) => incomeData.coin === ConstanstEnum.BS_COIN);

    const totalIncomeToDollarCoin = totalIncomeDollarCoin.reduce((currentIncome, nextIncome) => Number(currentIncome) + Number(nextIncome.amount), 0);
    const totalAmountIncomes = totalIncomesBolCoin.reduce((currentIncome, nextIncome) => Number(currentIncome) + Number(nextIncome.amount), 0) + totalWaterConsume;

    const totalAmountToExpenses: TotalAmount = {
      totalAmountBolCoin: totalAmountIncomes,
      totalAmountDollarCoin: totalIncomeToDollarCoin
    }

    return totalAmountToExpenses;
  }
}