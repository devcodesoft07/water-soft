import { Pipe, PipeTransform } from '@angular/core';
import { CommonDataCharts } from '@core/models/admin/charts/common-data-charts';
import { Income, IncomesResponseFunctions } from '@core/models/admin/incomes/income.model';
import { PayByPartner } from '@core/models/admin/partners/partner.model';
import moment from 'moment';

@Pipe({
  name: 'buildIncomesToShowChart'
})
export class BuildIncomesToShowChartPipe implements PipeTransform {

  transform(value: IncomesResponseFunctions): CommonDataCharts[] {

    const incomesAmountByMonth: CommonDataCharts[] = this._getAllMonths().map((month: string) => {
      const newData: CommonDataCharts = {
        name: month,
        value: `${this._getWaterConsumeDataToPartnerByMonth(value.incomesData, month, value.totalIncomesByWaterConsume)}`
      }

      return newData;
    })

    return incomesAmountByMonth;
  }

  private _getAllMonths(): string[] {
    const months = [];

    for (let month = 0; month < 12; month++) {
      const fecha = new Date(2023, month, 1);
      const monthName = fecha.toLocaleString('es-ES', { month: 'long' });
      months.push(monthName);
    }

    return months;
  }

  private _getWaterConsumeDataToPartnerByMonth(incomes: Income[], month: string, payByPartner: PayByPartner[]): number {

    const incomeByMonthFilter: Income[] | undefined = incomes?.filter((income: Income) => moment(income.date).format('MMMM') === month && income.coin === 'Bs');
    const paymentByPartnerByMonth: PayByPartner[] | undefined = payByPartner?.filter((payByPartner: PayByPartner) => moment(payByPartner.dateToPay).format('MMMM') === month);

    const amountIncomes: number = incomeByMonthFilter?.reduce((total, currentValue: Income) => total + (Number(currentValue.amount) || 0), 0);
    const amountWaterConsume: number = paymentByPartnerByMonth?.reduce((total, currentValue: PayByPartner) => total + (Number(currentValue.amount) + Number(currentValue.sewerage) + Number(currentValue.debts) || 0), 0);

    return amountIncomes + amountWaterConsume;
  }
}
