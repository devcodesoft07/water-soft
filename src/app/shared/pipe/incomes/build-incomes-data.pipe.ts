import { Pipe, PipeTransform } from '@angular/core';
import { Income, IncomesResponseFunctions } from '@core/models/admin/incomes/income.model';
import { PayByPartner } from '@core/models/admin/partners/partner.model';

@Pipe({
  name: 'buildIncomesDataByMonthAndYear'
})
export class BuildIncomesDataPipe implements PipeTransform {

  transform(value: IncomesResponseFunctions): Income[] {

    const amountWaterConsume: number = value.totalIncomesByWaterConsume.reduce((total, currentValue: PayByPartner) => total + (Number(currentValue.amount) + Number(currentValue.debts) + Number(currentValue.sewerage)), 0);
    const incomeWaterConsume: Income = {
      amount: amountWaterConsume,
      category: {
        description: 'Cobro de consumo de agua',
        id: '',
        image: '',
        name: 'Cobro consumo de Agua',
        type: 'WATER_CONSUME'
      },
      coin: 'Bs',
      detail: 'Cobro consumo de Agua',
      debt: [],
    } as Income;

    const allIncomes: Income[] = [...value.incomesData, incomeWaterConsume];

    return allIncomes;
  }

}
