import { Pipe, PipeTransform } from '@angular/core';
import { PayByPartner } from '@core/models/admin/partners/partner.model';
import moment from 'moment';

@Pipe({
  name: 'generateDebtsWaterConsumePay'
})
export class GenerateDebtsWaterConsumePayPipe implements PipeTransform {

  transform(waterConsumedByPartner: PayByPartner[], priceDebts: number): PayByPartner[] {
    const getWaterConsumeNotPayment: PayByPartner[] = waterConsumedByPartner.map((waterConsumedByPartner: PayByPartner) => {

      if (!waterConsumedByPartner.isPay && this.isDateToReadBeforeToday(waterConsumedByPartner.dateToRead as string)) {
        const newWaterConsume: PayByPartner = {
          ...waterConsumedByPartner,
          debts: Number(priceDebts ? priceDebts : 0)
        };
        return newWaterConsume;
      }

      return waterConsumedByPartner;
    });

    return getWaterConsumeNotPayment;
  }

  private isDateToReadBeforeToday(fecha1: string) {
    const momentFecha1 = moment(fecha1);
    const momentFecha2 = moment(new Date());

    if (momentFecha1.isBefore(momentFecha2, 'year')) {
      return true;
    } else if (momentFecha1.isAfter(momentFecha2, 'year')) {
      return false;
    }

    return momentFecha1.isBefore(momentFecha2, 'month');
  }
}
