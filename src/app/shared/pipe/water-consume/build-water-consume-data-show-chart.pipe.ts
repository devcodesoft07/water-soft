import { Pipe, PipeTransform } from '@angular/core';
import { PayByPartner } from '@core/models/admin/partners/partner.model';
import { CommonDataCharts } from '@core/models/admin/charts/common-data-charts';
import { DataLineChart } from '@core/models/admin/charts/data-line-chart';
import moment from 'moment';

@Pipe({
  name: 'buildWaterConsumeDataShowChart'
})
export class BuildWaterConsumeDataShowChartPipe implements PipeTransform {

  transform(dataToWaterConsume: PayByPartner[], title: string): DataLineChart[] {
    const waterConsumeDataToShowLineChart: DataLineChart = {
      name: title,
      series: this._getAllMonths().map((month: string) => {
        const newValue: CommonDataCharts = {
          name: month,
          value: this._getWaterConsumeDataToPartnerByMonth(dataToWaterConsume, month)
        }
        return newValue;
      })
    }
    return [waterConsumeDataToShowLineChart];
  }

  private _getAllMonths(): string[] {
    const months = [];

    for (let month = 0; month < 12; month++) {
      const fecha = new Date(2023, month, 1);
      const monthName = fecha.toLocaleString('es-ES', { month: 'long' });
      months.push(monthName);
    }

    return months;
  }

  private _getWaterConsumeDataToPartnerByMonth(dataToWaterConsume: PayByPartner[], month: string): string {
    const payByMonthFilterIndex: number = dataToWaterConsume.findIndex((payByPartner: PayByPartner) => moment(payByPartner.dateToRead).format('MMMM') === month);

    if (payByMonthFilterIndex !== -1) {
      return String(dataToWaterConsume[payByMonthFilterIndex].water_consume);
    }
    return '0';
  }
}