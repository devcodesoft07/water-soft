import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { AuthConstants } from '@core/config/auth-constanst';
import { IUser } from '@core/models/admin/user.model';
import { AuthFireService } from '@core/services/login/login-fire.service';
import { StorageService } from '@core/services/storage/storage.service';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  token: string;

  constructor(
    private localStorageService: StorageService,
    private router: Router,
    private _authFireService: AuthFireService,
    private _cookieService: CookieService
  ) {
  }

  // canActivate(
  //   route: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

  //     const token: string = localStorage.getItem('authToken');
  //     if (token) {
  //       return true;
  //     } else {
  //       this.router.navigate(['']);
  //       return false;
  //     }
  //   }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this._authFireService.isAuthenticated().pipe(map(authenticated => {
      if (!authenticated) {
        this.router.navigate(['/login']);
        return false;
      }
      const requiredRoles = next.data.roles as string[] || [];
      const userData: IUser = JSON.parse(this._cookieService.get(AuthConstants.user));
      const hasRequiredRole = requiredRoles.includes(userData.role);
      if (!hasRequiredRole) {
        return false;
      }
      return true;
    }));
  }
}
