import { Component, OnInit, LOCALE_ID } from '@angular/core';


import localeEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
import { MenuController } from '@ionic/angular';
registerLocaleData(localeEs, 'es');
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'es'
    }
  ]
})
export class NavbarComponent implements OnInit {

  date: Date = new Date();
  constructor(
    private menuController: MenuController
  ) {
    this.buildTimer();
  }

  ngOnInit() {}


  buildTimer(): void {
    setInterval (() => {
      this.date = new Date();
    }, 1000);
  }
  closeMenu(): void {
    this.menuController.close('main-menu');
  }
}
