/* eslint-disable @typescript-eslint/no-inferrable-types */
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-empty-data',
  templateUrl: './empty-data.component.html',
  styleUrls: ['./empty-data.component.scss'],
})
export class EmptyDataComponent {
  @Input() btnTitle: string = 'Agregar Nuevo';
  @Input() showButton: boolean;
  @Input() showTitle: boolean;
  @Output() btnEventEmitter: EventEmitter<boolean> = new EventEmitter();
  constructor() {
    this.showButton = true;
    this.showTitle = true;
  }

  actionBtn(): void {
    this.btnEventEmitter.emit(true);
  }
}
