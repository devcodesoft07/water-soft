import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-empty-search',
  templateUrl: './empty-search.component.html',
  styleUrls: ['./empty-search.component.scss'],
})
export class EmptySearchComponent implements OnInit {
  @Input() title: string;
  @Input() subtitle: string;
  @Input() showTitle: boolean;
  @Input() showSubtitle: boolean;
  @Input() offsetMd: number;
  @Input() sizeMd: number;
  @Input() noMargin: boolean;

  constructor() {
    this.title = 'Datos no encontrados';
    this.subtitle = '¡Intente realizar otra nueva busqueda, filtro, o seleccione una nueva fecha.!';
    this.showSubtitle = true;
    this.showTitle = true;
    this.offsetMd = 3;
    this.sizeMd = 6;
    this.noMargin = false;
  }

  ngOnInit() { }

}
