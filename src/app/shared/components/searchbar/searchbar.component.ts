import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss'],
})
export class SearchbarComponent implements OnInit {
  @Input() dataSelected: string;
  @Output() dataEventEmitter: EventEmitter<any> = new EventEmitter();
  textToSearch = '';
  constructor() { }

  ngOnInit() {}

  dataSearch(event: any): void {
    const textSearch = event.detail.value;
    this.textToSearch = textSearch;
    if (this.textToSearch !== '') {
      this.dataEventEmitter.emit([
        {data: '123'},
        {data: '345'},
        {data: '567'},
      ]);
    } else {
      this.dataEventEmitter.emit(false);
    }
  }

}
