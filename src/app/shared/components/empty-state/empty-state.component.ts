import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-empty-state',
  templateUrl: './empty-state.component.html',
  styleUrls: ['./empty-state.component.scss'],
})
export class EmptyStateComponent implements OnInit {

  res: string = this.router.url;
  ruta: string = this.res.split('/').join(' ');
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    // console.log('ROUTER URL',this.res.split('/').join(' '));
  }

}
