import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
// Moment
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';
import 'moment/locale/es';
import * as _moment from 'moment';
import { default as _rollupMoment, Moment } from 'moment';
// --------
import { DateFilter } from '@core/models/date-filter.model';

const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY',
  },
  display: {
    dateInput: 'YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class DatePickerComponent {
  @Output() dateEventEmitter: EventEmitter<DateFilter> = new EventEmitter();
  @Output() resetEventEmitter: EventEmitter<DateFilter> = new EventEmitter();
  date: FormControl;
  public showAllInput: string | null;

  constructor() {
    this.date = new FormControl();
  }

  chosenYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = moment();
    ctrlValue.year(normalizedYear.year());
    const year: number = ctrlValue.year();
    const month: number = ctrlValue.month();
    this.date.setValue(ctrlValue);
    const dateFiler: DateFilter = {
      year,
      month: month + 1
    };
    this.dateEventEmitter.emit(dateFiler);
    datepicker.close();
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    const year: number = ctrlValue.year();
    const month: number = ctrlValue.month();
    this.date.setValue(ctrlValue);
    const dateFiler: DateFilter = {
      year,
      month: month + 1
    };
    this.dateEventEmitter.emit(dateFiler);
    datepicker.close();
  }

  reset(): void {
    this.date.setValue(null);
    // const ctrlValue = this.date.value;
    // const year: number = ctrlValue.year();
    // const month: number = ctrlValue.month();
    // const dateFiler: DateFilter = {
    //   year,
    //   month: month + 1
    // };
    this.resetEventEmitter.emit(null);
  }

}
