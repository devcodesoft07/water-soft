import { Component, Input, OnInit, ViewChild } from '@angular/core';
//models and Services
import { WaterMeter } from '@core/models/admin/water-meter.model';
import { ErrorsMessage } from '@core/models/errors.model';
import { WaterMeterService } from '@core/services/admin/water-meter/water-meter.service';
//controllers
import { ModalController, PopoverController } from '@ionic/angular';
//components
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { EditMeasurerComponent } from 'src/app/module/admin/page/water-mater/components/edit-measurer/edit-measurer.component';

import Swal, { SweetAlertOptions } from 'sweetalert2';
@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.scss'],
})
export class MenuListComponent implements OnInit {
  @Input() waterMeter: WaterMeter;
  @Input() option: string;
  @ViewChild('deleteSwal') public readonly deleteSwal!: SwalComponent;

  options: SweetAlertOptions = {
    title: '¡Eliminado correctamente!',
    text: 'Los datos del medidor se eliminaron correctamente.',
    icon: 'success',
    iconColor: '#0284C9',
    backdrop: false,
    timer: 2500,
    showCancelButton: false,
    showConfirmButton: false
  };
  constructor(
    private popoverController: PopoverController,
    private modalController: ModalController,
    private waterMeterService: WaterMeterService
  ) { }

  ngOnInit() {
  }

  deleteData(confirmSwal: SwalComponent): void {
    Swal.fire({
      title: '¡Eliminando...!',
      text: 'Espere un momento, los datos se estan eliminando',
      backdrop: false,
      didOpen: () => {
        Swal.showLoading();
      },
      willOpen: () => {
        this.waterMeterService.deleteWaterMeter(this.waterMeter).subscribe(
          (res: boolean) => {
            if (res) {
              Swal.close();
              confirmSwal.fire();
            }
          },
          (error: ErrorsMessage) => {
            Swal.close();
            console.log(error);
          }
        );
      },
    }).then((result) => {
      /* Read more about handling dismissals below */
      if (result.isDismissed) {
        // console.log('I was closed by the timer');
        this.popoverController.dismiss({
          reloadPage: true
        });
      }
    });
  }

  async editData() {
    const modal = await this.modalController.create({
      component: EditMeasurerComponent,
      mode: 'ios',
      componentProps: {
        data: this.waterMeter,
        opt: this.option
      }
    });
    await modal.present();


    await modal.onDidDismiss()
    .then(
      (res: any) => {
        // console.log(res);
        const reload = res.data.save? res.data.save : undefined;
        if (reload) {
          this.popoverController.dismiss({
            reloadPage: reload
          });
        }
      }
    )
    .catch(
      (error: any) => {
        console.log(error);
      }
    );

  }
}
