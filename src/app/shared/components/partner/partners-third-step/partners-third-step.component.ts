/* eslint-disable @typescript-eslint/naming-convention */
import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ConstanstEnum } from '@core/enum/constants.enum';
import { Partner } from '@core/models/admin/partners/partner.model';

import { QuotasPlan } from '@core/models/admin/partners/quotas-plan.model';
import { WaterMeter } from '@core/models/admin/water-meter.model';
import { WaterMeterFireService } from '@core/services/admin/water-meter/water-meter-fire.service';
import { ModalController } from '@ionic/angular';
import { FileUploadComponent } from '@shared/components/file-upload/file-upload.component';
import { Observable } from 'rxjs';
import moment from 'moment';
import { AddMeasurerComponent } from '@module/admin/page/water-mater/components/add-measurer/add-measurer.component';

@Component({
  selector: 'app-partners-third-step',
  templateUrl: './partners-third-step.component.html',
  styleUrls: ['./partners-third-step.component.scss']
})
export class PartnersThirdStepComponent implements OnInit {
  @Input() partner: Partner;
  @Input() actionPrice = 500;
  @Output() thirdDataEventEmitter: EventEmitter<any> = new EventEmitter();
  thirdForm: UntypedFormGroup;
  today: string = new Date().toISOString();
  isQuotas = false;
  // isWays = false;
  dataPlanOficial: QuotasPlan[];

  public waterMeterList$: Observable<WaterMeter[]>;

  constructor(
    private formBuilder: UntypedFormBuilder,
    private modalController: ModalController,
    private _waterMeterFireService: WaterMeterFireService
  ) {
    this.buildForm();
  }

  buildForm(): void {
    this.thirdForm = this.formBuilder.group({
      payment_method: ['contado', [Validators.required]],
      quotas_number: [1, [Validators.required]],
      quota: [1, [Validators.required]],
      payment_way: ['banco'],
      voucher: ['https://thumbs.dreamstime.com/b/bill-pagar-79388606.jpg'],
      payment_date: [moment(this.today).format(ConstanstEnum.DATE_FORMATE)],
      next_payment: [moment(this.today).format(ConstanstEnum.DATE_FORMATE)],
      actionPrice: [''],
      price: [this.actionPrice],
      amount: [this.actionPrice],
      water_meter: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.thirdForm.controls.quotas_number.valueChanges.subscribe(
      (res: number) => {
        this.generateQuotasPlan();
      }
    );
    this.thirdForm.controls.payment_date.valueChanges.subscribe(
      (res: number) => {
        if (this.isQuotas) {
          this.generateQuotasPlan();
        }
      }
    );
    this._getWaterMetersAvailable();
  }

  private _getWaterMetersAvailable() {
    this.waterMeterList$ = this._waterMeterFireService.getAll();
  }

  changePaymentQuota(option: string): void {
    this.isQuotas = option !== 'only' ? true : false;

    if (!this.isQuotas) {
      const paymenDate: string = this.thirdForm.controls.payment_date.value;
      this.thirdForm.controls.next_payment.setValue(moment(paymenDate).format('YYYY-MM-DD h:mm:ss'));
      this.thirdForm.controls.quotas_number.setValue(1);
      this.thirdForm.controls.amount.setValue(this.actionPrice);
    }
  }

  generateQuotasPlan(): void {
    const quotasNumber = this.thirdForm.controls.quotas_number.value;
    const datasPlan: QuotasPlan[] = [];
    let iterator = 0;
    const datePlanIni = this.thirdForm.controls.payment_date.value;
    while (iterator !== quotasNumber) {
      const plan: QuotasPlan = {
        datePayment: moment(datePlanIni).add(iterator, 'month').toDate(),
        isPaitOut: iterator === 0 ? true : false,
        money: this.actionPrice / quotasNumber
      };
      datasPlan.push(plan);
      iterator += 1;
    }
    this.dataPlanOficial = datasPlan;
    this.thirdForm.controls.amount.setValue((this.actionPrice / quotasNumber).toFixed(2));
    this.thirdForm.controls.next_payment.setValue(moment(this.dataPlanOficial[1].datePayment).format('YYYY-MM-DD h:mm:ss'));
  }

  // changePaymentWay(option: string): void {
  //   this.isWays = option !== 'odev' ? true : false;
  // }

  async fileUpload() {
    const modal = await this.modalController.create({
      component: FileUploadComponent,
      mode: 'ios',
      componentProps: {
        category: 'action',
        partner: this.partner
      }
    });

    await modal.present();

    await modal.onDidDismiss()
      .then(
        (res: any) => {
          const voucher = res.data.voucher;
          if (voucher) {
            this.thirdForm.controls.voucher.setValue(voucher);
          }
        }
      )
      .catch(
        (error: any) => {
          console.log(error);
        }
      );
  }

  saveThirdData(): void {
    const thirdData = this.thirdForm.value;
    this.thirdDataEventEmitter.emit(thirdData);
  }


  async addMeasurer(waterMeters: WaterMeter[]) {
    const modal = await this.modalController.create({
      component: AddMeasurerComponent,
      mode: 'ios',
      backdropDismiss: false,
      componentProps: {
        waterMeters: waterMeters
      }
    });

    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data.save) {
        this._getWaterMetersAvailable();
      }
    });

    await modal.present();
  }

}
