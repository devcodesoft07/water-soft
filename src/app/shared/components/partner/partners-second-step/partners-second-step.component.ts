/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable no-underscore-dangle */
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators, AbstractControl } from '@angular/forms';
import { ConstanstEnum } from '@core/enum/constants.enum';
import { PartnerLocation } from '@core/models/admin/location/location.model';
import { Partner } from '@core/models/admin/partners/partner.model';
import { PartnerLocationService } from '@core/services/location/partner-location.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-partners-second-step',
  templateUrl: './partners-second-step.component.html',
  styleUrls: ['./partners-second-step.component.scss']
})
export class PartnersSecondStepComponent implements OnInit {
  @Input() partnerData?: Partner;
  @Output() secondDataEventEmitter: EventEmitter<any> = new EventEmitter();
  secondForm: UntypedFormGroup;
  today: string = new Date().toISOString();

  locationsList: PartnerLocation[];
  filteredOptions: Observable<PartnerLocation[]>;
  constructor(
    private formBuilder: UntypedFormBuilder,
    private partnerLocationService: PartnerLocationService,
  ) {
    this.buildForm();
  }

  get partnerSecondPhone(): any {
    return this.secondForm.get('second_phone');
  }
  get partnerThirdPhone(): any {
    return this.secondForm.get('third_phone');
  }

  get partnerMembership(): any {
    return this.secondForm.get('membership');
  }


  buildForm(): void {
    this.secondForm = this.formBuilder.group({
      spouse: ['',
        [
          Validators.minLength(2),
          Validators.maxLength(255),
          Validators.pattern(ConstanstEnum.PATTERN_ONLY_TEXT),
        ]
      ],
      community: ['',
        [
          Validators.required
        ]
      ],
      membership: ['', [Validators.required]],
      sewerage: [false, [Validators.required]],
      second_phone: [''],
      third_phone: [''],
    });
  }

  ngOnInit() {
    if (this.partnerData) {
      this.secondForm.patchValue(this.partnerData);
      this.secondForm.controls.community.setValue(this.partnerData.community);
      this.secondForm.controls.second_phone.setValue(this.partnerData.telephones[1] ? this.partnerData.telephones[1] : '');
      this.secondForm.controls.third_phone.setValue(this.partnerData.telephones[2] ? this.partnerData.telephones[2] : '');
    }
    this.getAllLocations();
    this.setValidators();
  }

  setValidators() {
    const secondNumberControl: AbstractControl = this.secondForm.controls.second_phone;
    secondNumberControl.valueChanges.subscribe(
      (value: string) => {
        this.setValidatorOnControl('second_phone', value);
      }
    );
    const thirdNumberControl: AbstractControl = this.secondForm.controls.third_phone;
    thirdNumberControl.valueChanges.subscribe(
      (value: string) => {
        this.setValidatorOnControl('third_phone', value);
      }
    );
  }

  setValidatorOnControl(controlOption: string, value: string) {
    if (value !== '') {
      this.secondForm.controls[controlOption].setValidators(
        [
          Validators.maxLength(8),
          Validators.minLength(8),
          Validators.pattern(ConstanstEnum.PATTERN_PHONE),
        ]
      );
    } else {
      this.secondForm.controls[controlOption].setValidators([]);
    }
  }

  getAllLocations() {
    this.partnerLocationService.getAllLocation()
      .subscribe(
        (res: PartnerLocation[]) => {
          this.locationsList = res.slice(1, res.length - 1);
          this.filteredOptions = this.secondForm.controls.membership.valueChanges.pipe(
            startWith(''),
            map(value => (typeof value === 'string' ? value : value.location)),
            map(location => (location ? this._filter(location) : this.locationsList.slice())),
          );
        },
        (error: any) => {
          console.log(error);
        }
      );
  }

  saveSecondData(): void {
    const secondData: Partner = this.secondForm.value;
    this.secondDataEventEmitter.emit(secondData);
  }

  displayFn(partnerLocation: PartnerLocation): string {
    return partnerLocation && partnerLocation.location ? partnerLocation.location : '';
  }

  private _filter(location: string): PartnerLocation[] {
    const filterValue = location.toLowerCase();
    return this.locationsList.filter(option => option.location.toLowerCase().includes(filterValue));
  }
}
