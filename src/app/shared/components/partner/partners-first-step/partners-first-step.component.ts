/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable no-underscore-dangle */
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatStep } from '@angular/material/stepper';
import { ConstanstEnum } from '@core/enum/constants.enum';
import { Partner } from '@core/models/admin/partners/partner.model';
import { ErrorsMessage } from '@core/models/errors.model';
import { PartnerService } from '@core/services/admin/partner/partner.service';
import { SwalComponent, SwalPortalTargets } from '@sweetalert2/ngx-sweetalert2';

@Component({
  selector: 'app-partners-first-step',
  templateUrl: './partners-first-step.component.html',
  styleUrls: ['./partners-first-step.component.scss']
})
export class PartnersFirstStepComponent implements OnInit {
  @Input() firstMatStep: MatStep;
  @Input() partnerData?: Partner;
  @Output() firstDataEventEmitter: EventEmitter<any> = new EventEmitter();
  firstForm: FormGroup;
  errorMessage: ErrorsMessage = {
    status: 400
  };

  constructor(
    private formBuilder: FormBuilder,
    private partnerService: PartnerService,
    public readonly swalTargets: SwalPortalTargets
  ) {
    this.buildForm();
  }

  get partnerName(): any {
    return this.firstForm.get('name');
  }

  get partnerLastName(): any {
    return this.firstForm.get('last_name');
  }

  get partnerIdentityNumber(): any {
    return this.firstForm.get('ci_number');
  }

  get partnerIdNumComplement(): any {
    return this.firstForm.get('complement');
  }

  get partnerFirstPhone(): any {
    return this.firstForm.get('first_phone');
  }


  buildForm(): void {
    this.firstForm = this.formBuilder.group({
      name: ['',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(255),
          Validators.pattern(ConstanstEnum.PATTERN_ONLY_TEXT)
        ]
      ],
      last_name: ['',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(255),
          Validators.pattern(ConstanstEnum.PATTERN_ONLY_TEXT)
        ]
      ],
      ci_number: ['',
        [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(13),
          Validators.pattern('[0-9]*')
        ]
      ],
      complement: ['',
        [
          Validators.minLength(2),
          Validators.maxLength(2),
          Validators.pattern(ConstanstEnum.PATTERN_ONLY_TEXT_NUMBER)
        ]
      ],
      issued: ['CB', [Validators.required]],
      gender: ['M', [Validators.required]],
      first_phone: ['',
        [
          Validators.required,
          Validators.maxLength(8),
          Validators.minLength(8),
          Validators.pattern(ConstanstEnum.PATTERN_PHONE),
        ]
      ],
    });
  }

  ngOnInit(): void {
    if (this.partnerData) {
      this.firstForm.patchValue(this.partnerData);
      this.firstForm.controls.ci_number.setValue(this.partnerData.identity_card.ci_number);
      this.firstForm.controls.complement.setValue(this.partnerData.identity_card.complement);
      this.firstForm.controls.issued.setValue(this.partnerData.identity_card.issued);
      this.firstForm.controls.first_phone.setValue(this.partnerData.telephones[0]);
    }
  }

  async saveFirstData(warningSwal?: SwalComponent, errorSwal?: SwalComponent) {
    const firstData: any = this.firstForm.value;
    this.firstDataEventEmitter.emit(firstData);
    this.firstMatStep._stepper.next();
    //TODO: change to firebase verification
    // if (this.partnerData) {
    //   this.firstDataEventEmitter.emit(firstData);
    //   this.firstMatStep._stepper.next();
    // } else {
    //   Swal.fire({
    //     title: 'Verificando Datos...!',
    //     text: 'Espere un momento, estamos verificando la consistencia de los datos.',
    //     backdrop: false,
    //     timerProgressBar: true,
    //     didOpen: () => {
    //       Swal.showLoading();
    //     },
    //     willOpen: () => {
    //       this.partnerService.verifyExistPartner(firstData.ci_number)
    //       .subscribe(
    //         (res: boolean) => {
    //           if (res) {
    //             this.firstDataEventEmitter.emit(firstData);
    //             localStorage.setItem('firstData', JSON.stringify(firstData));
    //             this.firstMatStep._stepper.next();
    //             Swal.close();
    //           } else {
    //             warningSwal.fire();
    //             Swal.close();
    //           }
    //         },
    //         (error: ErrorsMessage) => {
    //           this.errorMessage = error;
    //           setTimeout(() => {
    //             Swal.close();
    //             errorSwal.fire();
    //           }, 1000);
    //         }
    //       );
    //     },
    //   }).then((result) => {
    //     /* Read more about handling dismissals below */
    //     if (result.isDismissed) {
    //       // console.log('I was closed by the timer');
    //     }
    //   });
    // }
  }

}
