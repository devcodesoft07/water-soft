/* eslint-disable @typescript-eslint/no-inferrable-types */
import { Component, Input } from '@angular/core';
import { WaterMeter } from '@core/models/admin/water-meter.model';

@Component({
  selector: 'app-measurer-pdf',
  templateUrl: './measurer-pdf.component.html',
  styleUrls: ['./measurer-pdf.component.scss'],
})
export class MeasurerPdfComponent {
  @Input() data: WaterMeter[];
  @Input() single: any;
  @Input() page: number;
  @Input() gestion: number;
  date: Date = new Date();

  view: any = [600, 230];

  // options
  gradient: boolean = true;
  showLegend: boolean = true;
  showLabels: boolean = true;
  isDoughnut: boolean = false;

  colorScheme: any = {
    domain: ['#79BBD9', '#ACC9DC', '#D7E5EE', '#AAAAAA']
  };

  displayedColumns: string[] = ['number', 'money', 'date', 'observation', 'state'];
  constructor() { }

  calculateBackground(index: number): string {
    if (index % 2 === 0) {
      return '#D7DDE480';
    } else {
      return '#ffffff';
    }
  }

}
