import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-pie',
  templateUrl: './pie.component.html',
  styleUrls: ['./pie.component.scss'],
})
export class PieComponent implements OnInit {
  @Input() single: any[];
  view: any = [undefined, 230];
  colorScheme: any = {
    domain: ['#79BBD9', '#ACC9DC', '#D7E5EE', '#AAAAAA']
  };
  showLegend = true;
  showLabels = true;
  gradient = true;


  constructor() { }

  ngOnInit() {}

}
