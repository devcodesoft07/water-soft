import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-advanced-pie',
  templateUrl: './advanced-pie.component.html',
  styleUrls: ['./advanced-pie.component.scss'],
})
export class AdvancedPieComponent implements OnInit {
  @Input() single: any[];
  view: any = [undefined, 230];

  // options
  gradient = true;
  showLegend = true;
  showLabels = true;
  isDoughnut = false;

  colorScheme: any = {
    domain: ['#79BBD9', '#ACC9DC', '#D7E5EE', '#AAAAAA']
  };

  date: Date = new Date();
  month: number;
  year: number;

  constructor() { }

  ngOnInit() {}

}
