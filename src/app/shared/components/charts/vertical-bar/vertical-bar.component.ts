import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-vertical-bar',
  templateUrl: './vertical-bar.component.html',
  styleUrls: ['./vertical-bar.component.scss'],
})
export class VerticalBarComponent implements OnInit {
  @Input() single: any[];
  colorScheme: any = {
    domain: ['#79BBD9', '#ACC9DC', '#D7E5EE', '#AAAAAA']
  };
  view: any = [undefined, 230];
  gradient = true;
  showXAxis = true;
  showYAxis = true;
  showXAxisLabel = true;
  showYAxisLabel = true;
  showGridLines = false;
  yAxisLabel = 'Monto en Bs.';
  rotateXAxisTicks = true;
  showDataLabel = true;
  timeline = true;


  constructor() { }

  ngOnInit() {}

}
