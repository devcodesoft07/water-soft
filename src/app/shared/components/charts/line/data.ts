export const multiLineDataExpample = [
  {
    name: 'Consumo de agua m3',
    series: [
      {
        name: 'Ene',
        value: 62
      },
      {
        name: 'Feb',
        value: 73
      },
      {
        name: 'Mar',
        value: 894
      },
      {
        name: 'Abr',
        value: 62
      },
      {
        name: 'May',
        value: 73
      },
      {
        name: 'Jun',
        value: 894
      },
      {
        name: 'Jul',
        value: 62
      },
      {
        name: 'Ago',
        value: 73
      },
      {
        name: 'Sep',
        value: 894
      },
      {
        name: 'Oct',
        value: 62
      },
      {
        name: 'Nov',
        value: 73
      },
      {
        name: 'Dic',
        value: 894
      },
    ]
  },
];
