import { Component, Input } from '@angular/core';
import { Color, ScaleType } from '@swimlane/ngx-charts';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line.component.html',
  styleUrls: ['./line.component.scss'],
})
export class LineComponent {
  @Input() public xAxisLabel = 'Mes';
  @Input() public yAxisLabel = 'Cantidad/Monto';
  @Input() public multi: any[];
  public view: [number, number] = [800, 400];
  public legend = false;
  public showLabels = true;
  public animations = true;
  public xAxis = true;
  public yAxis = true;
  public showYAxisLabel = true;
  public showXAxisLabel = true;
  public timeline = true;
  public colorScheme: Color = {
    domain: ['#0284C9'],
    name: 'Linear chart',
    selectable: false,
    group: ScaleType.Linear
  };
}
