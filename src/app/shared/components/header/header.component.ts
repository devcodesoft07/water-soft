import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthConstants } from '@core/config/auth-constanst';
import { AuthFireService } from '@core/services/login/login-fire.service';
import Swal from 'sweetalert2';
import { IUser } from '@core/models/admin/user.model';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  res: string = window.location.pathname.split('/admin/').join('');
  // title: string = this.res.slice(0, this.res.indexOf('/'));
  title: string;
  user: IUser;

  constructor(
    private router: Router,
    private _authFireService: AuthFireService,
    private _cookieService: CookieService
  ) { }

  ngOnInit() {
    this.user = JSON.parse(this._cookieService.get(AuthConstants.user));
    if (this.res.includes('socios/detalle')) {
      this.title = 'Socio/detalle'
    } else {
      this.title = this.res.slice(0, this.res.length);
    }
  }

  logout() {

    Swal.fire({
      title: "¿Estás seguro de cerrar sesión?",
      icon: "question",
      showCancelButton: true,
      reverseButtons: true,
      focusCancel: false,
      backdrop: false,
      confirmButtonText: "Si, Cerrar Sesión",
      cancelButtonText: "No, Cancelar"
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: 'Cerrando sesión...!',
          text: 'Espere un momento, estamos cerrando la sesión.',
          backdrop: false,
          timerProgressBar: true,
          didOpen: () => {
            Swal.showLoading();
          },
          willOpen: () => {
            this._authFireService.logoutFire().then(() => {
              // this._cookieService.delete(AuthConstants.AUTH);
              // this._cookieService.delete(AuthConstants.user);
              Swal.close();
            });
          },
        }).then((result) => {
          if (result.isDismissed) {
            this._cookieService.delete(AuthConstants.AUTH);
            this._cookieService.delete(AuthConstants.user);
            this.router.navigate(['/login']);
          }
        });
      }
    });
  }
}
