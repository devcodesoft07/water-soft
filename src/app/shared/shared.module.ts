import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { HeaderComponent } from './components/header/header.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { EmptyStateComponent } from './components/empty-state/empty-state.component';
import { MenuListComponent } from './components/menu-list/menu-list.component';
import { EmptySearchComponent } from './components/empty-search/empty-search.component';
import { DatePickerComponent } from './components/date-picker/date-picker.component';
import { MeasurerPdfComponent } from './components/pdf/measurer-pdf/measurer-pdf.component';
import { EmptyDataComponent } from './components/empty-data/empty-data.component';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { AdvancedPieComponent } from './components/charts/advanced-pie/advanced-pie.component';
import { SearchbarComponent } from './components/searchbar/searchbar.component';
import { PartnersFirstStepComponent } from './components/partner/partners-first-step/partners-first-step.component';
import { PartnersSecondStepComponent } from './components/partner/partners-second-step/partners-second-step.component';
import { PartnersThirdStepComponent } from './components/partner/partners-third-step/partners-third-step.component';
import { VerticalBarComponent } from './components/charts/vertical-bar/vertical-bar.component';
import { PieComponent } from './components/charts/pie/pie.component';


import { SearchPipe } from './pipe/measurer/search/search.pipe';
import { WaterMeterByTypePipe } from './pipe/measurer/filter/water-meter-by-active.pipe';
import { RouterActiveDirective } from './directive/router-active/router-active.directive';

import { AngularMaterialModule } from './angular-material/angular-material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AngularFireStorageModule } from '@angular/fire/compat/storage';
import { LineComponent } from './components/charts/line/line.component';
import { PartnerReaderedPipe } from './pipe/partner-readered.pipe';
import { BuildWaterConsumeDataShowChartPipe } from './pipe/water-consume/build-water-consume-data-show-chart.pipe';
import { BuildIncomesDataPipe } from './pipe/incomes/build-incomes-data.pipe';
import { BuildIncomesToShowChartPipe } from './pipe/incomes/build-incomes-to-show-chart.pipe';
import { BuildIncomesToPieChartPipe } from './pipe/incomes/build-incomes-to-pie-chart.pipe';
import { BuildExpensesToShowChartPipe } from './pipe/expenses/build-expenses-to-show-chart.pipe';
import { BuildExpensesToPieChartPipe } from './pipe/expenses/build-expenses-to-pie-chart.pipe';
import { GenerateDebtsWaterConsumePayPipe } from './pipe/water-consume/generate-debts-water-consume-pay.pipe';
import { PartnerFilterByFullNamePipe } from './pipe/partner-filter-by-full-name.pipe';
import { FilterIncomesByCategoryPipe } from './pipe/incomes/filter-incomes-by-category.pipe';
import { CalculateTotalAmountIncomesPipe } from './pipe/incomes/caculate-total-amount-incomes.pipe';
import { FilterExpensesByCategoryPipe } from './pipe/expenses/filter-expenses-by-category.pipe';
import { CalculateTotalAmountExpensesPipe } from './pipe/expenses/calculate-total-amount-expenses.pipe';
import { CalculateTotalAmountToChartIncomesPipe } from './pipe/incomes/calculate-total-amount-incomes.pipe';
import { FilterPartnerByNameOrWaterMeterPipe } from './pipe/reader/filter-partner-by-name-or-water-meter.pipe';
import { FilterWaterMeterByNumberPipe } from './pipe/measurer/filter/filter-water-meter-by-number.pipe';

@NgModule({
  declarations: [
    EmptyStateComponent,
    NavbarComponent,
    HeaderComponent,
    MenuListComponent,
    EmptyDataComponent,
    EmptySearchComponent,
    DatePickerComponent,
    MeasurerPdfComponent,
    FileUploadComponent,
    AdvancedPieComponent,
    SearchbarComponent,
    PartnersFirstStepComponent,
    PartnersSecondStepComponent,
    PartnersThirdStepComponent,
    SearchPipe,
    WaterMeterByTypePipe,
    RouterActiveDirective,
    VerticalBarComponent,
    PieComponent,
    LineComponent,
    PartnerReaderedPipe,
    BuildWaterConsumeDataShowChartPipe,
    BuildIncomesDataPipe,
    BuildIncomesToShowChartPipe,
    BuildIncomesToPieChartPipe,
    BuildExpensesToShowChartPipe,
    BuildExpensesToPieChartPipe,
    GenerateDebtsWaterConsumePayPipe,
    PartnerFilterByFullNamePipe,
    FilterIncomesByCategoryPipe,
    CalculateTotalAmountIncomesPipe,
    FilterExpensesByCategoryPipe,
    CalculateTotalAmountExpensesPipe,
    CalculateTotalAmountToChartIncomesPipe,
    FilterPartnerByNameOrWaterMeterPipe,
    FilterWaterMeterByNumberPipe
  ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    NgxChartsModule,
    AngularFireStorageModule,
  ],
  exports: [
    EmptyStateComponent,
    NavbarComponent,
    HeaderComponent,
    EmptyDataComponent,
    EmptySearchComponent,
    DatePickerComponent,
    MeasurerPdfComponent,
    FileUploadComponent,
    AdvancedPieComponent,
    SearchbarComponent,
    PartnersFirstStepComponent,
    PartnersSecondStepComponent,
    PartnersThirdStepComponent,
    SearchPipe,
    WaterMeterByTypePipe,
    RouterActiveDirective,
    VerticalBarComponent,
    PieComponent,
    LineComponent,
    PartnerReaderedPipe,
    BuildWaterConsumeDataShowChartPipe,
    BuildIncomesDataPipe,
    BuildIncomesToShowChartPipe,
    BuildIncomesToPieChartPipe,
    BuildExpensesToShowChartPipe,
    BuildExpensesToPieChartPipe,
    GenerateDebtsWaterConsumePayPipe,
    PartnerFilterByFullNamePipe,
    FilterIncomesByCategoryPipe,
    CalculateTotalAmountIncomesPipe,
    FilterExpensesByCategoryPipe,
    CalculateTotalAmountExpensesPipe,
    CalculateTotalAmountToChartIncomesPipe,
    FilterPartnerByNameOrWaterMeterPipe,
    FilterWaterMeterByNumberPipe
  ],
})
export class SharedModule { }
